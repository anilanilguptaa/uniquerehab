package com.vnnogile.uniquerehab.attendancescreen.controller;

import android.content.Context;
import android.graphics.Bitmap;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import com.vnnogile.uniquerehab.attendancescreen.model.AttendanceModel;
import com.vnnogile.uniquerehab.global.Constant;
import com.vnnogile.uniquerehab.global.LocationModel;
import com.vnnogile.uniquerehab.global.MyApplication;
import com.vnnogile.uniquerehab.global.PreferenceManager;
import com.vnnogile.uniquerehab.global.UniqueUtils;
import com.vnnogile.uniquerehab.retrofit.ApiClient;
import com.vnnogile.uniquerehab.retrofit.ApiInterface;

import org.json.JSONArray;
import org.json.JSONObject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AttendanceController {

    public static final String TAG = AttendanceController.class.getSimpleName();

    private Context context;
    private AttendanceListener listener;
    private ApiInterface apiInterface;

    public AttendanceController(Context context) {
        this.context = context;
        listener =(AttendanceListener)context;
        apiInterface = ApiClient.getClient().create(ApiInterface.class);
    }

    public interface AttendanceListener{
        void  onSuccessAdded(String message);
        void  onSuccessGet(AttendanceModel model);
        void onFailure(String errorMessage);
    }


    public void addAttendance(Bitmap bitmap, LocationModel locationModel){
        JSONObject jsonObject = new JSONObject();
        try{
            jsonObject.accumulate("date",UniqueUtils.getCurrentDate(Constant.YYYY_MM_DD_HH_MM_SS));
            jsonObject.accumulate("user_id",PreferenceManager.getInstance().getUserId());
            jsonObject.accumulate("project_id",MyApplication.getInstance().getProjectId());
            JSONArray array = new JSONArray();

            for (int i = 0; i < 1; i++) {
                JSONObject object = new JSONObject();
                object.accumulate("data",UniqueUtils.bash64(bitmap));
                object.accumulate("latitude",locationModel.latitude);
                object.accumulate("longitude",locationModel.longitude);
                object.accumulate("location",locationModel.address);
                array.put(object);
            }

            jsonObject.accumulate("images",array);
        }catch (Exception e){
            e.printStackTrace();
        }

        JsonElement jsonElement = JsonParser.parseString(jsonObject.toString());


        Call<Object> objectCall = apiInterface.addAttendance(PreferenceManager.getInstance().getToken(),
                jsonElement);

        objectCall.enqueue(new Callback<Object>() {

            @Override
            public void onResponse(Call<Object> call, Response<Object> response) {
                if (response.isSuccessful()){
                    String message ="";
                    String data = new Gson().toJson(response.body());

                    try{
                        message = new JSONObject(data).getString("message");
                    }catch (Exception e){
                        e.printStackTrace();
                        message = "Parsing error";
                    }
                    listener.onSuccessAdded(message);
                }else {
                    listener.onFailure(Constant.SERVER_ERROR);
                }
            }

            @Override
            public void onFailure(Call<Object> call, Throwable t) {
                Log.e(TAG, "onFailure: "+t.toString() );
                listener.onFailure(Constant.SERVER_ERROR);
            }
        });
    }

    public void getAttendance(String date){

        Call<AttendanceModel>  objectCall = apiInterface.getAttendance(PreferenceManager.getInstance().getToken(),
                PreferenceManager.getInstance().getUserId(),
                MyApplication.getInstance().getProjectId(),date);

        objectCall.enqueue(new Callback<AttendanceModel>() {
            @Override
            public void onResponse(Call<AttendanceModel> call, Response<AttendanceModel> response) {

                if (response.isSuccessful()){

                    listener.onSuccessGet(response.body());
                }else {
                    listener.onFailure(Constant.SERVER_ERROR);
                }

            }

            @Override
            public void onFailure(Call<AttendanceModel> call, Throwable throwable) {
                Log.e(TAG, "onFailure: "+throwable.toString() );
                listener.onFailure(Constant.SERVER_ERROR);
            }
        });

    }
}
