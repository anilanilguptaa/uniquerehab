package com.vnnogile.uniquerehab.attendancescreen.view;

import android.Manifest;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.Location;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Base64;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;
import com.squareup.picasso.Picasso;
import com.vnnogile.uniquerehab.R;
import com.vnnogile.uniquerehab.attendancescreen.controller.AttendanceController;
import com.vnnogile.uniquerehab.attendancescreen.model.AttendanceModel;
import com.vnnogile.uniquerehab.global.Constant;
import com.vnnogile.uniquerehab.global.LocationModel;
import com.vnnogile.uniquerehab.global.MyApplication;
import com.vnnogile.uniquerehab.global.PreferenceManager;
import com.vnnogile.uniquerehab.global.ProgressBar;
import com.vnnogile.uniquerehab.global.UniqueLocationTrack;
import com.vnnogile.uniquerehab.global.UniqueUtils;
import com.vnnogile.uniquerehab.global.WarningDialog;

import java.util.Calendar;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class AttendanceActivity extends AppCompatActivity implements AttendanceController.AttendanceListener,
        UniqueLocationTrack.LocationManagerListener {

    public static final String TAG = AttendanceActivity.class.getSimpleName();
    private static int REQUEST_CAMERA = 1;
    @BindView(R.id.toolbar_menu)
    ImageView toolbarMenu;
    @BindView(R.id.toolbar_name)
    TextView toolbarName;
    @BindView(R.id.toolbar_profile)
    ImageView toolbarProfile;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.txt_date)
    TextView txtDate;
    @BindView(R.id.txt_time)
    TextView txtTime;
    @BindView(R.id.image)
    ImageView image;
    @BindView(R.id.btn_tack)
    Button btnTack;
    @BindView(R.id.txt_address)
    TextView txtAddress;
    @BindView(R.id.btn_submit)
    Button btnSubmit;
    @BindView(R.id.toolbar_project)
    TextView toolbarProject;


    private ProgressBar progressBar;
    private UniqueLocationTrack uniqueLocationTrack;
    private Bitmap bitmapList;
    private LocationModel locationModel = new LocationModel();
    private AttendanceController controller;

    private Calendar calendar = Calendar.getInstance();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_attendance);
        ButterKnife.bind(this);

        loadVariables();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK) {
            if (requestCode == REQUEST_CAMERA) {
                Bitmap bitmap = (Bitmap) data.getExtras().get("data");
                bitmapList = bitmap;
                image.setImageBitmap(bitmap);
                btnTack.setText("Take Another Photo");
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    private Bitmap convertBase64ToBitmap(String b64) {
        byte[] imageAsBytes = Base64.decode(b64.getBytes(), Base64.DEFAULT);
        return BitmapFactory.decodeByteArray(imageAsBytes, 0, imageAsBytes.length);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {

        if (item.getItemId()==android.R.id.home)
            onBackPressed();

        return super.onOptionsItemSelected(item);
    }

    private void loadVariables() {

        progressBar = new ProgressBar(this);
        controller = new AttendanceController(this);

        toolbar.setNavigationIcon(R.drawable.ic_back);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        toolbarProfile.setImageResource(R.drawable.profile_picture);
        toolbarName.setText("Record Attendance");

        toolbarProject.setText(MyApplication.getInstance().getProjectName());
        txtDate.setText(UniqueUtils.getCurrentDate("dd MMM yyyy"));
        txtTime.setText(UniqueUtils.getCurrentDate("HH:mm"));

        btnSubmit.setVisibility(View.GONE);
        btnTack.setVisibility(View.GONE);

        if (UniqueUtils.isNetworkAvailable(this)){
            progressBar.show();
            controller.getAttendance(UniqueUtils.getDateInFormat(calendar.getTime(),Constant.YYYY_MM_DD));
        }else {
            UniqueUtils.showErrorMessage(Constant.NETWORK_ERROR_MESSAGE);
        }
    }

    private void showSettingsDialog() {
        WarningDialog warningDialog = new WarningDialog(this, new WarningDialog.WarningDialogListener() {
            @Override
            public void onPositiveClick() {
                UniqueUtils.openSettings(AttendanceActivity.this);
            }

            @Override
            public void onNegativeClick() {

            }
        });
        warningDialog.setMessage("This app needs permission to use this feature. You can grant them in app settings.");
        warningDialog.setPositiveButtonName("Setting");
        warningDialog.setNegativeButtonName("Cancel");
        warningDialog.show();

    }

    private void openCamera() {
        Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(cameraIntent, REQUEST_CAMERA);
    }

    @OnClick({R.id.btn_tack, R.id.btn_submit})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.btn_tack:
                Dexter.withActivity(this)
                        .withPermissions(Manifest.permission.CAMERA,
                                Manifest.permission.READ_EXTERNAL_STORAGE,
                                Manifest.permission.ACCESS_COARSE_LOCATION,
                                Manifest.permission.ACCESS_FINE_LOCATION)
                        .withListener(new MultiplePermissionsListener() {
                            @Override
                            public void onPermissionsChecked(MultiplePermissionsReport report) {
                                if (report.areAllPermissionsGranted()) {
                                    if (UniqueUtils.isLocationEnabled(AttendanceActivity.this)) {
                                        uniqueLocationTrack = new UniqueLocationTrack(AttendanceActivity.this);
                                        uniqueLocationTrack.setListener(AttendanceActivity.this);
                                        openCamera();
                                    }else {
                                        UniqueUtils.showWarningMessage(Constant.ENABLE_LOCATION);
                                    }
                                } else {
                                    showSettingsDialog();
                                }
                            }

                            @Override
                            public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {
                                token.continuePermissionRequest();
                            }
                        })
                        .onSameThread()
                        .check();
                break;
            case R.id.btn_submit:

                if (bitmapList != null) {
                    if (UniqueUtils.isNetworkAvailable(this)) {
                        progressBar.show();
                        controller.addAttendance(bitmapList, locationModel);
                    } else {
                        UniqueUtils.showErrorMessage(Constant.NETWORK_ERROR_MESSAGE);
                    }
                } else {
                    UniqueUtils.showWarningMessage("Please Take a photo.");
                }
                break;
        }
    }

    @Override
    public void onSuccessAdded(String message) {
        progressBar.dismiss();
        UniqueUtils.showSuccessMessage(message);
        btnSubmit.setVisibility(View.GONE);
        btnTack.setVisibility(View.GONE);
    }

    @Override
    public void onFailure(String errorMessage) {
        progressBar.dismiss();
        UniqueUtils.showErrorMessage(errorMessage);
        if (!PreferenceManager.getInstance().getUserRole().equals(UniqueUtils.ADMIN)) {
            btnSubmit.setVisibility(View.VISIBLE);
            btnTack.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onSuccessGet(AttendanceModel model) {
        progressBar.dismiss();

        if (model!=null && model.path!=null && !model.path.isEmpty()){
            txtAddress.setText(model.location==null?"Unknown location":model.location);
            Picasso.get().load(model.path).into(image);
            btnSubmit.setVisibility(View.GONE);
            btnTack.setVisibility(View.GONE);
        }else {
            if (!PreferenceManager.getInstance().getUserRole().equals(UniqueUtils.ADMIN)) {
                btnSubmit.setVisibility(View.VISIBLE);
                btnTack.setVisibility(View.VISIBLE);
            }
        }

    }



    @Override
    public void onLocationResult(Location location) {
        if (uniqueLocationTrack != null) {
            locationModel = new LocationModel();
            locationModel.latitude = location.getLatitude();
            locationModel.longitude = location.getLongitude();
            locationModel.address = uniqueLocationTrack.getAddress(location.getLatitude(), location.getLongitude());
            Log.e(TAG, "onLocationResult: " + locationModel.latitude);
            txtAddress.setText(locationModel.address);
            PreferenceManager.getInstance().setPunchInAddress(locationModel.address);

        }
    }

    @OnClick(R.id.toolbar_profile)
    public void onViewClicked() {
        UniqueUtils.goToHomePage(this);
    }
}
