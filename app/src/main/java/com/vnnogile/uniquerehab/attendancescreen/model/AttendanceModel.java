package com.vnnogile.uniquerehab.attendancescreen.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class AttendanceModel implements Parcelable {

    @SerializedName("id")
    @Expose
    public int id;
    @SerializedName("name")
    @Expose
    public String name;
    @SerializedName("description")
    @Expose
    public String description;
    @SerializedName("path")
    @Expose
    public String path;
    @SerializedName("document_type")
    @Expose
    public String documentType;
    @SerializedName("created_date")
    @Expose
    public String createdDate;
    @SerializedName("created_at")
    @Expose
    public String createdAt;
    @SerializedName("is_active")
    @Expose
    public int isActive;
    @SerializedName("latitude")
    @Expose
    public String latitude;
    @SerializedName("longitude")
    @Expose
    public String longitude;
    @SerializedName("location")
    @Expose
    public String location="";
    @SerializedName("project_id")
    @Expose
    public int projectId;
    @SerializedName("status")
    @Expose
    public boolean status = false;
    @SerializedName("message")
    @Expose
    public String message;

    protected AttendanceModel(Parcel in) {
        id = in.readInt();
        name = in.readString();
        description = in.readString();
        path = in.readString();
        documentType = in.readString();
        createdDate = in.readString();
        createdAt = in.readString();
        isActive = in.readInt();
        latitude = in.readString();
        longitude = in.readString();
        location = in.readString();
        projectId = in.readInt();
        status = in.readByte() != 0;
        message = in.readString();
    }

    public static final Creator<AttendanceModel> CREATOR = new Creator<AttendanceModel>() {
        @Override
        public AttendanceModel createFromParcel(Parcel in) {
            return new AttendanceModel(in);
        }

        @Override
        public AttendanceModel[] newArray(int size) {
            return new AttendanceModel[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeString(name);
        dest.writeString(description);
        dest.writeString(path);
        dest.writeString(documentType);
        dest.writeString(createdDate);
        dest.writeString(createdAt);
        dest.writeInt(isActive);
        dest.writeString(latitude);
        dest.writeString(longitude);
        dest.writeString(location);
        dest.writeInt(projectId);
        dest.writeByte((byte) (status ? 1 : 0));
        dest.writeString(message);
    }
}
