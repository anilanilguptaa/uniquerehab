package com.vnnogile.uniquerehab.retrofit;

import com.google.gson.JsonElement;
import com.vnnogile.uniquerehab.attendancescreen.model.AttendanceModel;
import com.vnnogile.uniquerehab.dailyprogressscreen.model.DPPModel;
import com.vnnogile.uniquerehab.dashboardscreen.model.DoumentModel;
import com.vnnogile.uniquerehab.dashboardscreen.model.LandingPageModel;
import com.vnnogile.uniquerehab.dashboardscreen.model.NotificationModel;
import com.vnnogile.uniquerehab.dprscreen.model.BOQModel;
import com.vnnogile.uniquerehab.dprscreen.model.DPRModel;
import com.vnnogile.uniquerehab.dprscreen.model.SearchModel;
import com.vnnogile.uniquerehab.dprscreen.model.boqRequestBody;
import com.vnnogile.uniquerehab.global.ResponseMessage;
import com.vnnogile.uniquerehab.loginscreen.model.LoginResponse;
import com.vnnogile.uniquerehab.loginscreen.model.User;
import com.vnnogile.uniquerehab.manpowerscren.model.ManPowerModel;
import com.vnnogile.uniquerehab.materialaudit.model.MaterialAuditModel;
import com.vnnogile.uniquerehab.materialconsumptionscreen.model.MaterialConsumptionData;
import com.vnnogile.uniquerehab.materialinward.model.MaterialInModel;
import com.vnnogile.uniquerehab.materialinward.model.MaterialInWord;
import com.vnnogile.uniquerehab.materialtransfer.model.MaterialTransferModel;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;

public interface ApiInterface {


    // @Headers({""})
    @POST("login")
    Call<LoginResponse> login(@Body User user);


    @Headers({"Accept:application/json"})
    @GET("landingpage/projects/{user_id}")
    Call<List<LandingPageModel>> getLandingPage(@Header("Authorization") String token, @Path("user_id") String userId);


    @Headers({"Accept:application/json"})
    @GET("notification/{user_id}/{project_id}")
    Call<List<NotificationModel>> getUnreadProject(@Header("Authorization") String token, @Path("user_id") String userId,
                                                   @Path("project_id") String projectId);

    @Headers({"Accept:application/json"})
    @GET("dailyprogressreport/boq/{user_id}/{project_id}/{name}")
    Call<List<SearchModel>> getSearch(@Header("Authorization") String token, @Path("user_id") String userId,
                                      @Path("project_id") String project_id, @Path("name") String name);


    @Headers({"Accept:application/json"})
    @GET("dailyprogressreport/{user_id}/{project_id}/{boq_id}/{date}")
    Call<List<BOQModel>> getBOQDetails(@Header("Authorization") String token, @Path("user_id") String userId,
                                       @Path("project_id") String project_id, @Path("boq_id") String boq_id,
                                       @Path("date") String strDate);

    @Headers({"Accept:application/json"})
    @POST("deleteDprData")
    Call<Object> deleteBOQ(@Header("Authorization") String token, @Body boqRequestBody boqRequestBody);


    @Headers({"Accept:application/json"})
    @POST("dailyprogressreport")
    Call<Object> addBOQ(@Header("Authorization") String token, @Body boqRequestBody boqRequestBody);

    //urb.vnnogile.in/api/getdailyprogressphotos/1/1/2020-05-04
    @Headers({"Accept:application/json"})
    @GET("getdailyprogressphotos/{user_id}/{project_id}/{date}")
    Call<List<SearchModel>> getBOQListDPP(@Header("Authorization") String token, @Path("user_id") String userId,
                                          @Path("project_id") String project_id, @Path("date") String date);

    //getlistofdpponload/user_id/program_id/date
    @Headers({"Accept:application/json"})
    @GET("getlistofdpponload/{user_id}/{project_id}/{date}")
    Call<List<DPPModel>> getDPPList(@Header("Authorization") String token, @Path("user_id") String userId,
                                    @Path("project_id") String project_id, @Path("date") String date);

    @Headers({"Accept:application/json"})
    @POST("storedailyprogressphotos")
    Call<Object> uploadImage(@Header("Authorization") String token, @Body JsonElement boqRequestBody);

    @Headers({"Accept:application/json"})
    @GET("materialinwards/{user_id}/{project_id}/{date}")
    Call<MaterialInModel> getMaterialIn(@Header("Authorization") String token, @Path("user_id") String userId,
                                        @Path("project_id") String project_id, @Path("date") String date);


    @Headers({"Accept:application/json"})
    @POST("savematerialinwardsdata")
    Call<ResponseMessage> addMaterial(@Header("Authorization") String token, @Body JsonElement boqRequestBody);


    @Headers({"Accept:application/json"})
    @GET("getinwardstimelist/{user_id}/{project_id}/{date}")
    Call<List<MaterialInModel>> getMaterialInWardsTime(@Header("Authorization") String token, @Path("user_id") String userId,
                                                       @Path("project_id") String project_id, @Path("date") String date);


    //getinwardsdetailsdata/user_id/program_id/header_id
    @Headers({"Accept:application/json"})
    @GET("getinwardsdetailsdata/{user_id}/{project_id}/{header_id}")
    Call<MaterialInWord> getMaterialInWardsList(@Header("Authorization") String token, @Path("user_id") String userId,
                                                @Path("project_id") String project_id, @Path("header_id") String headerId);


    //manpower/user_id/program_id/date

    @Headers({"Accept:application/json"})
    @GET("manpower/{user_id}/{project_id}/{date}")
    Call<ManPowerModel> getManpower(@Header("Authorization") String token, @Path("user_id") String userId,
                                    @Path("project_id") String project_id, @Path("date") String date);


    @Headers({"Accept:application/json"})
    @POST("manpower")
    Call<ResponseMessage> uploadManPower(@Header("Authorization") String token, @Body JsonElement boqRequestBody);

    @Headers({"Accept:application/json"})
    @POST("deleteManpowerData")
    Call<Object> deleteManPower(@Header("Authorization") String token, @Body JsonElement boqRequestBody);


    @Headers({"Accept:application/json"})
    @POST("saveAttendanceRecord")
    Call<Object> addAttendance(@Header("Authorization") String token, @Body JsonElement boqRequestBody);


    @Headers({"Accept:application/json"})
    @POST("deletedailyprogressphotos")
    Call<ResponseMessage> deleteDPPphoto(@Header("Authorization") String token, @Body JsonElement boqRequestBody);

    @Headers({"Accept:application/json"})
    @POST("deletematerialinwardsimage")
    Call<ResponseMessage> deleteMaterialphoto(@Header("Authorization") String token, @Body JsonElement boqRequestBody);


    @Headers({"Accept:application/json"})
    @POST("deletematerialinwards")
    Call<ResponseMessage> deletematerialinwards(@Header("Authorization") String token, @Body JsonElement boqRequestBody);


    @Headers({"Accept:application/json"})
    @GET("materialconsumption/{user_id}/{project_id}/{date}")
    Call<MaterialConsumptionData> getMaterialConsumption(@Header("Authorization") String token, @Path("user_id") String userId,
                                                         @Path("project_id") String project_id, @Path("date") String date);


    @Headers({"Accept:application/json"})
    @GET("getmateriallistforconsumption/{user_id}/{project_id}/{date}")
    Call<List<MaterialInModel.MaterialDatum>> getMaterial(@Header("Authorization") String token, @Path("user_id") String userId,
                                                          @Path("project_id") String project_id, @Path("date") String date);


    @Headers({"Accept:application/json"})
    @POST("materialconsumption")
    Call<ResponseMessage> addMaterialConsumption(@Header("Authorization") String token, @Body JsonElement boqRequestBody);


    @Headers({"Accept:application/json"})
    @POST("deleteMaterialConsumptionData")
    Call<ResponseMessage> deleteMaterialConsumption(@Header("Authorization") String token, @Body JsonElement boqRequestBody);

    @Headers({"Accept:application/json"})
    @POST("deleteMaterialConsumptionImage")
    Call<ResponseMessage> deleteMaterialConsumptionImage(@Header("Authorization") String token, @Body JsonElement boqRequestBody);


    //urb.vnnogile.in/api/getDprDataForClientView/1/1/2020-05-19
    @Headers({"Accept:application/json"})
    @GET("getDprDataForClientView/{user_id}/{project_id}/{date}")
    Call<List<DPRModel>> getDprDataForClientView(@Header("Authorization") String token, @Path("user_id") String userId,
                                                 @Path("project_id") String project_id, @Path("date") String date);


    @Headers({"Accept:application/json"})
    @POST("changeBoqStatus")
    Call<ResponseMessage> updateDPRStatus(@Header("Authorization") String token, @Body JsonElement boqRequestBody);


    @Headers({"Accept:application/json"})
    @GET("materialrequest/{user_id}/{project_id}/{date}")
    Call<MaterialInModel> materialrequest(@Header("Authorization") String token, @Path("user_id") String userId,
                                          @Path("project_id") String project_id, @Path("date") String date);


    @Headers({"Accept:application/json"})
    @POST("materialrequest")
    Call<ResponseMessage> materialAddrequest(@Header("Authorization") String token, @Body JsonElement boqRequestBody);

    @Headers({"Accept:application/json"})
    @PUT("materialrequest")
    Call<ResponseMessage> materialUpdaterequest(@Header("Authorization") String token, @Body JsonElement boqRequestBody);


    @Headers({"Accept:application/json"})
    @POST("updatePriceOfMaterialRequest")
    Call<ResponseMessage> updatePriceOfMaterialRequest(@Header("Authorization") String token, @Body JsonElement boqRequestBody);

    @Headers({"Accept:application/json"})
    @POST("deletematerialrequest")
    Call<ResponseMessage> deletematerialrequest(@Header("Authorization") String token, @Body JsonElement boqRequestBody);


    @Headers({"Accept:application/json"})
    @GET("getMaterialListForMaterialTransfer/{user_id}/{project_id}/{date}")
    Call<List<MaterialInModel.MaterialDatum>> getMaterialListForMaterialTransfer(@Header("Authorization") String token, @Path("user_id") String userId,
                                                                                 @Path("project_id") String project_id, @Path("date") String date);


    @Headers({"Accept:application/json"})
    @GET("getSitesData/{user_id}/{project_id}/{date}")
    Call<List<MaterialTransferModel.SiteData>> getSitesData(@Header("Authorization") String token, @Path("user_id") String userId,
                                                            @Path("project_id") String project_id, @Path("date") String date);


    @Headers({"Accept:application/json"})
    @POST("materialtransfer")
    Call<ResponseMessage> materialtransfer(@Header("Authorization") String token, @Body JsonElement boqRequestBody);


    @Headers({"Accept:application/json"})
    @GET("materialtransfer/{user_id}/{project_id}/{date}")
    Call<MaterialTransferModel> getmaterialtransfer(@Header("Authorization") String token, @Path("user_id") String userId,
                                                    @Path("project_id") String project_id, @Path("date") String date);


    @Headers({"Accept:application/json"})
    @GET("getMaterialListForMaterialAudit/{user_id}/{project_id}/{date}")
    Call<List<MaterialInModel.MaterialDatum>> getMaterialListForMaterialAudit(@Header("Authorization") String token, @Path("user_id") String userId,
                                                                              @Path("project_id") String project_id, @Path("date") String date);


    @Headers({"Accept:application/json"})
    @POST("materialaudit")
    Call<ResponseMessage> materialaudit(@Header("Authorization") String token, @Body JsonElement boqRequestBody);


    @Headers({"Accept:application/json"})
    @GET("materialaudit/{user_id}/{project_id}/{date}")
    Call<MaterialAuditModel> getAdminMaterialaudit(@Header("Authorization") String token, @Path("user_id") String userId,
                                                   @Path("project_id") String project_id, @Path("date") String date);


    @Headers({"Accept:application/json"})
    @PUT("materialaudit")
    Call<ResponseMessage> updatematerialaudit(@Header("Authorization") String token, @Body JsonElement boqRequestBody);

    @Headers({"Accept:application/json"})
    @POST("deleteMaterialTransferImage")
    Call<ResponseMessage> deleteMaterialTransferImage(@Header("Authorization") String token, @Body JsonElement boqRequestBody);


    @Headers({"Accept:application/json"})
    @POST("deleteMaterialTransferData")
    Call<ResponseMessage> deleteMaterialTransferData(@Header("Authorization") String token, @Body JsonElement boqRequestBody);


    @Headers({"Accept:application/json"})
    @GET("attendance/{user_id}/{project_id}/{date}")
    Call<AttendanceModel> getAttendance(@Header("Authorization") String token, @Path("user_id") String userId,
                                        @Path("project_id") String project_id, @Path("date") String date);


    //urb.vnnogile.in/api/downloadProjectDocument/303/16/tender_doc
    @Headers({"Accept:application/json"})
    @GET("downloadProjectDocument/{user_id}/{project_id}/tender_doc")
    Call<DoumentModel> downloadTenderDoc(@Header("Authorization") String token, @Path("user_id") String userId,
                                   @Path("project_id") String project_id);


    @Headers({"Accept:application/json"})
    @GET("downloadProjectDocument/{user_id}/{project_id}/project_doc")
    Call<DoumentModel> downloadProjectDoc(@Header("Authorization") String token, @Path("user_id") String userId,
                                          @Path("project_id") String project_id);


    @Headers({"Accept:application/json"})
    @GET("getMaterialListByTypeAhead/{key}")
    Call<List<MaterialInModel.MaterialDatum>> getMaterialListByTypeAhead(@Header("Authorization") String token, @Path("key") String key);

//urb.vnnogile.in/api/getMaterialListFromInventoryOnTypeAhead/user_id/project_id/material_code
    @Headers({"Accept:application/json"})
    @GET("getMaterialListFromInventoryOnTypeAhead/{user_id}/{project_id}/{key}")
    Call<List<MaterialInModel.MaterialDatum>> getMaterialListFromInventoryOnTypeAhead(@Header("Authorization") String token,
                                                                                      @Path("user_id") String user_id,
                                                                                      @Path("project_id") String project_id,
                                                                                      @Path("key") String key);


    @Headers({"Accept:application/json"})
    @GET("getActiveDates/{user_id}/{project_id}/{eventCode}/{date}")
    Call<List<String>> getActiveDates(@Header("Authorization") String token, @Path("user_id") String user_id,
                                @Path("project_id") String project_id,@Path("eventCode") String eventCode,
                                @Path("date") String date);


    @Headers({"Accept:application/json"})
    @POST("forgotPassword")
    Call<ResponseMessage> resetPassword( @Body JsonElement boqRequestBody);

}
