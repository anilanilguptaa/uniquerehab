package com.vnnogile.uniquerehab.global;

public class Constant {


    public static final String NETWORK_ERROR_MESSAGE = "Internet connection error.";
    public static final String SERVER_ERROR = "Server error.";
    public static final String SOMETHING_WRONG = "Something wrong.";
    public static final String NO_DATA_FOUND = "No Data Found";
    public static final String YYYY_MM_DD = "yyyy-MM-dd";
    public static final String YYYY_MM_DD_HH_MM_SS = "yyyy-MM-dd HH:mm:ss";
    public static final String SELECT_PROJECT = "Please select the project first";
    public static final String ENABLE_LOCATION = "Please enable your device location.";
}
