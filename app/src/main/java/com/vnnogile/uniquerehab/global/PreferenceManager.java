package com.vnnogile.uniquerehab.global;

import android.content.Context;
import android.content.SharedPreferences;

import com.google.gson.Gson;
import com.vnnogile.uniquerehab.loginscreen.model.LoginResponse;

public class PreferenceManager {

    private static SharedPreferences sharedPreferences = null;
    private static PreferenceManager manager = null;


    public static PreferenceManager getInstance() {
        if (manager == null) {
            manager = new PreferenceManager();
        }
        sharedPreferences = MyApplication.getInstance().getSharedPreferences("unique_pref", Context.MODE_PRIVATE);

        return manager;
    }

    public void clearUserData() {
        sharedPreferences.edit().clear().apply();
    }

    public void setIsLogin(boolean flag) {
        sharedPreferences.edit().putBoolean("is_login", flag).apply();
    }

    public boolean isLogin() {
        return sharedPreferences.getBoolean("is_login", false);
    }

    public void setToken(String token) {
        sharedPreferences.edit().putString("token", token).apply();
    }

    public String getToken() {
        return "Bearer " + sharedPreferences.getString("token", "");
    }

    public void setUserDetails(LoginResponse userDetails) {
        String data = new Gson().toJson(userDetails, LoginResponse.class);
        sharedPreferences.edit().putString("userDetails", data).apply();
    }

    public LoginResponse getUserDetails() {
        LoginResponse loginResponse = new Gson().fromJson(sharedPreferences.getString("userDetails", ""), LoginResponse.class);

        return loginResponse;
    }

    public void setUsername(String username) {

        sharedPreferences.edit().putString("username", username).apply();
    }

    public String getUsername() {

        return sharedPreferences.getString("username", "");
    }

    public void setPassword(String password) {

        sharedPreferences.edit().putString("password", password).apply();
    }

    public String getPassword() {

        return sharedPreferences.getString("password", "");
    }

    public String getUserId() {
        LoginResponse loginResponse = getUserDetails();
        return String.valueOf(loginResponse.userDetail.id);
    }

    public String getUserRole() {
        LoginResponse loginResponse = getUserDetails();
        return String.valueOf(loginResponse.roleName);
    }

    public void setPunchInDate(String flag) {
        sharedPreferences.edit().putString("is_punchIn", flag).apply();
    }

    public String getIsPunchIn() {
        return sharedPreferences.getString("is_punchIn", "");
    }

    public void setPunchInAddress(String address) {
        sharedPreferences.edit().putString("address", address).apply();
    }

    public String getPunchInAddress() {
        return sharedPreferences.getString("address", "");
    }

    public void setImage(String base64) {
        sharedPreferences.edit().putString("base64", base64).apply();

    }

    public String getImage() {
        return sharedPreferences.getString("base64", "");

    }
}
