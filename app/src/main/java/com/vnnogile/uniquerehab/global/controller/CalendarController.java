package com.vnnogile.uniquerehab.global.controller;

import android.content.Context;
import android.util.Log;

import com.vnnogile.uniquerehab.global.Constant;
import com.vnnogile.uniquerehab.global.MyApplication;
import com.vnnogile.uniquerehab.global.PreferenceManager;
import com.vnnogile.uniquerehab.retrofit.ApiClient;
import com.vnnogile.uniquerehab.retrofit.ApiInterface;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CalendarController {

    public static final String TAG = CalendarController.class.getSimpleName();

    private Context context;
    private CalendarControllerListener listener;
    private ApiInterface apiInterface;

    public CalendarController(Context context) {
        this.context = context;
        apiInterface = ApiClient.getClient().create(ApiInterface.class);
    }

    public void setListener(CalendarControllerListener listener) {
        this.listener = listener;
    }

    public interface CalendarControllerListener{
     void onEventDataGet(List<String> list);
     void onFailure(String message);
    }

    public void getData(String code,String date){

        Call<List<String>> objectCall = apiInterface.getActiveDates(
                PreferenceManager.getInstance().getToken(),
                PreferenceManager.getInstance().getUserId(),
                MyApplication.getInstance().getProjectId(),
                code,date
        );


        objectCall.enqueue(new Callback<List<String>>() {

            @Override
            public void onResponse(Call<List<String>> call, Response<List<String>> response) {
                listener.onEventDataGet(response.body());
            }

            @Override
            public void onFailure(Call<List<String>> call, Throwable throwable) {
                Log.e(TAG, "onFailure: "+throwable.toString() );
                  listener.onFailure(Constant.SERVER_ERROR);
            }
        });

    }
}
