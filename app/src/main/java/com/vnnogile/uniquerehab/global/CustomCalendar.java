package com.vnnogile.uniquerehab.global;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.core.view.GravityCompat;

import com.vnnogile.uniquerehab.R;

import java.util.Date;
import java.util.HashSet;

import butterknife.BindView;
import butterknife.ButterKnife;

public class CustomCalendar extends Dialog {

    public static final String TAG = CustomCalendar.class.getSimpleName();

    @BindView(R.id.custom_calandar)
    CustomCalendarView customCalandar;
    private Context context;
    private CustomCalendarListener listener;

    private String eventCode="";

    public CustomCalendar(@NonNull Context context,String eventCode) {
        super(context);
        this.context = context;
        this.eventCode=eventCode;
    }

    public interface CustomCalendarListener{
        void onDateSet(Date selectedDate);
    }

    public void setListener(CustomCalendarListener listener) {
        this.listener = listener;
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_custom_calendar);
        ButterKnife.bind(this);

        if (getWindow()!=null){
            getWindow().setLayout(ViewGroup.LayoutParams.WRAP_CONTENT,ViewGroup.LayoutParams.WRAP_CONTENT);
            getWindow().setBackgroundDrawableResource(android.R.color.transparent);
            getWindow().setGravity(Gravity.CENTER);
        }

        customCalandar.getEventData(eventCode);
        customCalandar.setEventHandler(new CustomCalendarView.EventHandler() {
            @Override
            public void onDateSelect(Date date) {
                if (listener!=null){
                    dismiss();
                    listener.onDateSet(date);
                }
            }
        });

    }

}
