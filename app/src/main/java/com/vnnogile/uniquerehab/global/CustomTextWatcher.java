package com.vnnogile.uniquerehab.global;

import android.text.Editable;
import android.text.TextWatcher;

public class CustomTextWatcher implements TextWatcher {
    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {
        if (s.length()==1){
            if (s.charAt(0)=='.'){
                s="0";
            }
        }

    }

    @Override
    public void afterTextChanged(Editable s) {

    }
}
