package com.vnnogile.uniquerehab.global;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.vnnogile.uniquerehab.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class WarningDialog extends Dialog {

    public static final String TAG = WarningDialog.class.getSimpleName();
    @BindView(R.id.txt_message)
    TextView txtMessage;
    @BindView(R.id.txt_yes)
    TextView txtYes;
    @BindView(R.id.txt_no)
    TextView txtNo;

    private Context context;
    private WarningDialogListener listener;

    private String message ="";
    private String positiveButtonName = "";
    private String negativeButtonName = "";

    public WarningDialog(@NonNull Context context, WarningDialogListener listener) {
        super(context);
        this.context = context;
        this.listener = listener;
    }

    public interface WarningDialogListener {
        void onPositiveClick();

        void onNegativeClick();
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public void setPositiveButtonName(String positiveButtonName) {
        this.positiveButtonName = positiveButtonName;
    }

    public void setNegativeButtonName(String negativeButtonName) {
        this.negativeButtonName = negativeButtonName;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_warning);
        ButterKnife.bind(this);
        setCancelable(true);

        if (getWindow() != null) {
            getWindow().setBackgroundDrawableResource(android.R.color.transparent);
            getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        }

        if (!positiveButtonName.isEmpty())
            txtYes.setText(positiveButtonName);
        if (!negativeButtonName.isEmpty())
            txtNo.setText(negativeButtonName);
        txtMessage.setText(message);
    }

    @OnClick(R.id.txt_yes)
    public void onTxtYesClicked() {
        if (listener != null) {
            dismiss();
            listener.onPositiveClick();
        }
    }

    @OnClick(R.id.txt_no)
    public void onTxtNoClicked() {
        if (listener != null) {
            dismiss();
            listener.onNegativeClick();
        }
    }
}
