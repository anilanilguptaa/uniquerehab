package com.vnnogile.uniquerehab.global;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.provider.Settings;
import android.util.Base64;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Toast;

import androidx.annotation.NonNull;

import com.vnnogile.uniquerehab.dashboardscreen.view.DashboardActivity;

import java.io.ByteArrayOutputStream;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import es.dmoral.toasty.Toasty;

public class UniqueUtils {


    public static final String ADMIN="Admin";
    public static final String Site_Engineer="Site Engineer";
    public static final String Client = "Client";

    public static boolean isNetworkAvailable(Context context) {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    public static boolean isLocationEnabled(Context context) {
        LocationManager locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
        return locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER) || locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
    }


    public static void showErrorMessage(String message) {

        Toasty.error(MyApplication.getInstance().getApplicationContext(), message, Toast.LENGTH_SHORT, true).show();
    }


    public static void showSuccessMessage(String message) {
        Toasty.success(MyApplication.getInstance().getApplicationContext(), message, Toast.LENGTH_SHORT, true).show();
    }


    public static void showWarningMessage(String message) {
        Toasty.warning(MyApplication.getInstance().getApplicationContext(), message, Toast.LENGTH_SHORT, true).show();
    }

    public static String removeChar(String data) {
        return data.replace("\n", "").replace("\r", "");
    }

    public static String getCurrentDate(@NonNull String format) {
        return new SimpleDateFormat(format, new Locale("en", "in")).format(System.currentTimeMillis());
    }

    public static String getDateInFormat(Date date,String format){
        return new SimpleDateFormat(format, new Locale("en", "in")).format(date);
    }

    // navigating user to app settings
    public static void openSettings(Activity context) {
        Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
        Uri uri = Uri.fromParts("package", context.getPackageName(), null);
        intent.setData(uri);
        context.startActivityForResult(intent, 101);
    }

    public static String bash64(Bitmap bitmap) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStream);
        byte[] byteArray = byteArrayOutputStream.toByteArray();
        return Base64.encodeToString(byteArray, Base64.DEFAULT);
    }

    public static void hideKeyboard(Activity activity) {
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        //Find the currently focused view, so we can grab the correct window token from it.
        View view = activity.getCurrentFocus();
        //If no view currently has focus, create a new one, just so we can grab a window token from it
        if (view == null) {
            view = new View(activity);
        }
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    public static String convertDate(String dat,String oldFormat,String newFormat){
        DateFormat originalFormat = new SimpleDateFormat(oldFormat, Locale.ENGLISH);
        DateFormat targetFormat = new SimpleDateFormat(newFormat,Locale.ENGLISH);
        Date date = null;
        try {
            date = originalFormat.parse(dat);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        String formattedDate = targetFormat.format(date);

        return formattedDate;
    }

    public static Date convertDate(String dat,String oldFormat){
        DateFormat originalFormat = new SimpleDateFormat(oldFormat, Locale.ENGLISH);
        Date date = null;
        try {
            date = originalFormat.parse(dat);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return date;
    }

    public static void goToHomePage(Context context){
        Intent intent = new Intent(context, DashboardActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(intent);
    }
}
