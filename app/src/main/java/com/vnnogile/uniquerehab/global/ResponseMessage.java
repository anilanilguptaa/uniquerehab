package com.vnnogile.uniquerehab.global;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ResponseMessage implements Parcelable {

    @SerializedName("message")
    @Expose
    public String message="";
    @SerializedName("msg")
    @Expose
    public String msg="";
    @SerializedName("status")//"status":false,
    @Expose
    public boolean status = true;

    protected ResponseMessage(Parcel in) {
        message = in.readString();
        msg = in.readString();
        status = in.readByte() != 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(message);
        dest.writeString(msg);
        dest.writeByte((byte) (status ? 1 : 0));
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<ResponseMessage> CREATOR = new Creator<ResponseMessage>() {
        @Override
        public ResponseMessage createFromParcel(Parcel in) {
            return new ResponseMessage(in);
        }

        @Override
        public ResponseMessage[] newArray(int size) {
            return new ResponseMessage[size];
        }
    };

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }
}
