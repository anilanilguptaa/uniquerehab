package com.vnnogile.uniquerehab.global;

import android.app.Application;

import com.vnnogile.uniquerehab.dashboardscreen.model.LandingPageModel;

import java.util.ArrayList;
import java.util.List;

public class MyApplication extends Application {

    public static final String TAG = MyApplication.class.getSimpleName();

    private static MyApplication instance = null;

    public static MyApplication getInstance(){
        if (instance == null){
            instance = new MyApplication();
        }

        return instance;
    }

    private int projectId=0;
    private int selectedPosition=0;
    private String projectName="";
    private boolean isGetTheProject = false;
    private List<LandingPageModel> list = new ArrayList<>();
    private int siteId;

    @Override
    public void onCreate() {
        super.onCreate();

        instance = this;
    }

    public String getProjectId() {
        return String.valueOf(projectId);
    }

    public void setProjectId(int projectId) {
        this.projectId = projectId;
    }

    public int getSelectedPosition() {
        return selectedPosition;
    }

    public void setSelectedPosition(int selectedPosition) {
        this.selectedPosition = selectedPosition;
    }

    public String getProjectName() {
        return projectName;
    }

    public void setProjectName(String projectName) {
        this.projectName = projectName;
    }

    public boolean isGetTheProject() {
        return isGetTheProject;
    }

    public void setGetTheProject(boolean getTheProject) {
        isGetTheProject = getTheProject;
    }

    public List<LandingPageModel> getList() {
        return list;
    }

    public void setList(List<LandingPageModel> list) {
        this.list = list;
    }

    public void clear(){
        projectId=0;
        projectName="";
        selectedPosition =0;
        isGetTheProject = false;
        list.clear();
    }
}
