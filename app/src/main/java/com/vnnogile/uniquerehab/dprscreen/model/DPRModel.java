package com.vnnogile.uniquerehab.dprscreen.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class DPRModel implements Parcelable {

    @SerializedName("boq_name")
    @Expose
    public String boqName;
    @SerializedName("boq_id")
    @Expose
    public int id;
    @SerializedName("status")
    @Expose
    public String status="";
    @SerializedName("dpr_data")
    @Expose
    public List<BOQModel> dprData = null;

    protected DPRModel(Parcel in) {
        boqName = in.readString();
        id = in.readInt();
        status = in.readString();
        dprData = in.createTypedArrayList(BOQModel.CREATOR);
    }

    public static final Creator<DPRModel> CREATOR = new Creator<DPRModel>() {
        @Override
        public DPRModel createFromParcel(Parcel in) {
            return new DPRModel(in);
        }

        @Override
        public DPRModel[] newArray(int size) {
            return new DPRModel[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(boqName);
        dest.writeInt(id);
        dest.writeString(status);
        dest.writeTypedList(dprData);
    }

    public static class DprDatum implements Parcelable {
        @SerializedName("boq_id ")
        @Expose
        public int boqId;
        @SerializedName("boq_name")
        @Expose
        public String boqName;
        @SerializedName("project_id")
        @Expose
        public int projectId;
        @SerializedName("dpr_id")
        @Expose
        public int dprId;
        @SerializedName("length")
        @Expose
        public String length;
        @SerializedName("width")
        @Expose
        public String width;
        @SerializedName("depth")
        @Expose
        public String depth;
        @SerializedName("total")
        @Expose
        public String total;
        @SerializedName("quantity")
        @Expose
        public String quantity;

        protected DprDatum(Parcel in) {
            boqId = in.readInt();
            boqName = in.readString();
            projectId = in.readInt();
            dprId = in.readInt();
            length = in.readString();
            width = in.readString();
            depth = in.readString();
            total = in.readString();
            quantity = in.readString();
        }

        public static final Creator<DprDatum> CREATOR = new Creator<DprDatum>() {
            @Override
            public DprDatum createFromParcel(Parcel in) {
                return new DprDatum(in);
            }

            @Override
            public DprDatum[] newArray(int size) {
                return new DprDatum[size];
            }
        };

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeInt(boqId);
            dest.writeString(boqName);
            dest.writeInt(projectId);
            dest.writeInt(dprId);
            dest.writeString(length);
            dest.writeString(width);
            dest.writeString(depth);
            dest.writeString(total);
            dest.writeString(quantity);
        }
    }
}
