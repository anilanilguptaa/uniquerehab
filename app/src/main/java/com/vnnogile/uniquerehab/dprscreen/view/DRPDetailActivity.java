package com.vnnogile.uniquerehab.dprscreen.view;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.RecyclerView;

import com.airbnb.lottie.LottieAnimationView;
import com.vnnogile.uniquerehab.R;
import com.vnnogile.uniquerehab.dprscreen.adapter.BOQDetailAdapter;
import com.vnnogile.uniquerehab.dprscreen.controller.BOQDetailController;
import com.vnnogile.uniquerehab.dprscreen.model.BOQModel;
import com.vnnogile.uniquerehab.dprscreen.model.SearchModel;
import com.vnnogile.uniquerehab.dprscreen.model.boqRequestBody;
import com.vnnogile.uniquerehab.global.Constant;
import com.vnnogile.uniquerehab.global.CustomCalendar;
import com.vnnogile.uniquerehab.global.DecimalValueFilter;
import com.vnnogile.uniquerehab.global.MyApplication;
import com.vnnogile.uniquerehab.global.PreferenceManager;
import com.vnnogile.uniquerehab.global.SwipeToDeleteCallback;
import com.vnnogile.uniquerehab.global.UniqueUtils;
import com.vnnogile.uniquerehab.loginscreen.model.LoginResponse;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class DRPDetailActivity extends AppCompatActivity implements BOQDetailController.BOQDetailListener,
        BOQDetailAdapter.BOQDetailListener, TextWatcher {

    public static final String TAG = DRPDetailActivity.class.getSimpleName();
    @BindView(R.id.toolbar_menu)
    ImageView toolbarMenu;
    @BindView(R.id.toolbar_title)
    TextView toolbarTitle;
    @BindView(R.id.toolbar_profile)
    ImageView toolbarProfile;
    @BindView(R.id.txt_date)
    TextView txtDate;
    @BindView(R.id.txt_time)
    TextView txtTime;
    @BindView(R.id.txt_boq_name)
    TextView txtBoqName;
    @BindView(R.id.txt_remark)
    TextView txtRemark;
    @BindView(R.id.status)
    LottieAnimationView status;
    @BindView(R.id.edit_qty)
    EditText editQty;
    @BindView(R.id.edit_length)
    EditText editLength;
    @BindView(R.id.edit_width)
    EditText editWidth;
    @BindView(R.id.edit_depth)
    EditText editDepth;
    @BindView(R.id.edit_remark)
    EditText edit_remark;
    @BindView(R.id.edit_unit)
    EditText edit_unit;
    @BindView(R.id.btn_add)
    Button btnAdd;
    @BindView(R.id.boq_rv)
    RecyclerView boqRv;
    @BindView(R.id.progress_circular)
    ProgressBar progressCircular;
    @BindView(R.id.toolbar_project)
    TextView toolbarProject;


    private String date = "";
    private boolean isDataChange = false;

    private String boqId = "";
    private BOQDetailController controller;
    private SearchModel searchModel;
    private BOQDetailAdapter detailAdapter;
    private List<BOQModel> boqModels = new ArrayList<>();
    private com.vnnogile.uniquerehab.global.ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_drpdetail);
        ButterKnife.bind(this);
        init();
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent();
        intent.putExtra("isDataChange", isDataChange);
        setResult(Activity.RESULT_OK, intent);
        finish();
    }

    private void init() {
        controller = new BOQDetailController(this);

        searchModel = Objects.requireNonNull(getIntent().getExtras()).getParcelable("data");
        boqId = String.valueOf(searchModel.id);

        date = UniqueUtils.getCurrentDate(Constant.YYYY_MM_DD);

        detailAdapter = new BOQDetailAdapter(boqModels, this, true);
        progressBar = new com.vnnogile.uniquerehab.global.ProgressBar(this);


        editQty.setFilters(new InputFilter[]{new DecimalValueFilter(1)});
        editWidth.setFilters(new InputFilter[]{new DecimalValueFilter()});
        editLength.setFilters(new InputFilter[]{new DecimalValueFilter()});
        editDepth.setFilters(new InputFilter[]{new DecimalValueFilter()});

        editLength.addTextChangedListener(this);
        editWidth.addTextChangedListener(this);
        editDepth.addTextChangedListener(this);
        editQty.addTextChangedListener(this);

        edit_unit.setFocusable(false);

        toolbarTitle.setText("Daily Project Report");
        toolbarProject.setText(MyApplication.getInstance().getProjectName());
        toolbarMenu.setImageResource(R.drawable.ic_back);
        toolbarProfile.setImageResource(R.drawable.profile_picture);

        txtDate.setText(new SimpleDateFormat("dd MMM yyyy", new Locale("en", "in")).format(System.currentTimeMillis()));
        txtTime.setText(new SimpleDateFormat("HH:mm", new Locale("en", "in")).format(System.currentTimeMillis()));

        txtBoqName.setText(UniqueUtils.removeChar(searchModel.name));
        txtRemark.setText(UniqueUtils.removeChar(searchModel.description));
        edit_unit.setText(searchModel.unit_rate);

        boqRv.setAdapter(detailAdapter);
        enableSwipeToDelete();
        getBOQDetails();
        updateStatus();
    }

    public void updateStatus() {

        if (searchModel.status.equalsIgnoreCase("not started")) {
            status.pauseAnimation();
        } else if (searchModel.status.equalsIgnoreCase("started")) {
            status.playAnimation();
            status.setRepeatCount(-1);//-1 means infinite
        } else {
           status.setAnimation(R.raw.check);
           status.playAnimation();
        }
    }

    private void getBOQDetails() {
        progressCircular.setVisibility(View.VISIBLE);
        controller.getData(boqId, date);
    }

    private void clear() {
        editWidth.setText("");
        editLength.setText("");
        editQty.setText("1");
        editDepth.setText("0");
        edit_remark.setText("");
        // edit_unit.setText("");
    }

    private void enableSwipeToDelete() {
        SwipeToDeleteCallback swipeToDeleteCallback = new SwipeToDeleteCallback(this) {
            @Override
            public void onSwiped(@NonNull RecyclerView.ViewHolder viewHolder, int i) {

                final int position = viewHolder.getAdapterPosition();

                BOQModel model = detailAdapter.getData().get(position);
                detailAdapter.removeItem(position);
                progressBar.show();
                controller.deleteBOQ(model.id);
            }
        };

        ItemTouchHelper itemTouchhelper = new ItemTouchHelper(swipeToDeleteCallback);
        itemTouchhelper.attachToRecyclerView(boqRv);
    }

    @OnClick({R.id.toolbar_menu, R.id.toolbar_profile})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.toolbar_menu:
                onBackPressed();
                break;
            case R.id.toolbar_profile:
                break;
        }
    }

    @OnClick(R.id.btn_add)
    public void onViewClicked() {

        if (TextUtils.isEmpty(editQty.getText().toString().trim())) {
            UniqueUtils.showWarningMessage("Enter Quantity.");
        } else if (editQty.getText().toString().trim().equals("0")) {
            UniqueUtils.showWarningMessage("Quantity cannot be zero");
        } else if (Double.parseDouble(editQty.getText().toString().trim()) < 0.0001 ||
                Double.parseDouble(editQty.getText().toString().trim()) > 100) {
            UniqueUtils.showWarningMessage("Please enter quantity between 0.0001 to 100");
        } else if (TextUtils.isEmpty(editLength.getText().toString().trim())) {
            UniqueUtils.showWarningMessage("Enter Length");
        } else if (Double.parseDouble(editLength.getText().toString().trim()) < 0.0001 ||
                Double.parseDouble(editLength.getText().toString().trim()) > 100) {
            UniqueUtils.showWarningMessage("Please enter length between 0.0001 to 100");
        } else if (TextUtils.isEmpty(editWidth.getText().toString().trim())) {
            UniqueUtils.showWarningMessage("Enter Width");
        } else if (Double.parseDouble(editWidth.getText().toString().trim()) < 0.0001 ||
                Double.parseDouble(editWidth.getText().toString().trim()) > 100) {
            UniqueUtils.showWarningMessage("Please enter width between 0.0001 to 100");
        } else if (TextUtils.isEmpty(editDepth.getText().toString().trim())) {
            UniqueUtils.showWarningMessage("Enter Depth");
        } else if (Double.parseDouble(editDepth.getText().toString().trim()) < 0 ||
                Double.parseDouble(editDepth.getText().toString().trim()) > 100) {
            UniqueUtils.showWarningMessage("Please enter depth between 0 to 100");
        } else {


            double length = Double.parseDouble(editLength.getText().toString().trim());
            double depth = Double.parseDouble(editDepth.getText().toString().trim());
            double width = Double.parseDouble(editWidth.getText().toString().trim());
            double qty = Double.parseDouble(editQty.getText().toString().trim());

            LoginResponse loginResponse = PreferenceManager.getInstance().getUserDetails();
            boqRequestBody body = new boqRequestBody();
            body.name = UniqueUtils.removeChar(searchModel.name);
            body.projectId = searchModel.projectId;
            body.boq_id = searchModel.id;
            body.UserId = loginResponse.userDetail.id;
            boqRequestBody.Size size = new boqRequestBody.Size();
            size.length = String.valueOf(length);
            size.depth = String.valueOf(depth);
            size.quantity = editQty.getText().toString().trim();
            size.width = String.valueOf(width);
            size.createdDate = UniqueUtils.getCurrentDate(Constant.YYYY_MM_DD_HH_MM_SS);
            size.remark = edit_remark.getText().toString().trim();


            if (depth == 0 || depth == 0.0) {
                depth = 1;
            }


            double total = (length * depth);
            double total1 = (width * qty);
            double total2 = total1 * total;
            BigDecimal d = new BigDecimal((total2));
            Log.e(TAG, "onViewClicked: " + d);
            size.total = String.format("%.6f", d);

            body.sizes.add(size);
            progressBar.show();
            controller.addBoq(body);

        }

    }

    @Override
    public void onSuccessBOQDetail(List<BOQModel> boqModels) {
        progressCircular.setVisibility(View.GONE);
        this.boqModels.clear();
        this.boqModels.addAll(boqModels);
        detailAdapter.notifyDataSetChanged();
        detailAdapter.updateUnitRate(searchModel.unit_rate);
        if (boqModels.size() == 0) {
            UniqueUtils.showWarningMessage(Constant.NO_DATA_FOUND);
        }

    }

    @Override
    public void onDeleteSuccess(String status) {
        if (progressBar.isShowing())
            progressBar.dismiss();
        isDataChange = true;
        searchModel.status = status;
        updateStatus();
        // UniqueUtils.showSuccessMessage("Successfully Deleted.");
        getBOQDetails();
    }

    @Override
    public void onAddSuccess(String boq_status) {
        if (progressBar.isShowing())
            progressBar.dismiss();
        isDataChange = true;
        searchModel.status = boq_status;
        updateStatus();
        clear();
        getBOQDetails();
    }

    @Override
    public void onFailBOQDetail(String errorMessage) {
        progressCircular.setVisibility(View.GONE);
        if (progressBar.isShowing())
            progressBar.dismiss();
        UniqueUtils.showErrorMessage(errorMessage);
    }

    @Override
    public void onDeleteBOQ(BOQModel boqModel) {
        progressBar.show();
        controller.deleteBOQ(boqModel.id);
    }

    @OnClick(R.id.toolbar_profile)
    public void onClicked() {
        UniqueUtils.goToHomePage(this);
    }

    @OnClick({R.id.image_date, R.id.date_lyr})
    public void onDateClicked() {

        if (PreferenceManager.getInstance().getUserRole().equalsIgnoreCase(UniqueUtils.ADMIN)) {
           /* DatePickerDialog datePickerDialog = new DatePickerDialog(this, R.style.DatePicker, new DatePickerDialog.OnDateSetListener() {
                @Override
                public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                    mYear = year;
                    mMonth = month;
                    mDay = dayOfMonth;
                    selectedDate.set(mYear, mMonth, mDay);
                    txtDate.setText(UniqueUtils.getDateInFormat(selectedDate.getTime(), "dd MMM yyyy"));
                    date = UniqueUtils.getDateInFormat(selectedDate.getTime(), "yyyy-MM-dd");
                   getBOQDetails();
                    //api call
                }
            }, mYear, mMonth, mDay);
            datePickerDialog.getDatePicker().setMaxDate(new Date().getTime());
            datePickerDialog.show();*/

            CustomCalendar customCalendar = new CustomCalendar(this, "dpr");
            customCalendar.setListener(new CustomCalendar.CustomCalendarListener() {
                @Override
                public void onDateSet(Date selectedDate) {
                    txtDate.setText(UniqueUtils.getDateInFormat(selectedDate, "dd MMM yyyy"));
                    date = UniqueUtils.getDateInFormat(selectedDate, Constant.YYYY_MM_DD);
                    getBOQDetails();
                }
            });
            customCalendar.show();
        }
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {

    }

    @Override
    public void afterTextChanged(Editable s) {
        if (s.length() == 1) {
            if (s == editLength.getEditableText()) {
                if (s.charAt(0) == '.' || s.toString().trim().equals("00")) {
                    editLength.setText("1");
                    editLength.setSelection(1);
                }
            } else if (s == editWidth.getEditableText()) {
                if (s.charAt(0) == '.' || s.toString().trim().equals("00")) {
                    editWidth.setText("0");
                    editWidth.setSelection(1);
                }
            } else if (s == editDepth.getEditableText()) {
                if (s.charAt(0) == '.') {
                    editDepth.setText("0");
                    editDepth.setSelection(1);
                }
            } else if (s == editQty.getEditableText()) {
                if (s.toString().trim().equals("00") || s.charAt(0) == '.') {
                    editQty.setText("0");
                    editQty.setSelection(1);
                }
            }

        }
    }
}
