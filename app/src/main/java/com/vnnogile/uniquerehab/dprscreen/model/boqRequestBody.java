package com.vnnogile.uniquerehab.dprscreen.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class boqRequestBody implements Parcelable {

    @Expose
    @SerializedName("id")
    public int id;
    @SerializedName("boq_name")
    @Expose
    public String name;
    @SerializedName("boq_id")
    @Expose
    public int boq_id;
    @SerializedName("description")
    @Expose
    public String description;
    @SerializedName("user_id")
    @Expose
    public int UserId;
    @SerializedName("project_id")
    @Expose
    public int projectId;
    @SerializedName("dpr")
    @Expose
    public ArrayList<Size> sizes = new ArrayList<>();


    public boqRequestBody() {
    }

    protected boqRequestBody(Parcel in) {
        id = in.readInt();
        name = in.readString();
        boq_id = in.readInt();
        description = in.readString();
        UserId = in.readInt();
        projectId = in.readInt();
        sizes = in.createTypedArrayList(Size.CREATOR);
    }

    public static final Creator<boqRequestBody> CREATOR = new Creator<boqRequestBody>() {
        @Override
        public boqRequestBody createFromParcel(Parcel in) {
            return new boqRequestBody(in);
        }

        @Override
        public boqRequestBody[] newArray(int size) {
            return new boqRequestBody[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeString(name);
        dest.writeInt(boq_id);
        dest.writeString(description);
        dest.writeInt(UserId);
        dest.writeInt(projectId);
        dest.writeTypedList(sizes);
    }


    public static class Size implements Parcelable {
        @SerializedName("length")
        @Expose
        public String length;
        @SerializedName("width")
        @Expose
        public String width;
        @SerializedName("depth")
        @Expose
        public String depth;
        @SerializedName("quantity")
        @Expose
        public String quantity;
        @SerializedName("date")
        @Expose
        public String createdDate;
        @SerializedName("total")
        @Expose
        public String total;
        @SerializedName("remark")
        @Expose
        public String remark;

        public Size() {
        }

        protected Size(Parcel in) {
            length = in.readString();
            width = in.readString();
            depth = in.readString();
            quantity = in.readString();
            createdDate = in.readString();
            total = in.readString();
            remark = in.readString();
        }

        public static final Creator<Size> CREATOR = new Creator<Size>() {
            @Override
            public Size createFromParcel(Parcel in) {
                return new Size(in);
            }

            @Override
            public Size[] newArray(int size) {
                return new Size[size];
            }
        };

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeString(length);
            dest.writeString(width);
            dest.writeString(depth);
            dest.writeString(quantity);
            dest.writeString(createdDate);
            dest.writeString(total);
            dest.writeString(remark);
        }
    }
}
