package com.vnnogile.uniquerehab.dprscreen.controller;

import android.content.Context;
import android.util.Log;

import com.vnnogile.uniquerehab.dprscreen.model.SearchModel;
import com.vnnogile.uniquerehab.global.Constant;
import com.vnnogile.uniquerehab.global.MyApplication;
import com.vnnogile.uniquerehab.global.PreferenceManager;
import com.vnnogile.uniquerehab.loginscreen.model.LoginResponse;
import com.vnnogile.uniquerehab.retrofit.ApiClient;
import com.vnnogile.uniquerehab.retrofit.ApiInterface;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SearchController {

    public static final String TAG = SearchController.class.getSimpleName();

    private Context context;
    private ApiInterface apiInterface;
    private SearchListener listener;

    public SearchController(Context context) {
        this.context = context;
        apiInterface = ApiClient.getClient().create(ApiInterface.class);
        listener = (SearchListener) context;
    }

    public void search(String key) {

        Call<List<SearchModel>> stringCall = apiInterface.getSearch(PreferenceManager.getInstance().getToken(),
               PreferenceManager.getInstance().getUserId(),
               MyApplication.getInstance().getProjectId(), key);
        stringCall.enqueue(new Callback<List<SearchModel>>() {
            @Override
            public void onResponse(Call<List<SearchModel>> call, Response<List<SearchModel>> response) {
              //  Log.e(TAG, "onResponse: res" + response.body().size());

                if (response.isSuccessful()) {
                    if (response.body()!=null)
                    listener.onSuccessSearch(response.body());
                    else
                        listener.onFailSearch("No Data Found");
                } else {
                    listener.onFailSearch(Constant.SERVER_ERROR);
                }
            }

            @Override
            public void onFailure(Call<List<SearchModel>> call, Throwable t) {
                stringCall.cancel();
                Log.e(TAG, "onFailure: " + t.toString());
                listener.onFailSearch(Constant.SERVER_ERROR);
            }
        });

    }

    public interface SearchListener {
        void onSuccessSearch(List<SearchModel> searchModels);

        void onFailSearch(String message);
    }
}
