package com.vnnogile.uniquerehab.dprscreen.view;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.vnnogile.uniquerehab.R;
import com.vnnogile.uniquerehab.dprscreen.adapter.BOQAdapter;
import com.vnnogile.uniquerehab.dprscreen.controller.SearchController;
import com.vnnogile.uniquerehab.dprscreen.model.SearchModel;
import com.vnnogile.uniquerehab.global.Constant;
import com.vnnogile.uniquerehab.global.MyApplication;
import com.vnnogile.uniquerehab.global.PreferenceManager;
import com.vnnogile.uniquerehab.global.ProgressBar;
import com.vnnogile.uniquerehab.global.UniqueUtils;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class DPRActivity extends AppCompatActivity implements TextWatcher, SearchController.SearchListener,
        TextView.OnEditorActionListener, BOQAdapter.BOQAdapterListener {

    public static final String TAG = DPRActivity.class.getSimpleName();
    @BindView(R.id.toolbar_menu)
    ImageView toolbarMenu;
    @BindView(R.id.toolbar_profile)
    ImageView toolbarProfile;
    @BindView(R.id.image_date)
    ImageView imageDate;
    @BindView(R.id.txt_date)
    TextView txtDate;
    @BindView(R.id.image_time)
    ImageView imageTime;
    @BindView(R.id.txt_time)
    TextView txtTime;
    @BindView(R.id.search_bar)
    EditText searchBar;
    @BindView(R.id.search_icon)
    ImageView searchIcon;
    @BindView(R.id.toolbar_title)
    TextView toolbarTitle;
    @BindView(R.id.boq_rv)
    RecyclerView boqRv;
    @BindView(R.id.toolbar_project)
    TextView toolbarProject;

    private BOQAdapter boqAdapter;
    private ProgressBar progressBar;
    private List<SearchModel> searchModels = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dpr);
        ButterKnife.bind(this);

        loadVariables();
    }

    private void loadVariables() {
        progressBar = new ProgressBar(this);
        toolbarProfile.setImageResource(R.drawable.profile_picture);
        boqAdapter = new BOQAdapter(searchModels, this);
        boqAdapter.setListener(this);

        date = UniqueUtils.getCurrentDate(Constant.YYYY_MM_DD);

        toolbarProject.setText(MyApplication.getInstance().getProjectName());
        toolbarTitle.setText("Daily Project Report");
        txtDate.setText(new SimpleDateFormat("dd MMM yyyy", new Locale("en", "in")).format(System.currentTimeMillis()));
        txtTime.setText(new SimpleDateFormat("HH:mm", new Locale("en", "in")).format(System.currentTimeMillis()));
        searchBar.setHint("Search BOQ No. or Activity");
        searchBar.addTextChangedListener(this);
        searchBar.setOnEditorActionListener(this);
        boqRv.setAdapter(boqAdapter);
    }

    @OnClick({R.id.image_date, R.id.image_time, R.id.search_icon})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.image_date:
                break;
            case R.id.image_time:
                break;
            case R.id.search_icon:
                break;
        }
    }

    private void search() {
        SearchController searchController = new SearchController(this);
        searchController.search(searchBar.getText().toString());
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {

    }

    @Override
    public void afterTextChanged(Editable s) {
        if (s.length() >= 3) {
            search();
        }
    }

    @Override
    public void onSuccessSearch(List<SearchModel> searchModels) {
        if (progressBar.isShowing())
            progressBar.dismiss();
        this.searchModels.clear();
        this.searchModels.addAll(searchModels);
        boqAdapter.notifyDataSetChanged();
    }

    @Override
    public void onFailSearch(String message) {

    }


    @Override
    public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {

        if (actionId == EditorInfo.IME_ACTION_SEARCH) {
            UniqueUtils.hideKeyboard(DPRActivity.this);
        }
        return false;
    }

    @OnClick(R.id.toolbar_profile)
    public void onViewClicked() {
        UniqueUtils.goToHomePage(this);
    }

    @OnClick(R.id.toolbar_menu)
    public void onMenuClicked() {
        onBackPressed();
    }


    private Calendar calendar = Calendar.getInstance();
    private Calendar selectedDate = Calendar.getInstance();
    private int mYear = calendar.get(Calendar.YEAR);
    private int mMonth = calendar.get(Calendar.MONTH);
    private int mDay = calendar.get(Calendar.DAY_OF_MONTH);


    private String date = "";

    @OnClick(R.id.image_date)
    public void onDateClicked() {

        if (PreferenceManager.getInstance().getUserRole().equalsIgnoreCase(UniqueUtils.ADMIN)) {
           /* DatePickerDialog datePickerDialog = new DatePickerDialog(this, R.style.DatePicker, new DatePickerDialog.OnDateSetListener() {
                @Override
                public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                    mYear = year;
                    mMonth = month;
                    mDay = dayOfMonth;
                    selectedDate.set(mYear, mMonth, mDay);
                    txtDate.setText(UniqueUtils.getDateInFormat(selectedDate.getTime(), "dd MMM yyyy"));
                    date = UniqueUtils.getDateInFormat(selectedDate.getTime(), "yyyy-MM-dd");

                    //api call
                }
            }, mYear, mMonth, mDay);
            datePickerDialog.getDatePicker().setMaxDate(new Date().getTime());
            datePickerDialog.show();*/
        }
    }

    @Override
    public void onItemClick(SearchModel model) {
        Intent intent = new Intent(this, DRPDetailActivity.class);
        Bundle bundle = new Bundle();
        bundle.putParcelable("data", model);
        intent.putExtras(bundle);
        startActivityForResult(intent, 100);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 100 && resultCode == Activity.RESULT_OK && data != null) {
            if (data.getBooleanExtra("isDataChange", false)) {
                progressBar.show();
                search();
            }
        }
    }
}
