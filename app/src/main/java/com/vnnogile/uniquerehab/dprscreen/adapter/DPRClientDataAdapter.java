package com.vnnogile.uniquerehab.dprscreen.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.PopupMenu;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.airbnb.lottie.LottieAnimationView;
import com.vnnogile.uniquerehab.R;
import com.vnnogile.uniquerehab.dprscreen.model.DPRModel;
import com.vnnogile.uniquerehab.global.PreferenceManager;
import com.vnnogile.uniquerehab.global.UniqueUtils;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class DPRClientDataAdapter extends RecyclerView.Adapter<DPRClientDataAdapter.DPRClientView> {

    private Context context;
    private List<DPRModel> dprModels;
    private DPRClientAdapterListener listener;

    public DPRClientDataAdapter(Context context, List<DPRModel> dprModels) {
        this.context = context;
        this.dprModels = dprModels;
    }

    public void setListener(DPRClientAdapterListener listener) {
        this.listener = listener;
    }

    public interface DPRClientAdapterListener{
        void onStatusChange(String status,int id,int position);
    }

    public void updateData(int position,String status){
        DPRModel  dprModel = dprModels.get(position);
        dprModel.status = status;
        dprModels.set(position,dprModel);
        notifyItemChanged(position);
    }

    @NonNull
    @Override
    public DPRClientView onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.cell_client_dpr, parent, false);
        return new DPRClientView(view);
    }

    @Override
    public void onBindViewHolder(@NonNull DPRClientView holder, int position) {

        DPRModel dprModel = dprModels.get(position);

        holder.txtBoqName.setText(dprModel.boqName);


        if (dprModel.status.equalsIgnoreCase("not started")){
            holder.status.pauseAnimation();
        }else if (dprModel.status.equalsIgnoreCase("started")){
            holder.status.playAnimation();
            holder.status.setRepeatCount(-1);
        }else {
            holder.status.setAnimation(R.raw.check);
            holder.status.playAnimation();
            //  holder.status.pauseAnimation();
        }

        holder.dprRv.setAdapter(new BOQDetailAdapter(dprModel.dprData,null,false));

        if (dprModel.dprData.size()>0){
            holder.more.setOnClickListener(v->{
                PopupMenu popup = new PopupMenu(context, holder.more);
                //inflating menu from xml resource
                popup.inflate(R.menu.dpr_menu);
                //adding click listener
                popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        switch (item.getItemId()) {
                           /* case R.id.not_started:
                                if (listener!=null)
                                    listener.onStatusChange("not started",dprModel.id,position);
                                return true;*/
                            case R.id.started:
                                if (listener!=null)
                                    listener.onStatusChange("started",dprModel.id,position);
                                return true;
                            case R.id.completed:
                                if (listener!=null)
                                    listener.onStatusChange("completed",dprModel.id,position);
                                return true;
                            default:
                                return false;
                        }
                    }
                });
                //displaying the popup
                popup.show();
            });
        }

    }

    @Override
    public int getItemCount() {
        return dprModels.size();
    }

    class DPRClientView extends RecyclerView.ViewHolder {
        @BindView(R.id.txt_boq_name)
        TextView txtBoqName;
        @BindView(R.id.status)
        LottieAnimationView status;
        @BindView(R.id.more)
        ImageView more;
        @BindView(R.id.dpr_rv)
        RecyclerView dprRv;

        DPRClientView(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            dprRv.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false));
            if (PreferenceManager.getInstance().getUserRole().equalsIgnoreCase(UniqueUtils.ADMIN)){
                more.setVisibility(View.VISIBLE);
            }
        }
    }

}
