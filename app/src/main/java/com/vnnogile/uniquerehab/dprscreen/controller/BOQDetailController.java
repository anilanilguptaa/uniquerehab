package com.vnnogile.uniquerehab.dprscreen.controller;

import android.content.Context;
import android.util.Log;

import com.google.gson.Gson;
import com.vnnogile.uniquerehab.dprscreen.model.BOQModel;
import com.vnnogile.uniquerehab.global.Constant;
import com.vnnogile.uniquerehab.global.MyApplication;
import com.vnnogile.uniquerehab.global.PreferenceManager;
import com.vnnogile.uniquerehab.dprscreen.model.boqRequestBody;
import com.vnnogile.uniquerehab.global.UniqueUtils;
import com.vnnogile.uniquerehab.loginscreen.model.LoginResponse;
import com.vnnogile.uniquerehab.retrofit.ApiClient;
import com.vnnogile.uniquerehab.retrofit.ApiInterface;

import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class BOQDetailController {

    public static final String TAG = BOQDetailController.class.getSimpleName();

    private Context context;
    private ApiInterface apiInterface;
    private LoginResponse loginResponse;
    private BOQDetailListener listener;

    public BOQDetailController(Context context) {
        this.context = context;
        listener = (BOQDetailListener) context;
        apiInterface = ApiClient.getClient().create(ApiInterface.class);
        loginResponse = PreferenceManager.getInstance().getUserDetails();
    }

    public void getData(String id,String date) {

        Call<List<BOQModel>> objectCall = apiInterface.getBOQDetails(
                PreferenceManager.getInstance().getToken(),
                String.valueOf(loginResponse.userDetail.id),
                String.valueOf(MyApplication.getInstance().getProjectId()),
                id, date/*"2020-05-04"*/);

        objectCall.enqueue(new Callback<List<BOQModel>>() {
            @Override
            public void onResponse(Call<List<BOQModel>> call, Response<List<BOQModel>> response) {
              //  Log.e(TAG, "onResponse: " + response.body().size());

                if (response.isSuccessful()) {
                    if (response.body()!=null)
                    listener.onSuccessBOQDetail(response.body());
                    else
                        listener.onFailBOQDetail(Constant.NO_DATA_FOUND);
                } else {
                    listener.onFailBOQDetail(Constant.SERVER_ERROR);
                }
            }

            @Override
            public void onFailure(Call<List<BOQModel>> call, Throwable t) {
                Log.e(TAG, "onFailure: " + t.toString());
                listener.onFailBOQDetail(Constant.SERVER_ERROR);
            }
        });
    }


    public void deleteBOQ(int id) {
        boqRequestBody boqRequestBody = new boqRequestBody();
        boqRequestBody.id = id;
        boqRequestBody.UserId =Integer.parseInt(PreferenceManager.getInstance().getUserId());
        boqRequestBody.projectId =  Integer.parseInt(MyApplication.getInstance().getProjectId());

        Call<Object> objectCall = apiInterface.deleteBOQ(PreferenceManager.getInstance().getToken(), boqRequestBody);

        objectCall.enqueue(new Callback<Object>() {

            @Override
            public void onResponse(Call<Object> call, Response<Object> response) {
                if (response.isSuccessful()) {

                    String data = new Gson().toJson(response.body());

                    try {
                        JSONObject jsonObject = new JSONObject(data);

                        if (jsonObject.has("status")){
                            if (jsonObject.getBoolean("status")){
                                UniqueUtils.showSuccessMessage(jsonObject.getString("message"));
                            }else {
                                UniqueUtils.showErrorMessage(jsonObject.getString("message"));
                            }
                        }else {
                            UniqueUtils.showSuccessMessage(jsonObject.getString("message"));
                        }
                        String status="";
                        if (jsonObject.has("boq_status"))
                            status = jsonObject.getString("boq_status");
                        listener.onDeleteSuccess(status);
                    }catch (Exception e){
                        e.printStackTrace();
                        listener.onFailBOQDetail(Constant.SOMETHING_WRONG);
                    }
                } else {
                    listener.onFailBOQDetail(Constant.SERVER_ERROR);
                }
            }

            @Override
            public void onFailure(Call<Object> call, Throwable t) {
                Log.e(TAG, "onFailure: " + t.toString());
                listener.onFailBOQDetail(Constant.SERVER_ERROR);
            }
        });

    }

    public void addBoq(boqRequestBody requestBody){
        Call<Object> objectCall = apiInterface.addBOQ(PreferenceManager.getInstance().getToken(), requestBody);

        objectCall.enqueue(new Callback<Object>() {

            @Override
            public void onResponse(Call<Object> call, Response<Object> response) {
                if (response.isSuccessful()) {
                    String data = new Gson().toJson(response.body());

                    try {
                        JSONObject jsonObject = new JSONObject(data);

                        if (jsonObject.has("status")){
                            if (jsonObject.getBoolean("status")){
                                UniqueUtils.showSuccessMessage(jsonObject.getString("message"));
                            }else {
                                UniqueUtils.showErrorMessage(jsonObject.getString("message"));
                            }
                        }else {
                            UniqueUtils.showSuccessMessage(jsonObject.getString("message"));
                        }
                        String status="";
                        if (jsonObject.has("boq_status"))
                            status = jsonObject.getString("boq_status");
                        listener.onAddSuccess(status);
                    }catch (Exception e){
                        e.printStackTrace();
                        listener.onFailBOQDetail(Constant.SOMETHING_WRONG);
                    }

                } else {
                    listener.onFailBOQDetail(Constant.SERVER_ERROR);
                }
            }

            @Override
            public void onFailure(Call<Object> call, Throwable t) {
                Log.e(TAG, "onFailure: " + t.toString());
                listener.onFailBOQDetail(Constant.SERVER_ERROR);
            }
        });
    }

    public interface BOQDetailListener {
        void onSuccessBOQDetail(List<BOQModel> boqModels);

        void onDeleteSuccess(String boq_status);

        void onAddSuccess(String status);

        void onFailBOQDetail(String errorMessage);
    }
}
