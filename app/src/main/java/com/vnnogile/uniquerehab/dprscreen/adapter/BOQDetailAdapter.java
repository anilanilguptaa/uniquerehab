package com.vnnogile.uniquerehab.dprscreen.adapter;

import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.vnnogile.uniquerehab.R;
import com.vnnogile.uniquerehab.dprscreen.model.BOQModel;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class BOQDetailAdapter extends RecyclerView.Adapter<BOQDetailAdapter.BOQDViewHolder> {

    private List<BOQModel> boqModels;
    private BOQDetailListener listener;
    private boolean isDelete = false;

    public BOQDetailAdapter(List<BOQModel> boqModels, BOQDetailListener listener, boolean isDelete) {
        this.boqModels = boqModels;
        this.listener = listener;
        this.isDelete = isDelete;
    }

    public interface BOQDetailListener {
        void onDeleteBOQ(BOQModel boqModel);
    }

    public void updateUnitRate(String unit){
        for (int i = 0; i < boqModels.size(); i++) {
            BOQModel model=boqModels.get(i);
            model.boq_unit=unit;
            boqModels.set(i,model);
        }
       notifyDataSetChanged();
    }

    @NonNull
    @Override
    public BOQDViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.cell_boq_details, parent, false);
        return new BOQDViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull BOQDViewHolder holder, int position) {

        BOQModel model = boqModels.get(position);

        holder.txtDepth.setText(Html.fromHtml("<font color=#111111><strong>Depth: </strong></font>") + model.depth);
        holder.txtQty.setText(Html.fromHtml("<font color=#111111><strong>Qty: </strong></font>") + "" + model.quantity);
        holder.txtLength.setText(Html.fromHtml("<font color=#111111><strong>Length: </strong></font>") + model.length);
        holder.txtBoqName.setText("Total: " + model.total);
        holder.txtWidth.setText(Html.fromHtml("<font color=#111111><strong>Width: </strong></font>") + model.width);
        holder.txt_remark.setText(Html.fromHtml("<font color=#111111><strong>Remark: </strong></font>") + model.remark);
        holder.txt_unit.setText(Html.fromHtml("<font color=#111111><strong>Unit: </strong></font>")+model.boq_unit);

        holder.deleteContainer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (listener != null)
                    listener.onDeleteBOQ(model);
            }
        });

    }

    @Override
    public int getItemCount() {
        return boqModels.size();
    }

    public void removeItem(int position) {
        boqModels.remove(position);
        notifyItemRemoved(position);
    }


    public List<BOQModel> getData() {
        return boqModels;
    }

    class BOQDViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.image_delete)
        ImageView imageDelete;
        @BindView(R.id.delete_container)
        RelativeLayout deleteContainer;
        @BindView(R.id.txt_qty)
        TextView txtQty;
        @BindView(R.id.txt_length)
        TextView txtLength;
        @BindView(R.id.txt_width)
        TextView txtWidth;
        @BindView(R.id.txt_depth)
        TextView txtDepth;
        @BindView(R.id.txt_boq_name)
        TextView txtBoqName;
        @BindView(R.id.txt_remark)
        TextView txt_remark;
        @BindView(R.id.txt_unit)
        TextView txt_unit;

        BOQDViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

            if (isDelete) {
                deleteContainer.setVisibility(View.VISIBLE);
            } else {
                deleteContainer.setVisibility(View.GONE);
            }

        }
    }
}
