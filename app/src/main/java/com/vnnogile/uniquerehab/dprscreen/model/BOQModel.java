package com.vnnogile.uniquerehab.dprscreen.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class BOQModel implements Parcelable {

    @SerializedName("id")
    @Expose
    public int id;
    @SerializedName("project_activity_id")
    @Expose
    public int projectActivityId;
    @SerializedName("project_id")
    @Expose
    public int projectId;
    @SerializedName("length")
    @Expose
    public String length;
    @SerializedName("width")
    @Expose
    public String width;
    @SerializedName("height")
    @Expose
    public String height;
    @SerializedName("depth")
    @Expose
    public String depth;
    @SerializedName("quantity")
    @Expose
    public String quantity;
    @SerializedName("boq_id")
    @Expose
    public int boqId;
    @SerializedName("user_id")
    @Expose
    public int userId;
    @SerializedName("is_active")
    @Expose
    public String isActive;
    @SerializedName("created_date")
    @Expose
    public String createdDate;
    @SerializedName("total")
    @Expose
    public String total;
    @SerializedName("dpr_id")
    @Expose
    public int dprId;
    @SerializedName("remark")
    @Expose
    public String remark;
    @SerializedName("boq_unit")
    @Expose
    public String boq_unit;


    protected BOQModel(Parcel in) {
        id = in.readInt();
        projectActivityId = in.readInt();
        projectId = in.readInt();
        length = in.readString();
        width = in.readString();
        height = in.readString();
        depth = in.readString();
        quantity = in.readString();
        boqId = in.readInt();
        userId = in.readInt();
        isActive = in.readString();
        createdDate = in.readString();
        total = in.readString();
        dprId = in.readInt();
        remark = in.readString();
        boq_unit = in.readString();
    }

    public static final Creator<BOQModel> CREATOR = new Creator<BOQModel>() {
        @Override
        public BOQModel createFromParcel(Parcel in) {
            return new BOQModel(in);
        }

        @Override
        public BOQModel[] newArray(int size) {
            return new BOQModel[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeInt(projectActivityId);
        dest.writeInt(projectId);
        dest.writeString(length);
        dest.writeString(width);
        dest.writeString(height);
        dest.writeString(depth);
        dest.writeString(quantity);
        dest.writeInt(boqId);
        dest.writeInt(userId);
        dest.writeString(isActive);
        dest.writeString(createdDate);
        dest.writeString(total);
        dest.writeInt(dprId);
        dest.writeString(remark);
        dest.writeString(boq_unit);
    }
}
