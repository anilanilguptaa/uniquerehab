package com.vnnogile.uniquerehab.dprscreen.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SearchModel implements Parcelable {
    @SerializedName("id")
    @Expose
    public int id;
    @SerializedName("name")
    @Expose
    public String name ="";
    @SerializedName("description")
    @Expose
    public String description ="";
    @SerializedName("client_user_id")
    @Expose
    public int clientUserId;
    @SerializedName("project_id")
    @Expose
    public int projectId;
    @SerializedName("created_date")
    @Expose
    public String createdDate;
    @SerializedName("status")
    @Expose
    public String status;
    @SerializedName("srno")
    @Expose
    public String srno;
    @SerializedName("seq_no")
    @Expose
    public int seq_no;
    @SerializedName("unit_rate")
    @Expose
    public String unit_rate;


    protected SearchModel(Parcel in) {
        id = in.readInt();
        name = in.readString();
        description = in.readString();
        clientUserId = in.readInt();
        projectId = in.readInt();
        createdDate = in.readString();
        status = in.readString();
        srno = in.readString();
        seq_no = in.readInt();
        unit_rate = in.readString();
    }

    public static final Creator<SearchModel> CREATOR = new Creator<SearchModel>() {
        @Override
        public SearchModel createFromParcel(Parcel in) {
            return new SearchModel(in);
        }

        @Override
        public SearchModel[] newArray(int size) {
            return new SearchModel[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeString(name);
        dest.writeString(description);
        dest.writeInt(clientUserId);
        dest.writeInt(projectId);
        dest.writeString(createdDate);
        dest.writeString(status);
        dest.writeString(srno);
        dest.writeInt(seq_no);
        dest.writeString(unit_rate);
    }
}
