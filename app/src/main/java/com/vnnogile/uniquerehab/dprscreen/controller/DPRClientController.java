package com.vnnogile.uniquerehab.dprscreen.controller;

import android.content.Context;
import android.util.Log;

import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import com.vnnogile.uniquerehab.dprscreen.model.DPRModel;
import com.vnnogile.uniquerehab.global.Constant;
import com.vnnogile.uniquerehab.global.MyApplication;
import com.vnnogile.uniquerehab.global.PreferenceManager;
import com.vnnogile.uniquerehab.global.ResponseMessage;
import com.vnnogile.uniquerehab.global.UniqueUtils;
import com.vnnogile.uniquerehab.retrofit.ApiClient;
import com.vnnogile.uniquerehab.retrofit.ApiInterface;

import org.json.JSONObject;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DPRClientController {

    public static final String TAG = DPRClientController.class.getSimpleName();

    private Context context;
    private ApiInterface apiInterface;
    private Listener listener;


    public DPRClientController(Context context) {
        this.context = context;
        listener = (Listener) context;
        apiInterface = ApiClient.getClient().create(ApiInterface.class);
    }

    public interface Listener {
        void onSuccess(List<DPRModel> modelList);

        void onSuccessUpdate();

        void onFailure(String errorMessage);
    }

    public void getClientData(String date) {

        Call<List<DPRModel>> objectCall = apiInterface.getDprDataForClientView(
                PreferenceManager.getInstance().getToken(),
                PreferenceManager.getInstance().getUserId(),
                MyApplication.getInstance().getProjectId(), date
        );

        objectCall.enqueue(new Callback<List<DPRModel>>() {

            @Override
            public void onResponse(Call<List<DPRModel>> call, Response<List<DPRModel>> response) {
                if (response.isSuccessful()) {
                    listener.onSuccess(response.body());
                } else {
                    listener.onFailure(Constant.SERVER_ERROR);
                }
            }

            @Override
            public void onFailure(Call<List<DPRModel>> call, Throwable t) {
                Log.e(TAG, "onFailure() returned: " + t.toString());
                listener.onFailure(Constant.SERVER_ERROR);
            }
        });
    }


    public void updateStatus(String status, int id) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.accumulate("project_id", MyApplication.getInstance().getProjectId());
            jsonObject.accumulate("user_id", PreferenceManager.getInstance().getUserId());
            jsonObject.accumulate("boq_id", id);
            jsonObject.accumulate("date", UniqueUtils.getCurrentDate(Constant.YYYY_MM_DD_HH_MM_SS));
            jsonObject.accumulate("status", status);
        } catch (Exception e) {
            e.printStackTrace();
        }

        JsonElement jsonElement = JsonParser.parseString(jsonObject.toString());

        Call<ResponseMessage> messageCall = apiInterface.updateDPRStatus(
                PreferenceManager.getInstance().getToken(), jsonElement
        );

        messageCall.enqueue(new Callback<ResponseMessage>() {
            @Override
            public void onResponse(Call<ResponseMessage> call, Response<ResponseMessage> response) {
                if (response.isSuccessful()) {
                    if (response.body() != null && response.body().status) {
                        UniqueUtils.showSuccessMessage(response.body().message);
                        listener.onSuccessUpdate();
                    } else {
                      //  UniqueUtils.showErrorMessage(response.body().message);
                        listener.onFailure(response.body().message);
                    }

                } else {
                    listener.onFailure(Constant.SERVER_ERROR);
                }
            }

            @Override
            public void onFailure(Call<ResponseMessage> call, Throwable throwable) {
                Log.e(TAG, "onFailure: " + throwable.toString());
                listener.onFailure(Constant.SERVER_ERROR);
            }
        });
    }
}
