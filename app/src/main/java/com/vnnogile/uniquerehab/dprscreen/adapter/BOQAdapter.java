package com.vnnogile.uniquerehab.dprscreen.adapter;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.airbnb.lottie.LottieAnimationView;
import com.airbnb.lottie.LottieDrawable;
import com.vnnogile.uniquerehab.R;
import com.vnnogile.uniquerehab.dprscreen.model.SearchModel;
import com.vnnogile.uniquerehab.dprscreen.view.DRPDetailActivity;
import com.vnnogile.uniquerehab.global.MyApplication;
import com.vnnogile.uniquerehab.global.UniqueUtils;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class BOQAdapter extends RecyclerView.Adapter<BOQAdapter.BOQViewHolder> {

    private List<SearchModel> searchModels;
    private Context context;
    private BOQAdapterListener listener;


    public BOQAdapter(List<SearchModel> searchModels, Context context) {
        this.searchModels = searchModels;
        this.context = context;
    }

    public void setListener(BOQAdapterListener listener) {
        this.listener = listener;
    }

    public interface BOQAdapterListener{
        void  onItemClick(SearchModel model);
    }

    @NonNull
    @Override
    public BOQViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.cell_boq, parent, false);
        return new BOQViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull BOQViewHolder holder, int position) {
        SearchModel searchModel = searchModels.get(position);
        holder.txtBoqName.setText(UniqueUtils.removeChar(searchModel.name));
        holder.txtRemark.setText(UniqueUtils.removeChar(searchModel.description));

        if (searchModel.status.equalsIgnoreCase("not started")){
            holder.status.setAnimation(R.raw.timer);//not started
            holder.status.pauseAnimation();
        }else if (searchModel.status.equalsIgnoreCase("started")){
            holder.status.setAnimation(R.raw.timer);
            holder.status.playAnimation();
            holder.status.setRepeatCount(-1);
        }else {
            holder.status.setAnimation(R.raw.check);
            holder.status.playAnimation();
          //  holder.status.pauseAnimation();
        }
    }

    @Override
    public int getItemCount() {
        return searchModels.size();
    }

    class BOQViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.indicator)
        TextView indicator;
        @BindView(R.id.txt_boq_name)
        TextView txtBoqName;
        @BindView(R.id.txt_remark)
        TextView txtRemark;
        @BindView(R.id.status)
        LottieAnimationView status;

        BOQViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    SearchModel searchModel = searchModels.get(getAdapterPosition());
                    if (listener!=null){
                        listener.onItemClick(searchModel);
                    }
                }
            });
        }
    }
}