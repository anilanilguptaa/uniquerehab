package com.vnnogile.uniquerehab.dprscreen.view;

import android.app.DatePickerDialog;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.RecyclerView;

import com.vnnogile.uniquerehab.R;
import com.vnnogile.uniquerehab.dprscreen.adapter.DPRClientDataAdapter;
import com.vnnogile.uniquerehab.dprscreen.controller.DPRClientController;
import com.vnnogile.uniquerehab.dprscreen.model.DPRModel;
import com.vnnogile.uniquerehab.global.Constant;
import com.vnnogile.uniquerehab.global.CustomCalendar;
import com.vnnogile.uniquerehab.global.MyApplication;
import com.vnnogile.uniquerehab.global.ProgressBar;
import com.vnnogile.uniquerehab.global.UniqueUtils;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class DPRClientActivity extends AppCompatActivity implements DPRClientController.Listener,
        DPRClientDataAdapter.DPRClientAdapterListener {

    public static final String TAG = DPRClientActivity.class.getSimpleName();
    @BindView(R.id.toolbar_menu)
    ImageView toolbarMenu;
    @BindView(R.id.toolbar_name)
    TextView toolbarName;
    @BindView(R.id.toolbar_profile)
    ImageView toolbarProfile;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.image_date)
    ImageView imageDate;
    @BindView(R.id.txt_date)
    TextView txtDate;
    @BindView(R.id.txt_time)
    TextView txtTime;
    @BindView(R.id.dpr_client_rv)
    RecyclerView dprClientRv;
    @BindView(R.id.txt_no_data)
    TextView txtNoData;
    @BindView(R.id.toolbar_project)
    TextView toolbarProject;



    private String date = "";

    private ProgressBar progressBar;

    private DPRClientController controller;

    private DPRClientDataAdapter dprClientDataAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dprclient);
        ButterKnife.bind(this);
        loadVariables();
    }

    private void loadVariables() {
        toolbarName.setText("Daily Project Report");
        toolbarProject.setText(MyApplication.getInstance().getProjectName());
        txtDate.setText(UniqueUtils.getCurrentDate("dd MMM yyyy"));
        txtTime.setText(UniqueUtils.getCurrentDate("HH:mm"));
        toolbarProfile.setImageResource(R.drawable.profile_picture);
        toolbar.setNavigationIcon(R.drawable.ic_back);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        controller = new DPRClientController(this);
        progressBar = new ProgressBar(this);
        date = UniqueUtils.getCurrentDate(Constant.YYYY_MM_DD);

        if (UniqueUtils.isNetworkAvailable(this)) {
            getData();
        } else {
            UniqueUtils.showErrorMessage(Constant.NETWORK_ERROR_MESSAGE);
        }


    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {

        onBackPressed();

        return super.onOptionsItemSelected(item);
    }

    private void getData() {
        progressBar.show();
        controller.getClientData(date);
    }

    @OnClick({R.id.image_date,R.id.date_lyr})
    public void onViewClicked() {
      /*  DatePickerDialog datePickerDialog = new DatePickerDialog(this, R.style.DatePicker, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                mYear = year;
                mMonth = month;
                mDay = dayOfMonth;
                selectedDate.set(mYear, mMonth, mDay);
                txtDate.setText(UniqueUtils.getDateInFormat(selectedDate.getTime(), "dd MMM yyyy"));
                date = UniqueUtils.getDateInFormat(selectedDate.getTime(), Constant.YYYY_MM_DD);

                //api call
                getData();
            }
        }, mYear, mMonth, mDay);
        datePickerDialog.getDatePicker().setMaxDate(new Date().getTime());
        datePickerDialog.show();*/

        CustomCalendar customCalendar = new CustomCalendar(this,"dpr");
        customCalendar.setListener(new CustomCalendar.CustomCalendarListener() {
            @Override
            public void onDateSet(Date selectedDate) {
                txtDate.setText(UniqueUtils.getDateInFormat(selectedDate, "dd MMM yyyy"));
                date = UniqueUtils.getDateInFormat(selectedDate, Constant.YYYY_MM_DD);
                //api call
                getData();
            }
        });
        customCalendar.show();

    }

    @Override
    public void onSuccess(List<DPRModel> modelList) {
        progressBar.dismiss();

        dprClientDataAdapter = new DPRClientDataAdapter(this, modelList);
        dprClientDataAdapter.setListener(DPRClientActivity.this);

        dprClientRv.setAdapter(dprClientDataAdapter);

        if (modelList.size() == 0) {
            txtNoData.setVisibility(View.VISIBLE);
        } else {
            txtNoData.setVisibility(View.GONE);
        }
    }

    @Override
    public void onSuccessUpdate() {
        progressBar.dismiss();
        if (dprClientDataAdapter!=null){
            dprClientDataAdapter.updateData(position,status);
        }
    }

    @Override
    public void onFailure(String errorMessage) {
        progressBar.dismiss();
        UniqueUtils.showErrorMessage(errorMessage);

    }

    @OnClick(R.id.toolbar_profile)
    public void onClicked() {
        UniqueUtils.goToHomePage(this);
    }

    @OnClick(R.id.toolbar_menu)
    public void onMenuClicked() {
        onBackPressed();
    }

    String status="";
    int position;

    @Override
    public void onStatusChange(String status,int id,int position) {
        this.status = status;
        this.position=position;
        progressBar.show();
        controller.updateStatus(status,id);
    }
}
