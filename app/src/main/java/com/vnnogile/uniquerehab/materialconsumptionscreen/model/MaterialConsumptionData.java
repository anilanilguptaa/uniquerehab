package com.vnnogile.uniquerehab.materialconsumptionscreen.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class MaterialConsumptionData implements Parcelable {

    @SerializedName("material_consumption_data")
    @Expose
    public List<Datum> data = new ArrayList<>();
    @SerializedName("imageData")
    @Expose
    public List<Images> imageData = new ArrayList<>();

    protected MaterialConsumptionData(Parcel in) {
        data = in.createTypedArrayList(Datum.CREATOR);
    }

    public static final Creator<MaterialConsumptionData> CREATOR = new Creator<MaterialConsumptionData>() {
        @Override
        public MaterialConsumptionData createFromParcel(Parcel in) {
            return new MaterialConsumptionData(in);
        }

        @Override
        public MaterialConsumptionData[] newArray(int size) {
            return new MaterialConsumptionData[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeTypedList(data);
    }


    public static class Datum implements Parcelable{
        @SerializedName("header_id")
        @Expose
        public int headerId;
        @SerializedName("detail_id")
        @Expose
        public int detailId;
        @SerializedName("quantity")
        @Expose
        public String quantity;
        @SerializedName("material_name")
        @Expose
        public String materialName;
        @SerializedName("material_id")
        @Expose
        public String material_id;
        @SerializedName("unit")
        @Expose
        public String unit;

        protected Datum(Parcel in) {
            headerId = in.readInt();
            detailId = in.readInt();
            quantity = in.readString();
            materialName = in.readString();
            material_id = in.readString();
            unit = in.readString();
        }

        public static final Creator<Datum> CREATOR = new Creator<Datum>() {
            @Override
            public Datum createFromParcel(Parcel in) {
                return new Datum(in);
            }

            @Override
            public Datum[] newArray(int size) {
                return new Datum[size];
            }
        };

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeInt(headerId);
            dest.writeInt(detailId);
            dest.writeString(quantity);
            dest.writeString(materialName);
            dest.writeString(material_id);
            dest.writeString(unit);
        }
    }


    public static class Images implements Parcelable{

        @SerializedName("header_id")
        @Expose
        public int headerId;
        @SerializedName("document_id")
        @Expose
        public int documentId;
        @SerializedName("image_path")
        @Expose
        public String imagePath;
        @SerializedName("location")
        @Expose
        public String location;


        protected Images(Parcel in) {
            headerId = in.readInt();
            documentId = in.readInt();
            imagePath = in.readString();
            location = in.readString();
        }

        public static final Creator<Images> CREATOR = new Creator<Images>() {
            @Override
            public Images createFromParcel(Parcel in) {
                return new Images(in);
            }

            @Override
            public Images[] newArray(int size) {
                return new Images[size];
            }
        };

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeInt(headerId);
            dest.writeInt(documentId);
            dest.writeString(imagePath);
            dest.writeString(location);
        }
    }




}
