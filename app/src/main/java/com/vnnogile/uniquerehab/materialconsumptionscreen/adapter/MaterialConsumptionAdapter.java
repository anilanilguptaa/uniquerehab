package com.vnnogile.uniquerehab.materialconsumptionscreen.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.vnnogile.uniquerehab.R;
import com.vnnogile.uniquerehab.global.UniqueUtils;
import com.vnnogile.uniquerehab.materialconsumptionscreen.model.MaterialConsumptionData;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MaterialConsumptionAdapter extends RecyclerView.Adapter<MaterialConsumptionAdapter.MaterialConView> {

    private MaterialConsumptionData data;
    private Context context;
    private Listener listener;

    public MaterialConsumptionAdapter(MaterialConsumptionData data, Context context,Listener listener) {
        this.data = data;
        this.context = context;
        this.listener = listener;
    }

    public interface Listener{
        void onDeleteMaterial(MaterialConsumptionData.Datum datum);
    }

    @NonNull
    @Override
    public MaterialConView onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.cell_material_in_regular, parent, false);
        return new MaterialConView(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MaterialConView holder, int position) {
        MaterialConsumptionData.Datum   datum=data.data.get(position);

        holder.txtMaterialName.setText("Material:"+ UniqueUtils.removeChar(datum.materialName));
        holder.txtUnit.setText("Unit:"+ UniqueUtils.removeChar(datum.unit));
        holder.txtQty.setText("Qty:"+ UniqueUtils.removeChar(datum.quantity));

        holder.deleteContainer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (listener!=null)
                    listener.onDeleteMaterial(datum);
            }
        });
    }

    @Override
    public int getItemCount() {
        return data.data.size();
    }

    class MaterialConView extends RecyclerView.ViewHolder {
        @BindView(R.id.delete_container)
        RelativeLayout deleteContainer;
        @BindView(R.id.txt_material_name)
        TextView txtMaterialName;
        @BindView(R.id.txt_unit)
        TextView txtUnit;
        @BindView(R.id.txt_qty)
        TextView txtQty;
        MaterialConView(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
