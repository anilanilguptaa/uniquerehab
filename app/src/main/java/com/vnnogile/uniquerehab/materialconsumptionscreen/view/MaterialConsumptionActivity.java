package com.vnnogile.uniquerehab.materialconsumptionscreen.view;

import android.Manifest;
import android.content.Intent;
import android.graphics.Bitmap;
import android.location.Location;
import android.os.Bundle;
import android.provider.MediaStore;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.RecyclerView;

import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;
import com.vnnogile.uniquerehab.R;
import com.vnnogile.uniquerehab.dailyprogressscreen.adapter.ImageAdapter;
import com.vnnogile.uniquerehab.global.Constant;
import com.vnnogile.uniquerehab.global.CustomCalendar;
import com.vnnogile.uniquerehab.global.DecimalValueFilter;
import com.vnnogile.uniquerehab.global.LocationModel;
import com.vnnogile.uniquerehab.global.MyApplication;
import com.vnnogile.uniquerehab.global.PreferenceManager;
import com.vnnogile.uniquerehab.global.ProgressBar;
import com.vnnogile.uniquerehab.global.UniqueLocationTrack;
import com.vnnogile.uniquerehab.global.UniqueUtils;
import com.vnnogile.uniquerehab.global.WarningDialog;
import com.vnnogile.uniquerehab.materialconsumptionscreen.adapter.MaterialConsumptionAdapter;
import com.vnnogile.uniquerehab.materialconsumptionscreen.controller.MaterialConsumptionController;
import com.vnnogile.uniquerehab.materialconsumptionscreen.model.MaterialConsumptionData;
import com.vnnogile.uniquerehab.materialinward.adapter.ImageAdapterWithUrl;
import com.vnnogile.uniquerehab.materialinward.model.MaterialInModel;
import com.vnnogile.uniquerehab.materiallistscreen.view.MaterialListScreen;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MaterialConsumptionActivity extends AppCompatActivity implements
        MaterialConsumptionController.MaterialConsumptionListener, UniqueLocationTrack.LocationManagerListener,
        ImageAdapterWithUrl.Listener, MaterialConsumptionAdapter.Listener, TextWatcher {

    public static final String TAG = MaterialConsumptionActivity.class.getSimpleName();
    private static int REQUEST_CAMERA = 1;
    @BindView(R.id.toolbar_menu)
    ImageView toolbarMenu;
    @BindView(R.id.toolbar_name)
    TextView toolbarName;
    @BindView(R.id.toolbar_profile)
    ImageView toolbarProfile;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.txt_date)
    TextView txtDate;
    @BindView(R.id.txt_time)
    TextView txtTime;
    @BindView(R.id.edit_quantity)
    EditText editQuantity;
    @BindView(R.id.edit_unit)
    EditText editUnit;
    @BindView(R.id.edit_material)
    EditText edit_material;
    @BindView(R.id.btn_click)
    ImageView btnClick;
    @BindView(R.id.image_rv)
    RecyclerView imageRv;
    @BindView(R.id.btn_submit)
    Button btnSubmit;
    @BindView(R.id.lbl)
    TextView lbl;
    @BindView(R.id.material_rv)
    RecyclerView materialRv;
    @BindView(R.id.container1)
    LinearLayout container1;
    @BindView(R.id.date_container)
    LinearLayout dateContainer;
    @BindView(R.id.container2)
    LinearLayout container2;
    @BindView(R.id.toolbar_project)
    TextView toolbarProject;
    @BindView(R.id.btn_add)
    Button btn_add;

    private ProgressBar progressBar;
    private UniqueLocationTrack locationTrack;
    private MaterialConsumptionController controller;
    private ImageAdapter imageAdapter;
    private ImageAdapterWithUrl adapterWithUrl;
    private List<Bitmap> bitmapList = new ArrayList<>();
    private List<LocationModel> locationModelList = new ArrayList<>();
    private MaterialConsumptionData model;
    private MaterialConsumptionAdapter consumptionAdapter;
    MaterialTransferAddAdapter transferAddAdapter;
    private int materialId = -1;
    private MaterialInModel.MaterialDatum materialData = null;

    private static int REQUEST_mATERIAL = 2;


    private List<MaterialInModel.MaterialRequestDatum> addData = new ArrayList<>();

    //date
    private Calendar calendar = Calendar.getInstance();
    private Calendar selectedDate = Calendar.getInstance();
    private int mYear = calendar.get(Calendar.YEAR);
    private int mMonth = calendar.get(Calendar.MONTH);
    private int mDay = calendar.get(Calendar.DAY_OF_MONTH);


    private String date = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_material_consumption);
        ButterKnife.bind(this);

        loadVariables();

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (requestCode == REQUEST_CAMERA) {
                Bitmap bitmap = (Bitmap) data.getExtras().get("data");
                bitmapList.add(bitmap);
                imageAdapter.notifyDataSetChanged();
            } else if (requestCode == REQUEST_mATERIAL && data != null) {
                Log.e(TAG, "onActivityResult: search data");
                materialData = data.getParcelableExtra("data");
                editUnit.setText(materialData.unit);
                edit_material.setText(materialData.name);
            }
        }

    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {

        if (item.getItemId() == android.R.id.home)
            onBackPressed();

        return super.onOptionsItemSelected(item);
    }

    private void loadVariables() {

        progressBar = new ProgressBar(this);
        controller = new MaterialConsumptionController(this);

        toolbar.setNavigationIcon(R.drawable.ic_back);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        toolbarProfile.setImageResource(R.drawable.profile_picture);
        toolbarName.setText("Material Consumption");
        toolbarProject.setText(MyApplication.getInstance().getProjectName());
        txtDate.setText(UniqueUtils.getCurrentDate("dd MMM yyyy"));
        txtTime.setText(UniqueUtils.getCurrentDate("HH:mm"));

        editQuantity.setFilters(new InputFilter[]{new DecimalValueFilter(4)});
        editQuantity.addTextChangedListener(this);

        imageAdapter = new ImageAdapter(bitmapList, this, R.layout.cell_material_image, locationModelList);
        imageAdapter.setListener(new ImageAdapter.ImageClickListener() {
            @Override
            public void onDelete(int position) {
                bitmapList.remove(position);
                if (locationModelList.size() != 0)
                    locationModelList.remove(position);
                imageAdapter.notifyDataSetChanged();
            }
        });
        imageRv.setAdapter(imageAdapter);

        date = UniqueUtils.getCurrentDate("yyyy-MM-dd");

        if (UniqueUtils.isNetworkAvailable(this)) {
            //  progressBar.show();
            if (PreferenceManager.getInstance().getUserRole().equalsIgnoreCase(UniqueUtils.ADMIN)) {
                btnSubmit.setVisibility(View.GONE);
                edit_material.setVisibility(View.GONE);
                container1.setVisibility(View.GONE);
                btnClick.setVisibility(View.GONE);
                btn_add.setVisibility(View.GONE);
                lbl.setText("Material Photos");
                controller.getMaterial(date);
            } else {
                transferAddAdapter = new MaterialTransferAddAdapter();

                materialRv.setAdapter(transferAddAdapter);

                lbl.setText("Click material photo");
                btnSubmit.setVisibility(View.VISIBLE);
                edit_material.setVisibility(View.VISIBLE);
                container1.setVisibility(View.VISIBLE);
                btnClick.setVisibility(View.VISIBLE);
                btn_add.setVisibility(View.VISIBLE);
                editUnit.setFocusable(false);
                edit_material.setFocusable(false);
                // controller.getMaterialData();
            }
        } else {
            UniqueUtils.showErrorMessage(Constant.NETWORK_ERROR_MESSAGE);
        }
    }

/*    private void setSpinnerData(List<MaterialInModel.MaterialDatum> dataList) {

        ArrayList<String> data = new ArrayList<>();
        data.add("--Select Material--");

        for (int i = 0; i < dataList.size(); i++) {
            data.add(dataList.get(i).name.replace(" ", ""));
        }

        // Creating adapter for spinner
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, data);
        // Drop down layout style - list view with radio button
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        // attaching data adapter to spinner
        materialSpinner.setAdapter(dataAdapter);

        materialSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position != 0) {
                    // Toasty.success(getActivity(), data.get(position)).show();

                    editUnit.setText(dataList.get(position - 1).unit);
                    materialId = dataList.get(position - 1).id;
                } else {
                    materialId = -1;
                    editUnit.setText("");
                    TextView textView = (TextView) view;
                    textView.setTextColor(getResources().getColor(R.color.colorDarkGrey));
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

    }*/


    private void showSettingsDialog() {
        WarningDialog warningDialog = new WarningDialog(this, new WarningDialog.WarningDialogListener() {
            @Override
            public void onPositiveClick() {
                UniqueUtils.openSettings(MaterialConsumptionActivity.this);
            }

            @Override
            public void onNegativeClick() {

            }
        });
        warningDialog.setMessage("This app needs permission to use this feature. You can grant them in app settings.");
        warningDialog.setPositiveButtonName("Setting");
        warningDialog.setNegativeButtonName("Cancel");
        warningDialog.show();

    }

    private void openCamera() {
        Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(cameraIntent, REQUEST_CAMERA);

    }

    private void clear() {
        bitmapList.clear();
        locationModelList.clear();
        editQuantity.setText("");
        editUnit.setText("");
        materialData = null;
        imageAdapter.notifyDataSetChanged();
        addData.clear();
        transferAddAdapter.notifyDataSetChanged();
    }

    @OnClick(R.id.btn_click)
    public void onBtnClickClicked() {
        Dexter.withActivity(this)
                .withPermissions(Manifest.permission.CAMERA,
                        Manifest.permission.ACCESS_FINE_LOCATION,
                        Manifest.permission.ACCESS_COARSE_LOCATION)
                .withListener(new MultiplePermissionsListener() {
                    @Override
                    public void onPermissionsChecked(MultiplePermissionsReport report) {
                        if (report.areAllPermissionsGranted()) {
                            if (UniqueUtils.isLocationEnabled(MaterialConsumptionActivity.this)) {
                                locationTrack = new UniqueLocationTrack(MaterialConsumptionActivity.this);
                                locationTrack.setListener(MaterialConsumptionActivity.this);
                                openCamera();
                            } else {
                                UniqueUtils.showWarningMessage(Constant.ENABLE_LOCATION);
                            }

                        } else {
                            showSettingsDialog();
                        }
                    }

                    @Override
                    public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {
                        token.continuePermissionRequest();
                    }
                })
                .onSameThread()
                .check();
    }

    @OnClick({R.id.btn_submit, R.id.btn_add, R.id.edit_material})
    public void onBtnSubmitClicked(View view) {

        if (view.getId() == R.id.btn_add) {
            if (materialData == null) {
                UniqueUtils.showWarningMessage("Select Material");
            } else if (TextUtils.isEmpty(editQuantity.getText().toString().trim())) {
                UniqueUtils.showWarningMessage("Enter Quantity");
            } else if (Double.parseDouble(editQuantity.getText().toString().trim()) < 1 ||
                    Double.parseDouble(editQuantity.getText().toString().trim()) > 1000) {
                UniqueUtils.showWarningMessage("Please enter quantity between 1 to 1000 ");
            } else if (TextUtils.isEmpty(editUnit.getText().toString().trim())) {
                UniqueUtils.showWarningMessage("Enter Unit");
            } else {

                MaterialInModel.MaterialRequestDatum datum = new MaterialInModel.MaterialRequestDatum();

                datum.materialName = materialData.name;
                datum.quantity = editQuantity.getText().toString().trim();
                datum.unit = editUnit.getText().toString().trim();
                datum.materialId = materialData.id + "";
                addData.add(datum);

                transferAddAdapter.notifyDataSetChanged();

                //  materialSpinner.setSelection(0);
                editUnit.setText("");
                editQuantity.setText("");
                edit_material.setText("");
                materialData = null;
            }
        } else if (view.getId() == R.id.btn_submit) {
            if (addData.size() == 0) {
                UniqueUtils.showWarningMessage("Please add Material");
            } else if (bitmapList.size() == 0) {
                UniqueUtils.showWarningMessage("Please click photo.");
            } else {
                if (UniqueUtils.isNetworkAvailable(this)) {
                    progressBar.show();
                    controller.addMaterialData(addData,
                            bitmapList, locationModelList);
                } else {
                    UniqueUtils.showErrorMessage(Constant.NETWORK_ERROR_MESSAGE);
                }
            }
        } else if (view.getId() == R.id.edit_material) {
            Intent intent = new Intent(this, MaterialListScreen.class);
            intent.putExtra("isMasterList", false);
            startActivityForResult(intent, REQUEST_mATERIAL);
        }
    }

    @Override
    public void onFailure(String errorMessage) {
        progressBar.dismiss();
        UniqueUtils.showErrorMessage(errorMessage);
    }


    List<MaterialInModel.MaterialDatum> datumList = new ArrayList<>();

    @Override
    public void onSuccess(MaterialConsumptionData list, List<MaterialInModel.MaterialDatum> datumList) {
        progressBar.dismiss();
        this.datumList.clear();

        if (list != null) {

            if (list.data.size() == 0) {
                UniqueUtils.showWarningMessage("No Material Data Found");
            }

            if (list.imageData.size() == 0) {
                UniqueUtils.showWarningMessage("No Image Data Found");
            }

            consumptionAdapter = new MaterialConsumptionAdapter(list, this, this);
            materialRv.setAdapter(consumptionAdapter);

            adapterWithUrl = new ImageAdapterWithUrl(list, this, MaterialConsumptionActivity.this, "");
            imageRv.setAdapter(adapterWithUrl);
        }


    }

    @Override
    public void onSuccessAdd(String message) {
        progressBar.dismiss();
        UniqueUtils.showSuccessMessage(message);
        clear();
    }

    @Override
    public void onSuccessDelete(String message) {
        progressBar.dismiss();
        UniqueUtils.showSuccessMessage(message);
        progressBar.show();
        controller.getMaterial(date);
    }

    @Override
    public void onLocationResult(Location location) {
        if (locationTrack != null) {
            System.out.println("deepak" + location.getLatitude());
            LocationModel locationModel = new LocationModel();
            locationModel.latitude = location.getLatitude();
            locationModel.longitude = location.getLongitude();
            locationModel.address = locationTrack.getAddress(location.getLatitude(), location.getLongitude());
            locationModelList.add(locationModel);
        }
    }


    @Override
    public void onDeleteImage(int id) {
        if (UniqueUtils.isNetworkAvailable(this)) {
            progressBar.show();
            controller.deleteMaterialImage(id);
        } else {
            UniqueUtils.showErrorMessage(Constant.NETWORK_ERROR_MESSAGE);
        }
    }

    @Override
    public void onDeleteMaterial(MaterialConsumptionData.Datum datum) {
        if (UniqueUtils.isNetworkAvailable(this)) {
            progressBar.show();
            controller.deleteMaterial(datum);
        } else {
            UniqueUtils.showErrorMessage(Constant.NETWORK_ERROR_MESSAGE);
        }
    }

    @OnClick(R.id.toolbar_profile)
    public void onViewClicked() {
        UniqueUtils.goToHomePage(this);
    }


    @OnClick({R.id.image_date, R.id.date_lyr})
    public void onDateClicked() {
        if (!PreferenceManager.getInstance().getUserRole().equalsIgnoreCase(UniqueUtils.Site_Engineer)) {
            CustomCalendar customCalendar = new CustomCalendar(this, "material_consumption");
            customCalendar.setListener(new CustomCalendar.CustomCalendarListener() {
                @Override
                public void onDateSet(Date selectedDate) {
                    txtDate.setText(UniqueUtils.getDateInFormat(selectedDate, "dd MMM yyyy"));
                    date = UniqueUtils.getDateInFormat(selectedDate, Constant.YYYY_MM_DD);
                    progressBar.show();
                    controller.getMaterial(date);
                }
            });
            customCalendar.show();
        }
    }

        @Override
        public void beforeTextChanged (CharSequence s,int start, int count, int after){

        }

        @Override
        public void onTextChanged (CharSequence s,int start, int before, int count){

        }

        @Override
        public void afterTextChanged (Editable s){
            if (s.length() == 1) {
                if (s.charAt(0) == '0' || s.charAt(0) == '.') {
                    editQuantity.setText("1");
                    editQuantity.setSelection(1);
                }
            }
        }


        class MaterialTransferAddAdapter extends RecyclerView.Adapter<MaterialTransferAddAdapter.MaterialTransferAdd> {


            @NonNull
            @Override
            public MaterialTransferAdd onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
                View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.cell_naterial_add, parent, false);
                return new MaterialTransferAdd(view);
            }

            @Override
            public void onBindViewHolder(@NonNull MaterialTransferAdd holder, int position) {
                MaterialInModel.MaterialRequestDatum datum = addData.get(position);

                holder.txtMaterialName.setText("" + datum.materialName);
                holder.txtQty.setText("Qty: " + datum.quantity);
                holder.txtUnit.setText("Unit: " + datum.unit);

                holder.deleteContainer.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        addData.remove(position);
                        notifyItemRemoved(position);
                    }
                });
            }

            @Override
            public int getItemCount() {
                return addData.size();
            }

            class MaterialTransferAdd extends RecyclerView.ViewHolder {
                @BindView(R.id.delete_container)
                RelativeLayout deleteContainer;
                @BindView(R.id.txt_material_name)
                TextView txtMaterialName;
                @BindView(R.id.txt_qty)
                TextView txtQty;
                @BindView(R.id.txt_unit)
                TextView txtUnit;

                MaterialTransferAdd(@NonNull View itemView) {
                    super(itemView);
                    ButterKnife.bind(this, itemView);
                }
            }
        }

    }
