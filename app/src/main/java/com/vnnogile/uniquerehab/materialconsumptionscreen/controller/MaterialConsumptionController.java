package com.vnnogile.uniquerehab.materialconsumptionscreen.controller;

import android.content.Context;
import android.graphics.Bitmap;
import android.util.Log;

import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import com.vnnogile.uniquerehab.global.Constant;
import com.vnnogile.uniquerehab.global.LocationModel;
import com.vnnogile.uniquerehab.global.MyApplication;
import com.vnnogile.uniquerehab.global.PreferenceManager;
import com.vnnogile.uniquerehab.global.ResponseMessage;
import com.vnnogile.uniquerehab.global.UniqueUtils;
import com.vnnogile.uniquerehab.materialconsumptionscreen.model.MaterialConsumptionData;
import com.vnnogile.uniquerehab.materialinward.model.MaterialInModel;
import com.vnnogile.uniquerehab.retrofit.ApiClient;
import com.vnnogile.uniquerehab.retrofit.ApiInterface;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MaterialConsumptionController {

    public static final String TAG = MaterialConsumptionController.class.getSimpleName();

    private Context context;
    private ApiInterface apiInterface;
    private MaterialConsumptionListener  listener;

    public MaterialConsumptionController(Context context) {
        this.context = context;
        listener = (MaterialConsumptionListener)context;
        apiInterface = ApiClient.getClient().create(ApiInterface.class);
    }

    public interface MaterialConsumptionListener{
        void onFailure(String errorMessage);
        void onSuccess(MaterialConsumptionData list,List<MaterialInModel.MaterialDatum> datumList);
        void onSuccessAdd(String message);
        void onSuccessDelete(String message);
    }

    public void getMaterial(String date){
        Call<MaterialConsumptionData> listCall = apiInterface.getMaterialConsumption(
                PreferenceManager.getInstance().getToken(),
                PreferenceManager.getInstance().getUserId(),
                MyApplication.getInstance().getProjectId(),
                date

        );

        listCall.enqueue(new Callback<MaterialConsumptionData>() {
            @Override
            public void onResponse(Call<MaterialConsumptionData> call, Response<MaterialConsumptionData> response) {
                if (response.isSuccessful()){
                    listener.onSuccess(response.body(),null);
                }else {
                    listener.onFailure(Constant.SERVER_ERROR);
                }
            }

            @Override
            public void onFailure(Call<MaterialConsumptionData> call, Throwable t) {
                Log.e(TAG, "onFailure: "+t.toString() );
                listener.onFailure(Constant.SERVER_ERROR);
            }
        });
    }

    public void addMaterialData(List<MaterialInModel.MaterialRequestDatum> addData,
                                List<Bitmap> bitmapList,
                                List<LocationModel> locationModels){
        JSONObject jsonObject=new JSONObject();
        try{

            jsonObject.accumulate("project_id", MyApplication.getInstance().getProjectId());
            jsonObject.accumulate("date",UniqueUtils.getCurrentDate(Constant.YYYY_MM_DD_HH_MM_SS));
            jsonObject.accumulate("user_id",PreferenceManager.getInstance().getUserId());

            JSONArray jsonArray = new JSONArray();
            JSONArray dataArray = new JSONArray();

            for (int i = 0; i < addData.size(); i++) {

                JSONObject object = new JSONObject();
                object.accumulate("material_id",addData.get(i).materialId);
                object.accumulate("quantity",addData.get(i).quantity);
                object.accumulate("unit",addData.get(i).quantity);
                dataArray.put(object);
            }

            for (int i = 0; i < bitmapList.size(); i++) {
                JSONObject object = new JSONObject();
                object.accumulate("data",UniqueUtils.bash64(bitmapList.get(i)));
                if (locationModels.size() != 0) {
                    object.accumulate("latitude", locationModels.get(i).latitude);
                    object.accumulate("longitude", locationModels.get(i).longitude);
                    object.accumulate("location", locationModels.get(i).address);
                } else {
                    object.accumulate("latitude", 0.0);
                    object.accumulate("longitude", 0.0);
                    object.accumulate("location", "");
                }
                jsonArray.put(object);
            }
            jsonObject.accumulate("material_consumption_details_data",dataArray);
            jsonObject.accumulate("images",jsonArray);

        }catch (Exception e ){
            e.printStackTrace();
        }

        JsonElement jsonObject1 = JsonParser.parseString(jsonObject.toString());

        Call<ResponseMessage> objectCall=apiInterface.addMaterialConsumption(PreferenceManager.getInstance().getToken(),jsonObject1);

        objectCall.enqueue(new Callback<ResponseMessage>() {

            @Override
            public void onResponse(Call<ResponseMessage> call, Response<ResponseMessage> response) {

                if (response.isSuccessful()){
                    listener.onSuccessAdd(response.body().message);
                }else {
                    listener.onFailure(Constant.SERVER_ERROR);
                }

            }

            @Override
            public void onFailure(Call<ResponseMessage> call, Throwable t) {
                Log.e(TAG, "onFailure: "+t.toString() );
                listener.onFailure(Constant.SERVER_ERROR);
            }
        });
    }


    public void getMaterialData(){
        Call<List<MaterialInModel.MaterialDatum>> listCall = apiInterface.getMaterial(
                PreferenceManager.getInstance().getToken(),
                PreferenceManager.getInstance().getUserId(),
                MyApplication.getInstance().getProjectId(),
                UniqueUtils.getCurrentDate(Constant.YYYY_MM_DD)

        );

        listCall.enqueue(new Callback<List<MaterialInModel.MaterialDatum>>() {
            @Override
            public void onResponse(Call<List<MaterialInModel.MaterialDatum>> call, Response<List<MaterialInModel.MaterialDatum>> response) {
                if (response.isSuccessful()){
                    listener.onSuccess(null,response.body());
                }else {
                    listener.onFailure(Constant.SERVER_ERROR);
                }
            }

            @Override
            public void onFailure(Call<List<MaterialInModel.MaterialDatum>> call, Throwable t) {
            listCall.cancel();
                Log.e(TAG, "onFailure: "+t.toString() );
                listener.onFailure(Constant.SERVER_ERROR);
            }
        });
    }


    public void deleteMaterial(MaterialConsumptionData.Datum datum){
        JSONObject jsonObject=new JSONObject();
        try{

            jsonObject.accumulate("project_id", MyApplication.getInstance().getProjectId());
            jsonObject.accumulate("date",UniqueUtils.getCurrentDate(Constant.YYYY_MM_DD_HH_MM_SS));
            jsonObject.accumulate("user_id",PreferenceManager.getInstance().getUserId());
            jsonObject.accumulate("material_id",datum.material_id);
            jsonObject.accumulate("material_consumption_detail_id",datum.detailId);


        }catch (Exception e ){
            e.printStackTrace();
        }

        JsonElement jsonObject1 = JsonParser.parseString(jsonObject.toString());

        Call<ResponseMessage> messageCall = apiInterface.deleteMaterialConsumption(
                PreferenceManager.getInstance().getToken(),
                jsonObject1
        );

        messageCall.enqueue(new Callback<ResponseMessage>() {
            @Override
            public void onResponse(Call<ResponseMessage> call, Response<ResponseMessage> response) {
                if (response.isSuccessful()){
                    listener.onSuccessDelete(response.body().message);
                }else {
                    listener.onFailure(Constant.SERVER_ERROR);
                }
            }

            @Override
            public void onFailure(Call<ResponseMessage> call, Throwable t) {
                Log.e(TAG, "onFailure: "+t.toString() );
                listener.onFailure(Constant.SERVER_ERROR);
            }
        });
    }


    public void deleteMaterialImage(int id){
        JSONObject jsonObject=new JSONObject();
        try{
            jsonObject.accumulate("user_id",PreferenceManager.getInstance().getUserId());
            jsonObject.accumulate("project_id",MyApplication.getInstance().getProjectId());
            jsonObject.accumulate("material_consumption_document_id",id);


        }catch (Exception e ){
            e.printStackTrace();
        }

        JsonElement jsonObject1 = JsonParser.parseString(jsonObject.toString());

        Call<ResponseMessage> messageCall = apiInterface.deleteMaterialConsumptionImage(
                PreferenceManager.getInstance().getToken(),
                jsonObject1
        );

        messageCall.enqueue(new Callback<ResponseMessage>() {
            @Override
            public void onResponse(Call<ResponseMessage> call, Response<ResponseMessage> response) {
                if (response.isSuccessful()){
                    listener.onSuccessDelete(response.body().message);
                }else {
                    listener.onFailure(Constant.SERVER_ERROR);
                }
            }

            @Override
            public void onFailure(Call<ResponseMessage> call, Throwable t) {
                Log.e(TAG, "onFailure: "+t.toString() );
                listener.onFailure(Constant.SERVER_ERROR);
            }
        });
    }

}
