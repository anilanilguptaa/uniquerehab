package com.vnnogile.uniquerehab.dailyprogressscreen.controller;

import android.content.Context;
import android.graphics.Bitmap;
import android.location.Address;
import android.location.Geocoder;
import android.util.Log;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.vnnogile.uniquerehab.dailyprogressscreen.model.DPPModel;
import com.vnnogile.uniquerehab.dprscreen.model.SearchModel;
import com.vnnogile.uniquerehab.global.Constant;
import com.vnnogile.uniquerehab.global.LocationModel;
import com.vnnogile.uniquerehab.global.LocationTrack;
import com.vnnogile.uniquerehab.global.MyApplication;
import com.vnnogile.uniquerehab.global.PreferenceManager;
import com.vnnogile.uniquerehab.global.ResponseMessage;
import com.vnnogile.uniquerehab.global.UniqueUtils;
import com.vnnogile.uniquerehab.retrofit.ApiClient;
import com.vnnogile.uniquerehab.retrofit.ApiInterface;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DPPController {

    public static final String TAG = DPPController.class.getSimpleName();

    private Context context;
    private DPPControllerListener listener;
    private ApiInterface apiInterface;
    private LocationTrack locationTrack;

    public interface DPPControllerListener {
        void onSuccessBOQList(List<SearchModel> list);

        void onSuccessUploadImage();

        void onSuccessDelete(String mg);

        void onSuccessDPPList(List<DPPModel> list);

        void onFailed(String errorMessage);
    }

    public DPPController(Context context) {
        this.context = context;
        listener = (DPPControllerListener) context;
        apiInterface = ApiClient.getClient().create(ApiInterface.class);
        locationTrack = new LocationTrack(context);
    }

    public void getBOQList(String date){
        Call<List<SearchModel>> objectCall = apiInterface.getBOQListDPP(PreferenceManager.getInstance().getToken(),
                PreferenceManager.getInstance().getUserId(),
               MyApplication.getInstance().getProjectId(),
                date);

        objectCall.enqueue(new Callback<List<SearchModel>>() {

            @Override
            public void onResponse(Call<List<SearchModel>> call, Response<List<SearchModel>> response) {
                if (response.isSuccessful()) {
                    listener.onSuccessBOQList(response.body());
                } else {
                    listener.onFailed(Constant.SERVER_ERROR);
                }
            }

            @Override
            public void onFailure(Call<List<SearchModel>> call, Throwable t) {
                Log.e(TAG, "onFailure: " + t.toString());
                listener.onFailed(Constant.SERVER_ERROR);
            }
        });
    }

    public void getDPPList(String date){
        Call<List<DPPModel>> objectCall = apiInterface.getDPPList(PreferenceManager.getInstance().getToken(),
                PreferenceManager.getInstance().getUserId(),
                MyApplication.getInstance().getProjectId(),date);

        objectCall.enqueue(new Callback<List<DPPModel>>() {

            @Override
            public void onResponse(Call<List<DPPModel>> call, Response<List<DPPModel>> response) {
                if (response.isSuccessful()){
                    listener.onSuccessDPPList(response.body());
                }else {
                    listener.onFailed(Constant.SERVER_ERROR);
                }
            }

            @Override
            public void onFailure(Call<List<DPPModel>> call, Throwable t) {
                Log.e(TAG, "onFailure: "+t.toString() );
                listener.onFailed(Constant.SERVER_ERROR);
            }
        });
    }

    public void uploadPicture(SearchModel model, List<Bitmap> bitmaps,
                              List<LocationModel> modelList,String date){
        JSONObject jsonObject=new JSONObject();
        try{

            jsonObject.accumulate("boq_id",model.id);
            jsonObject.accumulate("user_id",PreferenceManager.getInstance().getUserId());
            jsonObject.accumulate("project_id",MyApplication.getInstance().getProjectId());
            jsonObject.accumulate("boq_name",UniqueUtils.removeChar(model.name));
            jsonObject.accumulate("description",UniqueUtils.removeChar(model.description));
            jsonObject.accumulate("date",/*UniqueUtils.getCurrentDate(Constant.YYYY_MM_DD_HH_MM_SS)*/date);

            JSONArray jsonArray = new JSONArray();

            for (int i = 0; i < bitmaps.size(); i++) {
                JSONObject object = new JSONObject();
                object.accumulate("data",UniqueUtils.bash64(bitmaps.get(i)));
                if (modelList.size() != 0) {
                    object.accumulate("latitude", modelList.get(i).latitude);
                    object.accumulate("longitude", modelList.get(i).longitude);
                    object.accumulate("location", modelList.get(i).address);
                } else {
                    object.accumulate("latitude", 0.0);
                    object.accumulate("longitude", 0.0);
                    object.accumulate("location", "");
                }
                jsonArray.put(object);
            }

            jsonObject.accumulate("images",jsonArray);

        }catch (Exception e ){
            e.printStackTrace();
        }
        JsonElement jsonObject1 = JsonParser.parseString(jsonObject.toString());
        Call<Object> objectCall = apiInterface.uploadImage(PreferenceManager.getInstance().getToken(),
                jsonObject1);

        objectCall.enqueue(new Callback<Object>() {

            @Override
            public void onResponse(Call<Object> call, Response<Object> response) {

                if (response.isSuccessful()){
                    listener.onSuccessUploadImage();
                }else {
                    listener.onFailed(Constant.SERVER_ERROR);
                }

            }

            @Override
            public void onFailure(Call<Object> call, Throwable t) {
                Log.e(TAG, "onFailure: "+t.toString() );
                listener.onFailed(Constant.SERVER_ERROR);
            }
        });


    }

    public void deleteImage(int id){
        JSONObject jsonObject=new JSONObject();
        try{

            jsonObject.accumulate("document_id",id);
            jsonObject.accumulate("user_id",PreferenceManager.getInstance().getUserId());
            jsonObject.accumulate("project_id",MyApplication.getInstance().getProjectId());

        }catch (Exception e ){
            e.printStackTrace();
        }
        JsonElement jsonObject1 = JsonParser.parseString(jsonObject.toString());

        Call<ResponseMessage>  messageCall = apiInterface.deleteDPPphoto(
                PreferenceManager.getInstance().getToken(),jsonObject1
        );

        messageCall.enqueue(new Callback<ResponseMessage>() {
            @Override
            public void onResponse(Call<ResponseMessage> call, Response<ResponseMessage> response) {
            if (response.isSuccessful()){
                listener.onSuccessDelete(response.body().message);
            }else {
                listener.onFailed(Constant.SERVER_ERROR);
            }
            }

            @Override
            public void onFailure(Call<ResponseMessage> call, Throwable t) {
                Log.e(TAG, "onFailure: "+t.toString() );
                listener.onFailed(Constant.SERVER_ERROR);
            }
        });
    }

}
