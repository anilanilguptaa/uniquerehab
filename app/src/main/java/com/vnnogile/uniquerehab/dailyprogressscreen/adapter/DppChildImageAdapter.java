package com.vnnogile.uniquerehab.dailyprogressscreen.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;
import com.vnnogile.uniquerehab.R;
import com.vnnogile.uniquerehab.dailyprogressscreen.model.DPPModel;
import com.vnnogile.uniquerehab.materialinward.model.MaterialInWord;
import com.vnnogile.uniquerehab.zoomimageview.view.ZoomImageViewActivity;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class DppChildImageAdapter extends RecyclerView.Adapter<DppChildImageAdapter.ImageViewHolder> {

    private Context context;
    private List<DPPModel.Photo> photos;
    private ImageDeleteListener listener;

    public DppChildImageAdapter(Context context, List<DPPModel.Photo> photos) {
        this.context = context;
        this.photos = photos;
    }

    public interface ImageDeleteListener{
        void onDeleteImage(DPPModel.Photo photo);
    }

    public void setListener(ImageDeleteListener listener) {
        this.listener = listener;
    }

    @NonNull
    @Override
    public ImageViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.cell_dpp_image, parent, false);
        return new ImageViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ImageViewHolder holder, int position) {
        DPPModel.Photo datum = photos.get(position);

        Picasso.get().load(datum.imagePath).into(holder.imageView);

        holder.imageDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onDeleteImage(datum);
            }
        });
    }

    @Override
    public int getItemCount() {
        return photos.size();
    }

    class ImageViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.image_view)
        ImageView imageView;
        @BindView(R.id.image_location)
        ImageView imageLocation;
        @BindView(R.id.image_delete)
        ImageView imageDelete;
        @BindView(R.id.pic_view)
        ImageView picView;
        ImageViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    DPPModel.Photo datum = photos.get(getAdapterPosition());
                    Intent intent = new Intent(context, ZoomImageViewActivity.class);
                    intent.putExtra("photo", datum);
                    context.startActivity(intent);
                }
            });
        }
    }
}
