package com.vnnogile.uniquerehab.dailyprogressscreen.view;

import android.Manifest;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.location.Location;
import android.os.Bundle;
import android.provider.MediaStore;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;
import com.vnnogile.uniquerehab.R;
import com.vnnogile.uniquerehab.dailyprogressscreen.adapter.DPPImageAdapter;
import com.vnnogile.uniquerehab.dailyprogressscreen.adapter.ImageAdapter;
import com.vnnogile.uniquerehab.dailyprogressscreen.controller.DPPController;
import com.vnnogile.uniquerehab.dailyprogressscreen.model.DPPModel;
import com.vnnogile.uniquerehab.dprscreen.model.SearchModel;
import com.vnnogile.uniquerehab.global.Constant;
import com.vnnogile.uniquerehab.global.CustomCalendar;
import com.vnnogile.uniquerehab.global.LocationModel;
import com.vnnogile.uniquerehab.global.MyApplication;
import com.vnnogile.uniquerehab.global.PreferenceManager;
import com.vnnogile.uniquerehab.global.ProgressBar;
import com.vnnogile.uniquerehab.global.UniqueLocationTrack;
import com.vnnogile.uniquerehab.global.UniqueUtils;
import com.vnnogile.uniquerehab.global.WarningDialog;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class DailyProgressPhotoActivity extends AppCompatActivity implements DPPController.DPPControllerListener, UniqueLocationTrack.LocationManagerListener, DPPImageAdapter.Listener {

    public static final String TAG = DailyProgressPhotoActivity.class.getSimpleName();
    private static int REQUEST_CAMERA = 1;
    @BindView(R.id.toolbar_name)
    TextView toolbarName;
    @BindView(R.id.toolbar_profile)
    ImageView toolbarProfile;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.txt_date)
    TextView txtDate;
    @BindView(R.id.txt_time)
    TextView txtTime;
    @BindView(R.id.image_rv)
    RecyclerView imageRv;
    @BindView(R.id.btn_upload_photo)
    Button btnUploadPhoto;
    @BindView(R.id.toolbar_project)
    TextView toolbarProject;


    private DPPController controller;
    private ImageAdapter imageAdapter;
    private ProgressBar progressBar;
    private DPPImageAdapter dppImageAdapter;
    private UniqueLocationTrack uniqueLocationTrack;
    private List<SearchModel> list = new ArrayList<>();
    private List<Bitmap> bitmapList = new ArrayList<>();
    public static List<LocationModel> locationModelList = new ArrayList<>();
    private List<DPPModel> dppModelList = new ArrayList<>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_daily_progress_photo);
        ButterKnife.bind(this);

        setSupportActionBar(toolbar);

        loadVariables();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK) {
            if (requestCode == REQUEST_CAMERA) {
                Bitmap bitmap = (Bitmap) data.getExtras().get("data");
                bitmapList.add(bitmap);
                imageAdapter.notifyDataSetChanged();
            }
        }
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {

        if (item.getItemId() == android.R.id.home)
            onBackPressed();

        return super.onOptionsItemSelected(item);
    }


    private void loadVariables() {
        progressBar = new ProgressBar(this);
        controller = new DPPController(this);
        date = UniqueUtils.getCurrentDate(Constant.YYYY_MM_DD);
        toolbar.setNavigationIcon(R.drawable.ic_back);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        imageAdapter = new ImageAdapter(bitmapList, this, R.layout.cell_image, locationModelList);
        imageAdapter.setListener(new ImageAdapter.ImageClickListener() {
            @Override
            public void onDelete(int position) {
                bitmapList.remove(position);
                imageAdapter.notifyDataSetChanged();
                if (locationModelList!=null&&locationModelList.size() > 0)
                    locationModelList.remove(position);
            }
        });
        toolbarProfile.setImageResource(R.drawable.profile_picture);
        toolbarName.setText("Daily Progress Photos");
        toolbarProject.setText(MyApplication.getInstance().getProjectName());
        txtDate.setText(UniqueUtils.getCurrentDate("dd MMM yyyy"));
        txtTime.setText(UniqueUtils.getCurrentDate("HH:mm"));

        if (PreferenceManager.getInstance().getUserRole().equalsIgnoreCase((UniqueUtils.Client))) {
            btnUploadPhoto.setVisibility(View.GONE);
        } else {
            btnUploadPhoto.setVisibility(View.VISIBLE);
        }

        getBoqList();
    }

    private void showSettingsDialog() {
        WarningDialog warningDialog = new WarningDialog(this, new WarningDialog.WarningDialogListener() {
            @Override
            public void onPositiveClick() {
                UniqueUtils.openSettings(DailyProgressPhotoActivity.this);
            }

            @Override
            public void onNegativeClick() {

            }
        });
        warningDialog.setMessage("This app needs permission to use this feature. You can grant them in app settings.");
        warningDialog.setPositiveButtonName("Setting");
        warningDialog.setNegativeButtonName("Cancel");
        warningDialog.show();

    }

    private Dialog selectBOQDialog;

    private void selectBOQDialog() {
        Dialog dialog = new Dialog(this);
        View view = LayoutInflater.from(this).inflate(R.layout.dialog_select_boq, null);
        selectBOQDialog = dialog;
        dialog.setContentView(view);
        EditText search = view.findViewById(R.id.search_bar);
        RecyclerView recyclerView = view.findViewById(R.id.boq_rv);
        BOQAdapter boqAdapter = new BOQAdapter();

        if (dialog.getWindow() != null) {
            dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
            dialog.getWindow().setGravity(Gravity.BOTTOM);
            dialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
        }

        recyclerView.setAdapter(boqAdapter);

        search.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                boqAdapter.getFilter().filter(s.toString());
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        dialog.show();
    }


    private void tackPictureDialog(SearchModel model) {

        Dialog dialog = new Dialog(this);
        View view = LayoutInflater.from(this).inflate(R.layout.dialog_tack_picture, null);

        dialog.setContentView(view);

        TextView txtBoqName = view.findViewById(R.id.txt_boq_name);
        Button button = view.findViewById(R.id.btn_submit);
        LinearLayout layout = view.findViewById(R.id.btn_click);
        RecyclerView image_rv = view.findViewById(R.id.image_rv);

        if (dialog.getWindow() != null) {
            dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
            dialog.getWindow().setGravity(Gravity.CENTER);
        }

        txtBoqName.setText(UniqueUtils.removeChar(model.name));
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                if (bitmapList.size() > 0) {
                    progressBar.show();
                    controller.uploadPicture(model, bitmapList, locationModelList,
                            UniqueUtils.getDateInFormat(selectedCalendar.getTime(), Constant.YYYY_MM_DD_HH_MM_SS));
                } else
                    UniqueUtils.showWarningMessage("No image found.");
            }
        });

        layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (bitmapList.size() < 6) {
                    if (UniqueUtils.isLocationEnabled(DailyProgressPhotoActivity.this)) {
                        uniqueLocationTrack = new UniqueLocationTrack(DailyProgressPhotoActivity.this);
                        uniqueLocationTrack.setListener(DailyProgressPhotoActivity.this);
                        openCamera();
                    } else {
                        UniqueUtils.showWarningMessage(Constant.ENABLE_LOCATION);
                    }
                } else {
                    UniqueUtils.showWarningMessage("Cannot click more then 6 photos.");
                }
            }
        });

        image_rv.setLayoutManager(new GridLayoutManager(this, 3));
        image_rv.setAdapter(imageAdapter);

        dialog.show();
    }

    private void openCamera() {
        Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(cameraIntent, REQUEST_CAMERA);
    }

    private void getBoqList() {
        if (!PreferenceManager.getInstance().getUserRole().equalsIgnoreCase((UniqueUtils.Client))) {
            controller.getBOQList(date);
        }
        progressBar.show();

        controller.getDPPList(date);
    }

    @OnClick(R.id.btn_upload_photo)
    public void onViewClicked() {
        Dexter.withActivity(this)
                .withPermissions(Manifest.permission.CAMERA,
                        Manifest.permission.READ_EXTERNAL_STORAGE,
                        Manifest.permission.ACCESS_COARSE_LOCATION,
                        Manifest.permission.ACCESS_FINE_LOCATION)
                .withListener(new MultiplePermissionsListener() {
                    @Override
                    public void onPermissionsChecked(MultiplePermissionsReport report) {
                        if (report.areAllPermissionsGranted()) {
                            selectBOQDialog();
                        } else {
                            showSettingsDialog();
                        }
                    }

                    @Override
                    public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {
                        token.continuePermissionRequest();
                    }
                })
                .onSameThread()
                .check();
    }

    @OnClick(R.id.toolbar_profile)
    public void onClicked() {
        UniqueUtils.goToHomePage(this);
    }

    @Override
    public void onSuccessBOQList(List<SearchModel> list) {
        this.list.clear();
        this.list.addAll(list);
        searchModels.addAll(list);
    }

    @Override
    public void onSuccessDPPList(List<DPPModel> list) {
        progressBar.dismiss();
        dppImageAdapter = new DPPImageAdapter(list, DailyProgressPhotoActivity.this, this);
        imageRv.setAdapter(dppImageAdapter);

        if (list != null && list.size() == 0) {
            UniqueUtils.showWarningMessage("No data found");
        }

    }

    @Override
    public void onSuccessUploadImage() {
        if (progressBar.isShowing())
            progressBar.dismiss();
        bitmapList.clear();
        locationModelList.clear();
        UniqueUtils.showSuccessMessage("Successfully uploaded.");

        progressBar.show();

        controller.getDPPList(date);
    }

    @Override
    public void onSuccessDelete(String mg) {
        progressBar.dismiss();
        UniqueUtils.showSuccessMessage(mg);
        progressBar.show();
        controller.getDPPList(date);
    }

    @Override
    public void onFailed(String errorMessage) {
        if (progressBar.isShowing())
            progressBar.dismiss();
        UniqueUtils.showErrorMessage(errorMessage);
    }

    private List<SearchModel> searchModels = new ArrayList<>();

    @Override
    public void onLocationResult(Location location) {
        if (uniqueLocationTrack != null) {
            LocationModel locationModel = new LocationModel();
            locationModel.latitude = location.getLatitude();
            locationModel.longitude = location.getLongitude();
            locationModel.address = uniqueLocationTrack.getAddress(location.getLatitude(), location.getLongitude());
            locationModelList.add(locationModel);
        }
    }

    @Override
    public void onDeleteImage(DPPModel.Photo photo) {

        if (!PreferenceManager.getInstance().getUserRole().equalsIgnoreCase(UniqueUtils.Client)) {
            progressBar.show();
            controller.deleteImage(photo.imageId);

        }
    }

    private Calendar selectedCalendar = Calendar.getInstance();

    private String date = "";

    @OnClick({R.id.image_date, R.id.date_lyr})
    public void onDateClicked() {

        if (!PreferenceManager.getInstance().getUserRole().equalsIgnoreCase(UniqueUtils.Site_Engineer)) {

            CustomCalendar customCalendar = new CustomCalendar(this, "dpp");
            customCalendar.setListener(new CustomCalendar.CustomCalendarListener() {
                @Override
                public void onDateSet(Date selectedDate) {
                    txtDate.setText(UniqueUtils.getDateInFormat(selectedDate, "dd MMM yyyy"));
                    date = UniqueUtils.getDateInFormat(selectedDate, "yyyy-MM-dd");
                    selectedCalendar.setTime(selectedDate);
                    getBoqList();
                }
            });
            customCalendar.show();
        }
    }

    //inner class
    class BOQAdapter extends RecyclerView.Adapter<BOQAdapter.BoqView> implements Filterable {

        @NonNull
        @Override
        public BoqView onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.cell_single_text, parent, false);
            return new BoqView(view);
        }

        @Override
        public void onBindViewHolder(@NonNull BoqView holder, int position) {
            holder.txtBoqName.setText(UniqueUtils.removeChar(list.get(position).name));
            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    tackPictureDialog(list.get(position));
                    selectBOQDialog.dismiss();
                }
            });
        }

        @Override
        public int getItemCount() {
            return list.size();
        }

        @Override
        public Filter getFilter() {
            return filter;
        }

        Filter filter = new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                List<SearchModel> filteredList = new ArrayList<>();

                if (constraint == null || constraint.length() == 0) {
                    filteredList.addAll(searchModels);
                } else {
                    String filterPattern = constraint.toString().toLowerCase().trim();

                    for (SearchModel item : searchModels) {
                        if (item.name.toLowerCase().contains(filterPattern)) {
                            filteredList.add(item);
                        }
                    }
                }

                FilterResults results = new FilterResults();
                results.values = filteredList;

                return results;
            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                list.clear();
                list.addAll((List) results.values);
                notifyDataSetChanged();
            }
        };

        class BoqView extends RecyclerView.ViewHolder {
            @BindView(R.id.txt_boq_name)
            TextView txtBoqName;

            BoqView(@NonNull View itemView) {
                super(itemView);
                ButterKnife.bind(this, itemView);
            }
        }
    }
}
