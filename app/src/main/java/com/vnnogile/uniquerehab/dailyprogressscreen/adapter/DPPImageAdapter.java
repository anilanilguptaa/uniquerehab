package com.vnnogile.uniquerehab.dailyprogressscreen.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.AsyncTask;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.vnnogile.uniquerehab.R;
import com.vnnogile.uniquerehab.dailyprogressscreen.model.DPPModel;
import com.vnnogile.uniquerehab.materialinward.adapter.ImageAdapterWithUrl;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class DPPImageAdapter extends RecyclerView.Adapter<DPPImageAdapter.DPPViewHolder> implements DppChildImageAdapter.ImageDeleteListener {

    private List<DPPModel> list;
    private Context context;
    private DppChildImageAdapter dppChildImageAdapter;
    private Listener listener;

    public DPPImageAdapter(List<DPPModel> list, Context context,Listener listener) {
        this.list = list;
        this.context = context;
        this.listener = listener;
    }

    public interface Listener{
        void onDeleteImage(DPPModel.Photo photo);
    }

    @NonNull
    @Override
    public DPPViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.cell_dpp, parent, false);
        return new DPPViewHolder(view);
    }

    @SuppressLint("StaticFieldLeak")
    @Override
    public void onBindViewHolder(@NonNull DPPViewHolder holder, int position) {
        DPPModel model = list.get(position);

        holder.txtBoqName.setText(model.boqName);
        dppChildImageAdapter = new DppChildImageAdapter(context,model.photos);
        dppChildImageAdapter.setListener(this);
        holder.imageRv.setAdapter(dppChildImageAdapter);


    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    @Override
    public void onDeleteImage(DPPModel.Photo photo) {
        listener.onDeleteImage(photo);
    }

    class DPPViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.txt_boq_name)
        TextView txtBoqName;
        @BindView(R.id.image_rv)
        RecyclerView imageRv;

        DPPViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);
            imageRv.setLayoutManager(new GridLayoutManager(context,3));
        }
    }
}
