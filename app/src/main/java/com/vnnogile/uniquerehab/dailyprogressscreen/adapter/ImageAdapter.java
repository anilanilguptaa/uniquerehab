package com.vnnogile.uniquerehab.dailyprogressscreen.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.vnnogile.uniquerehab.R;
import com.vnnogile.uniquerehab.global.LocationModel;
import com.vnnogile.uniquerehab.zoomimageview.view.ZoomImageViewActivity;

import java.io.ByteArrayOutputStream;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ImageAdapter extends RecyclerView.Adapter<ImageAdapter.ImageViewHolder> {

    private List<Bitmap> bitmapList;
    private Context context;
    private ImageClickListener listener;
    private int resId = R.layout.cell_image;
    private List<LocationModel> models;

    public ImageAdapter(List<Bitmap> bitmapList, Context context, int resId, List<LocationModel> models) {
        this.bitmapList = bitmapList;
        this.context = context;
        this.resId = resId;
        this.models = models;
    }

    public void setListener(ImageClickListener listener) {
        this.listener = listener;
    }

    public interface ImageClickListener {
        void onDelete(int position);
    }

    @NonNull
    @Override
    public ImageViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(resId, parent, false);

        return new ImageViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ImageViewHolder holder, int position) {
        holder.imageView.setImageBitmap(bitmapList.get(position));

        holder.imageDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (listener != null) {
                    listener.onDelete(position);
                }

            }
        });

        holder.picView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ByteArrayOutputStream stream = new ByteArrayOutputStream();
                bitmapList.get(position).compress(Bitmap.CompressFormat.PNG, 100, stream);
                byte[] byteArray = stream.toByteArray();
                Intent intent = new Intent(context, ZoomImageViewActivity.class);
                intent.putExtra("bitmap", byteArray);
                if (models.size() != 0)
                    intent.putExtra("address", models.get(position).address);
                else
                    intent.putExtra("address", " ");
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return bitmapList.size();
    }

    class ImageViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.image_view)
        ImageView imageView;
        @BindView(R.id.image_location)
        ImageView imageLocation;
        @BindView(R.id.image_delete)
        ImageView imageDelete;
        @BindView(R.id.pic_view)
        ImageView picView;

        ImageViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);


        }
    }
}
