package com.vnnogile.uniquerehab.dailyprogressscreen.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class DPPModel implements Parcelable{
    @SerializedName("boq_name")
    @Expose
    public String boqName;
    @SerializedName("photos")
    @Expose
    public List<Photo> photos = new ArrayList<>();

    protected DPPModel(Parcel in) {
        boqName = in.readString();
        photos = in.createTypedArrayList(Photo.CREATOR);
    }

    public static final Creator<DPPModel> CREATOR = new Creator<DPPModel>() {
        @Override
        public DPPModel createFromParcel(Parcel in) {
            return new DPPModel(in);
        }

        @Override
        public DPPModel[] newArray(int size) {
            return new DPPModel[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(boqName);
        dest.writeTypedList(photos);
    }


    public static class Photo implements Parcelable {
        @SerializedName("boq_number")
        @Expose
        public String boqNumber;
        @SerializedName("boq_id")
        @Expose
        public int boqId;
        @SerializedName("image_id")
        @Expose
        public int imageId;
        @SerializedName("image_path")
        @Expose
        public String imagePath;
        @SerializedName("location")
        @Expose
        public String location;

        protected Photo(Parcel in) {
            boqNumber = in.readString();
            boqId = in.readInt();
            imageId = in.readInt();
            imagePath = in.readString();
            location = in.readString();
        }

        public static final Creator<Photo> CREATOR = new Creator<Photo>() {
            @Override
            public Photo createFromParcel(Parcel in) {
                return new Photo(in);
            }

            @Override
            public Photo[] newArray(int size) {
                return new Photo[size];
            }
        };

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeString(boqNumber);
            dest.writeInt(boqId);
            dest.writeInt(imageId);
            dest.writeString(imagePath);
            dest.writeString(location);
        }
    }
}
