package com.vnnogile.uniquerehab.zoomimageview.view;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.squareup.picasso.Picasso;
import com.vnnogile.uniquerehab.R;
import com.vnnogile.uniquerehab.dailyprogressscreen.model.DPPModel;
import com.vnnogile.uniquerehab.materialinward.model.MaterialInWord;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ZoomImageViewActivity extends AppCompatActivity {

    public static final String TAG = ZoomImageViewActivity.class.getSimpleName();
    @BindView(R.id.image)
    ImageView image;
    @BindView(R.id.image_clear)
    ImageView imageClear;
    @BindView(R.id.txt_address)
    TextView txt_address;

    private Bitmap bitmap=null;
    MaterialInWord.ImageDatum datum=null;
    DPPModel.Photo photo = null;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_zoom_image_view);
        ButterKnife.bind(this);

        if (getIntent().hasExtra("bitmap")){
            byte[] byteArray = getIntent().getByteArrayExtra("bitmap");
            Bitmap bmp = BitmapFactory.decodeByteArray(byteArray, 0, byteArray.length);
            image.setImageBitmap(bmp);
            txt_address.setText(getIntent().getStringExtra("address"));
        }

        if (getIntent().hasExtra("data")){
             datum= getIntent().getParcelableExtra("data");

             if (datum.path!=null && !datum.path.isEmpty()){
                 Picasso.get().load(datum.path).into(image);
             }else {
                 Picasso.get().load(datum.image_path).into(image);
             }

            txt_address.setText(datum.location);


        }

        if (getIntent().hasExtra("photo")){
             photo= getIntent().getParcelableExtra("photo");

            Picasso.get().load(photo.imagePath).into(image);
            txt_address.setText(photo.location);
        }

        if (getIntent().hasExtra("singlePhoto")){
            Picasso.get().load(getIntent().getStringExtra("singlePhoto")).into(image);
            txt_address.setText(getIntent().getStringExtra("location"));
        }
    }

    @OnClick(R.id.image_clear)
    public void onViewClicked() {
        onBackPressed();
    }
}
