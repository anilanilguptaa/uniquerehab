package com.vnnogile.uniquerehab.loginscreen.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class LoginResponse implements Parcelable {
    @SerializedName("status")
    @Expose
    public Boolean status;

    @SerializedName("message")
    @Expose
    public String message;

    @SerializedName("error")
    @Expose
    public String error;

    @SerializedName("user_hash")
    @Expose
    public String userHash;


    @SerializedName("role_id")
    @Expose
    public int roleId;

    @SerializedName("role_name")
    @Expose
    public String roleName;


    @SerializedName("user_detail")
    @Expose
    public UserDetail userDetail;


    @SerializedName("user_profile_detail")
    @Expose
    public UserProfileDetail userProfileDetail;

    protected LoginResponse(Parcel in) {
        byte tmpStatus = in.readByte();
        status = tmpStatus == 0 ? null : tmpStatus == 1;
        message = in.readString();
        error = in.readString();
        userHash = in.readString();
        roleId = in.readInt();
        roleName = in.readString();
        userDetail = in.readParcelable(UserDetail.class.getClassLoader());
        userProfileDetail= in.readParcelable(UserProfileDetail.class.getClassLoader());
    }

    public static final Creator<LoginResponse> CREATOR = new Creator<LoginResponse>() {
        @Override
        public LoginResponse createFromParcel(Parcel in) {
            return new LoginResponse(in);
        }

        @Override
        public LoginResponse[] newArray(int size) {
            return new LoginResponse[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeByte((byte) (status == null ? 0 : status ? 1 : 2));
        dest.writeString(message);
        dest.writeString(error);
        dest.writeString(userHash);
        dest.writeInt(roleId);
        dest.writeString(roleName);
        dest.writeParcelable(userDetail, flags);
    }


    public static class UserDetail implements Parcelable {

        @SerializedName("id")
        @Expose
        public Integer id;
        @SerializedName("first_name")
        @Expose
        public String firstName;
        @SerializedName("last_name")
        @Expose
        public String lastName;
        @SerializedName("email")
        @Expose
        public String email;
        @SerializedName("personal_email")
        @Expose
        public Object personalEmail;
        @SerializedName("api_token")
        @Expose
        public String apiToken;
        @SerializedName("userid")
        @Expose
        public String userid;

        protected UserDetail(Parcel in) {
            if (in.readByte() == 0) {
                id = null;
            } else {
                id = in.readInt();
            }
            firstName = in.readString();
            lastName = in.readString();
            email = in.readString();
            apiToken = in.readString();
            userid = in.readString();
        }

        public static final Creator<UserDetail> CREATOR = new Creator<UserDetail>() {
            @Override
            public UserDetail createFromParcel(Parcel in) {
                return new UserDetail(in);
            }

            @Override
            public UserDetail[] newArray(int size) {
                return new UserDetail[size];
            }
        };

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            if (id == null) {
                dest.writeByte((byte) 0);
            } else {
                dest.writeByte((byte) 1);
                dest.writeInt(id);
            }
            dest.writeString(firstName);
            dest.writeString(lastName);
            dest.writeString(email);
            dest.writeString(apiToken);
            dest.writeString(userid);
        }
    }

    public static class UserProfileDetail implements Parcelable {
        @SerializedName("photo")
        @Expose
        public String photo;

        protected UserProfileDetail(Parcel in) {
            photo = in.readString();
        }

        public static final Creator<UserProfileDetail> CREATOR = new Creator<UserProfileDetail>() {
            @Override
            public UserProfileDetail createFromParcel(Parcel in) {
                return new UserProfileDetail(in);
            }

            @Override
            public UserProfileDetail[] newArray(int size) {
                return new UserProfileDetail[size];
            }
        };

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeString(photo);
        }
    }
}
