package com.vnnogile.uniquerehab.loginscreen.controller;

import android.content.Context;
import android.util.Log;

import androidx.annotation.NonNull;

import com.vnnogile.uniquerehab.global.Constant;
import com.vnnogile.uniquerehab.global.PreferenceManager;
import com.vnnogile.uniquerehab.loginscreen.model.LoginResponse;
import com.vnnogile.uniquerehab.loginscreen.model.User;
import com.vnnogile.uniquerehab.retrofit.ApiClient;
import com.vnnogile.uniquerehab.retrofit.ApiInterface;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginController {

    public static final String TAG = LoginController.class.getSimpleName();
    private Context context;
    private ApiInterface apiInterface;
    private LoginIController loginIController;

    public LoginController(Context context) {
        this.context = context;
        loginIController = (LoginIController) context;
        apiInterface = ApiClient.getClient().create(ApiInterface.class);
    }

    public interface LoginIController {
        void onLoginSuccess(LoginResponse loginResponse);

        void onLoginFailed(String errorMessage);

        void onLoginError(String errorMessage);
    }

    public void login(User user) {

        Call<LoginResponse> stringCall = apiInterface.login(user);

        stringCall.enqueue(new Callback<LoginResponse>() {
            @Override
            public void onResponse(@NonNull Call<LoginResponse> call, @NonNull Response<LoginResponse> response) {
                Log.e(TAG, "onResponse: " + response.body());

                if (response.isSuccessful()) {
                    if (response.body().status) {
                        PreferenceManager.getInstance().setToken(response.body().userDetail.apiToken);
                        PreferenceManager.getInstance().setUserDetails(response.body());
                        loginIController.onLoginSuccess(response.body());
                    } else {
                        loginIController.onLoginError(response.body().message);
                    }
                }else {
                    loginIController.onLoginFailed(Constant.SERVER_ERROR);
                }

            }

            @Override
            public void onFailure(@NonNull Call<LoginResponse> call, @NonNull Throwable t) {
                Log.e(TAG, "onFailure: " + t.toString());
                loginIController.onLoginFailed(Constant.SERVER_ERROR);
            }
        });
    }
}
