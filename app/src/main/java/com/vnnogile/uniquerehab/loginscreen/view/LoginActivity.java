package com.vnnogile.uniquerehab.loginscreen.view;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.vnnogile.uniquerehab.R;
import com.vnnogile.uniquerehab.dashboardscreen.view.DashboardActivity;
import com.vnnogile.uniquerehab.global.Constant;
import com.vnnogile.uniquerehab.global.PreferenceManager;
import com.vnnogile.uniquerehab.global.ProgressBar;
import com.vnnogile.uniquerehab.global.UniqueUtils;
import com.vnnogile.uniquerehab.loginscreen.controller.LoginController;
import com.vnnogile.uniquerehab.loginscreen.model.LoginResponse;
import com.vnnogile.uniquerehab.loginscreen.model.User;
import com.vnnogile.uniquerehab.resetpassword.ResetPasswordActivity;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class LoginActivity extends AppCompatActivity implements LoginController.LoginIController {

    public static final String TAG = LoginActivity.class.getSimpleName();

    private LoginController controller;
    private ProgressBar progressBar;


    @BindView(R.id.image_profile)
    ImageView imageProfile;
    @BindView(R.id.edit_username)
    EditText editUsername;
    @BindView(R.id.edit_password)
    EditText editPassword;
    @BindView(R.id.txt_forgot_password)
    TextView txtForgotPassword;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);

        loadVariables();

    }

    private void loadVariables() {
        controller = new LoginController(this);
        progressBar = new ProgressBar(this);
        imageProfile.setImageResource(R.drawable.profile_picture);
    }

    private boolean isValidate() {

        if (TextUtils.isEmpty(editUsername.getText().toString().trim())) {
            UniqueUtils.showWarningMessage("Username cannot be empty.");
            return false;
        } else if (editUsername.getText().toString().trim().length() < 4) {
            UniqueUtils.showWarningMessage("Username length must be greater than 4 char.");
            return false;
        } else if (TextUtils.isEmpty(editPassword.getText().toString().trim())) {
            UniqueUtils.showWarningMessage("Password cannot be empty.");
            return false;
        } else if (editPassword.getText().toString().trim().length() < 4) {
            UniqueUtils.showWarningMessage("Password length must be greater than 4 char.");
            return false;
        }


        return true;
    }

    @OnClick(R.id.btn_login)
    public void onBtnLoginClicked() {

        if (isValidate()) {
            if (UniqueUtils.isNetworkAvailable(this)) {
                progressBar.show();
                User user = new User();
                user.username = editUsername.getText().toString().trim();
                user.password = editPassword.getText().toString().trim();
                controller.login(user);
            } else {
                UniqueUtils.showErrorMessage(Constant.NETWORK_ERROR_MESSAGE);
            }
        }

    }

    @OnClick(R.id.txt_forgot_password)
    public void onTxtForgotPasswordClicked() {
        Intent intent = new Intent(this, ResetPasswordActivity.class);
        startActivity(intent);
    }

    @Override
    public void onLoginSuccess(LoginResponse loginResponse) {
        if (progressBar.isShowing())
            progressBar.dismiss();
        UniqueUtils.showSuccessMessage(loginResponse.message);
        PreferenceManager.getInstance().setIsLogin(true);
        PreferenceManager.getInstance().setPassword(editPassword.getText().toString().trim());
        PreferenceManager.getInstance().setUsername(editUsername.getText().toString().trim());
        Intent intent = new Intent(this, DashboardActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
    }

    @Override
    public void onLoginFailed(String errorMessage) {
        if (progressBar.isShowing())
            progressBar.dismiss();
        UniqueUtils.showErrorMessage(errorMessage);
        PreferenceManager.getInstance().setIsLogin(false);
    }

    @Override
    public void onLoginError(String errorMessage) {
        if (progressBar.isShowing())
            progressBar.dismiss();
        UniqueUtils.showErrorMessage(errorMessage);
        PreferenceManager.getInstance().setIsLogin(false);
    }
}
