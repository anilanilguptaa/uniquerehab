package com.vnnogile.uniquerehab.materialaudit.view;


import android.Manifest;
import android.content.Intent;
import android.graphics.Bitmap;
import android.location.Location;
import android.os.Bundle;
import android.provider.MediaStore;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;
import com.vnnogile.uniquerehab.R;
import com.vnnogile.uniquerehab.dailyprogressscreen.adapter.ImageAdapter;
import com.vnnogile.uniquerehab.global.Constant;
import com.vnnogile.uniquerehab.global.DecimalValueFilter;
import com.vnnogile.uniquerehab.global.LocationModel;
import com.vnnogile.uniquerehab.global.ProgressBar;
import com.vnnogile.uniquerehab.global.UniqueLocationTrack;
import com.vnnogile.uniquerehab.global.UniqueUtils;
import com.vnnogile.uniquerehab.global.WarningDialog;
import com.vnnogile.uniquerehab.materialaudit.controller.MaterialAuditController;
import com.vnnogile.uniquerehab.materialaudit.model.MaterialAuditModel;
import com.vnnogile.uniquerehab.materialinward.model.MaterialInModel;
import com.vnnogile.uniquerehab.materiallistscreen.view.MaterialListScreen;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static android.app.Activity.RESULT_OK;


/**
 * A simple {@link Fragment} subclass.
 */
public class MaterialAuditSiteFragment extends Fragment implements UniqueLocationTrack.LocationManagerListener,
        MaterialAuditController.MaterialAuditListener, TextWatcher {

public static final String TAG = MaterialAuditSiteFragment.class.getSimpleName();

    @BindView(R.id.txt_date)
    TextView txtDate;
    @BindView(R.id.txt_time)
    TextView txtTime;
    @BindView(R.id.material_spinner)
    Spinner materialSpinner;
    @BindView(R.id.edit_quantity)
    EditText editQuantity;
    @BindView(R.id.edit_unit)
    EditText editUnit;
    @BindView(R.id.btn_add)
    Button btnAdd;
    @BindView(R.id.material_rv)
    RecyclerView materialRv;
    @BindView(R.id.btn_click)
    ImageView btnClick;
    @BindView(R.id.image_rv)
    RecyclerView imageRv;
    @BindView(R.id.btn_submit)
    Button btnSubmit;
    @BindView(R.id.edit_material)
    EditText edit_material;

    public MaterialAuditSiteFragment() {
        // Required empty public constructor
    }

    private ProgressBar progressBar;
    private UniqueLocationTrack locationTrack;
    private static int REQUEST_CAMERA = 1;
    private static int REQUEST_mATERIAL = 2;
    private ImageAdapter imageAdapter;

    private MaterialAuditAddAdapter auditAddAdapter;
    private MaterialInModel.MaterialDatum materialData=null;
    private List<Bitmap> bitmapList = new ArrayList<>();
    private List<LocationModel> locationModelList = new ArrayList<>();
    private List<MaterialInModel.MaterialRequestDatum> addData = new ArrayList<>();

    private MaterialAuditController controller;

    public static MaterialAuditSiteFragment newInstance() {

        Bundle args = new Bundle();

        MaterialAuditSiteFragment fragment = new MaterialAuditSiteFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_material_audit_site, container, false);
        ButterKnife.bind(this, view);

        loadVariables();
        return view;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (requestCode == REQUEST_CAMERA) {
                Bitmap bitmap = (Bitmap) data.getExtras().get("data");
                bitmapList.add(bitmap);

                imageAdapter.notifyDataSetChanged();
            }else if (requestCode == REQUEST_mATERIAL&&data!=null){
                Log.e(TAG, "onActivityResult: search data");
                materialData=data.getParcelableExtra("data");
                editUnit.setText(materialData.unit);
                edit_material.setText(materialData.name);
            }
        }

    }

    private void loadVariables() {
        txtDate.setText(UniqueUtils.getCurrentDate("dd MMM yyyy"));
        txtTime.setText(UniqueUtils.getCurrentDate("HH:mm"));
        editUnit.setFocusable(false);
        edit_material.setFocusable(false);

        progressBar = new ProgressBar(getContext());

        editQuantity.setFilters(new InputFilter[]{new DecimalValueFilter(1)});
        editQuantity.addTextChangedListener(this);

        imageAdapter = new ImageAdapter(bitmapList, getContext(), R.layout.cell_material_image,locationModelList);
        imageAdapter.setListener(new ImageAdapter.ImageClickListener() {
            @Override
            public void onDelete(int position) {
                bitmapList.remove(position);
                locationModelList.remove(position);
                imageAdapter.notifyDataSetChanged();
            }
        });
        imageRv.setAdapter(imageAdapter);

        auditAddAdapter = new MaterialAuditAddAdapter();

        materialRv.setAdapter(auditAddAdapter);

        controller = new MaterialAuditController(getContext());
        controller.setListener(this);

       /* if (UniqueUtils.isNetworkAvailable(getContext())) {
            progressBar.show();
            controller.getMaterialAuditData();
        } else {
            UniqueUtils.showErrorMessage(Constant.NETWORK_ERROR_MESSAGE);
        }*/

    }


    private void setMSpinnerData(List<MaterialInModel.MaterialDatum> datumList) {

        ArrayList<String> data = new ArrayList<>();
        data.add("--Select Material--");
        for (int i = 0; i < datumList.size(); i++) {
            data.add(datumList.get(i).name.replace(" ", ""));
        }

        // Creating adapter for spinner
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(getContext(), android.R.layout.simple_spinner_item, data);
        // Drop down layout style - list view with radio button
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        // attaching data adapter to spinner
        materialSpinner.setAdapter(dataAdapter);

        materialSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position != 0) {
                    editUnit.setText(datumList.get(position - 1).unit);
                } else {
                    editUnit.setText("");
                    TextView textView = (TextView) view;
                    textView.setTextColor(getResources().getColor(R.color.colorDarkGrey));
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

    }

    private void showSettingsDialog() {
        WarningDialog warningDialog = new WarningDialog(getContext(), new WarningDialog.WarningDialogListener() {
            @Override
            public void onPositiveClick() {
                UniqueUtils.openSettings(getActivity());
            }

            @Override
            public void onNegativeClick() {

            }
        });
        warningDialog.setMessage("This app needs permission to use this feature. You can grant them in app settings.");
        warningDialog.setPositiveButtonName("Setting");
        warningDialog.setNegativeButtonName("Cancel");
        warningDialog.show();

    }

    private void openCamera() {
        Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(cameraIntent, REQUEST_CAMERA);

    }


    @OnClick({R.id.btn_add, R.id.btn_click, R.id.btn_submit,R.id.edit_material})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.btn_add:


                if (materialData==null) {
                    UniqueUtils.showWarningMessage("Select Material");
                } else if (editQuantity.getText().toString().trim().isEmpty()) {
                    UniqueUtils.showWarningMessage("Enter Quantity");
                }else if (Double.parseDouble(editQuantity.getText().toString().trim())<1||
                        Double.parseDouble(editQuantity.getText().toString().trim())>1000){
                    UniqueUtils.showWarningMessage("Please enter quantity between 1 to 1000 ");
                } else if (editUnit.getText().toString().trim().isEmpty()) {
                    UniqueUtils.showWarningMessage("Enter Unit");
                } else {
                    MaterialInModel.MaterialRequestDatum datum = new MaterialInModel.MaterialRequestDatum();

                    datum.materialName = materialData.name;
                    datum.quantity = editQuantity.getText().toString().trim();
                    datum.unit = editUnit.getText().toString().trim();
                    datum.materialId = materialData.id+ "";
                    addData.add(datum);

                    auditAddAdapter.notifyDataSetChanged();

                  //  materialSpinner.setSelection(0);
                    editUnit.setText("");
                    editQuantity.setText("");
                    edit_material.setText("");
                    materialData=null;
                    UniqueUtils.hideKeyboard(Objects.requireNonNull(getActivity()));
                }

                break;
            case R.id.btn_click:

                Dexter.withActivity(getActivity())
                        .withPermissions(Manifest.permission.CAMERA,
                                Manifest.permission.ACCESS_FINE_LOCATION,
                                Manifest.permission.ACCESS_COARSE_LOCATION)
                        .withListener(new MultiplePermissionsListener() {
                            @Override
                            public void onPermissionsChecked(MultiplePermissionsReport report) {
                                if (report.areAllPermissionsGranted()) {
                                    if (UniqueUtils.isLocationEnabled(getContext())) {
                                        locationTrack = new UniqueLocationTrack(getActivity());
                                        locationTrack.setListener(MaterialAuditSiteFragment.this);
                                        openCamera();
                                    }else {
                                        UniqueUtils.showWarningMessage(Constant.ENABLE_LOCATION);
                                    }
                                } else {
                                    showSettingsDialog();
                                }
                            }

                            @Override
                            public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {
                                token.continuePermissionRequest();
                            }
                        })
                        .onSameThread()
                        .check();

                break;
            case R.id.btn_submit:
                if (addData.size() == 0) {
                    UniqueUtils.showWarningMessage("No Material added");
                } else if (bitmapList.size() == 0) {
                    UniqueUtils.showWarningMessage("Please click photo.");
                } else {
                    progressBar.show();
                    controller.addMaterial(bitmapList, locationModelList, addData);
                }

                break;

            case R.id.edit_material:
                Intent intent = new Intent(getContext(), MaterialListScreen.class);
                intent.putExtra("isMasterList",true);
                startActivityForResult(intent,REQUEST_mATERIAL);
                break;
        }
    }

    @Override
    public void onLocationResult(Location location) {
        if (locationTrack != null) {
            LocationModel locationModel = new LocationModel();
            locationModel.latitude = location.getLatitude();
            locationModel.longitude = location.getLongitude();
            locationModel.address = locationTrack.getAddress(location.getLatitude(), location.getLongitude());
            locationModelList.add(locationModel);
        }
    }

    List<MaterialInModel.MaterialDatum> datumList = new ArrayList<>();

    @Override
    public void onSuccessMaterial(List<MaterialInModel.MaterialDatum> datumList) {
        progressBar.dismiss();
        this.datumList.clear();
        this.datumList.addAll(datumList);
        setMSpinnerData(datumList);
    }

    @Override
    public void onFailure(String errorMessage) {
        progressBar.dismiss();
        UniqueUtils.showErrorMessage(errorMessage);
    }

    @Override
    public void onSuccessAdd(String message) {
        bitmapList.clear();
        imageAdapter.notifyDataSetChanged();
        addData.clear();
        auditAddAdapter.notifyDataSetChanged();
        locationModelList.clear();
        progressBar.dismiss();
        materialSpinner.setSelection(0);
        UniqueUtils.showSuccessMessage(message);
    }

    @Override
    public void onAdminDataSuccess(MaterialAuditModel model) {
        progressBar.dismiss();
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {

    }

    @Override
    public void afterTextChanged(Editable s) {
        if (s.length()==1){
            if (s.charAt(0)=='0'||s.charAt(0)=='.'){
                editQuantity.setText("1");
                editQuantity.setSelection(1);
            }
        }
    }


    class MaterialAuditAddAdapter extends RecyclerView.Adapter<MaterialAuditAddAdapter.MaterialTransferAdd> {


        @NonNull
        @Override
        public MaterialTransferAdd onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.cell_naterial_add, parent, false);
            return new MaterialTransferAdd(view);
        }

        @Override
        public void onBindViewHolder(@NonNull MaterialTransferAdd holder, int position) {
            MaterialInModel.MaterialRequestDatum datum = addData.get(position);

            holder.txtMaterialName.setText("Material :" + datum.materialName);
            holder.txtQty.setText("Qty: " + datum.quantity);
            holder.txtUnit.setText("Unit: " + datum.unit);

            holder.deleteContainer.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    addData.remove(position);
                    notifyItemRemoved(position);
                }
            });
        }

        @Override
        public int getItemCount() {
            return addData.size();
        }

        class MaterialTransferAdd extends RecyclerView.ViewHolder {
            @BindView(R.id.delete_container)
            RelativeLayout deleteContainer;
            @BindView(R.id.txt_material_name)
            TextView txtMaterialName;
            @BindView(R.id.txt_qty)
            TextView txtQty;
            @BindView(R.id.txt_unit)
            TextView txtUnit;

            MaterialTransferAdd(@NonNull View itemView) {
                super(itemView);
                ButterKnife.bind(this, itemView);
            }
        }
    }

}
