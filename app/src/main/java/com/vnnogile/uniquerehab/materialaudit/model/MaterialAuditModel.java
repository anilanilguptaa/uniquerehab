package com.vnnogile.uniquerehab.materialaudit.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.vnnogile.uniquerehab.materialinward.model.MaterialInWord;
import com.vnnogile.uniquerehab.materialtransfer.model.MaterialTransferModel;

import java.util.List;

public class MaterialAuditModel implements Parcelable {

    @SerializedName("material_audit_data")
    @Expose
    public List<MaterialTransferModel.Datum>  datumList;

    @SerializedName("imageData")
    @Expose
    public List<MaterialInWord.ImageDatum>  imageData;

    protected MaterialAuditModel(Parcel in) {
        datumList = in.createTypedArrayList(MaterialTransferModel.Datum.CREATOR);
        imageData = in.createTypedArrayList(MaterialInWord.ImageDatum.CREATOR);
    }

    public static final Creator<MaterialAuditModel> CREATOR = new Creator<MaterialAuditModel>() {
        @Override
        public MaterialAuditModel createFromParcel(Parcel in) {
            return new MaterialAuditModel(in);
        }

        @Override
        public MaterialAuditModel[] newArray(int size) {
            return new MaterialAuditModel[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeTypedList(datumList);
        dest.writeTypedList(imageData);
    }
}
