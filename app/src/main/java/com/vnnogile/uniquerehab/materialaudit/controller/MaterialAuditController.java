package com.vnnogile.uniquerehab.materialaudit.controller;

import android.content.Context;
import android.graphics.Bitmap;
import android.util.Log;

import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import com.vnnogile.uniquerehab.global.Constant;
import com.vnnogile.uniquerehab.global.LocationModel;
import com.vnnogile.uniquerehab.global.MyApplication;
import com.vnnogile.uniquerehab.global.PreferenceManager;
import com.vnnogile.uniquerehab.global.ResponseMessage;
import com.vnnogile.uniquerehab.global.UniqueUtils;
import com.vnnogile.uniquerehab.materialaudit.model.MaterialAuditModel;
import com.vnnogile.uniquerehab.materialinward.model.MaterialInModel;
import com.vnnogile.uniquerehab.materialtransfer.model.MaterialTransferModel;
import com.vnnogile.uniquerehab.retrofit.ApiClient;
import com.vnnogile.uniquerehab.retrofit.ApiInterface;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MaterialAuditController {

    public static final String TAG = MaterialAuditController.class.getSimpleName();

    private Context context;
    private MaterialAuditListener listener;
    private ApiInterface apiInterface;

    public MaterialAuditController(Context context) {
        this.context = context;
        apiInterface = ApiClient.getClient().create(ApiInterface.class);
    }

    public interface MaterialAuditListener {

        void onSuccessMaterial(List<MaterialInModel.MaterialDatum> datumList);

        void onFailure(String errorMessage);

        void onSuccessAdd(String message);

        void onAdminDataSuccess(MaterialAuditModel model);
    }

    public void setListener(MaterialAuditListener listener) {
        this.listener = listener;
    }


    public void getMaterialAuditData() {
        Call<List<MaterialInModel.MaterialDatum>> objectCall = apiInterface.getMaterialListForMaterialAudit(
                PreferenceManager.getInstance().getToken(),
                PreferenceManager.getInstance().getUserId(),
                MyApplication.getInstance().getProjectId(),
                UniqueUtils.getCurrentDate(Constant.YYYY_MM_DD)
        );

        objectCall.enqueue(new Callback<List<MaterialInModel.MaterialDatum>>() {

            @Override
            public void onResponse(Call<List<MaterialInModel.MaterialDatum>> call,
                                   Response<List<MaterialInModel.MaterialDatum>> response) {

                if (response.isSuccessful()) {
                    listener.onSuccessMaterial(response.body());
                } else {
                    listener.onFailure(Constant.SERVER_ERROR);
                }

            }


            @Override
            public void onFailure(Call<List<MaterialInModel.MaterialDatum>> call, Throwable t) {
                Log.e(TAG, "onFailure: " + t.toString());
                listener.onFailure(Constant.SERVER_ERROR);
            }
        });
    }


    public void addMaterial(List<Bitmap> bitmaps, List<LocationModel> locationModels,
                            List<MaterialInModel.MaterialRequestDatum> materialRequestData) {

        JSONObject jsonObject = new JSONObject();
        try {


            jsonObject.accumulate("user_role", PreferenceManager.getInstance().getUserRole());
            jsonObject.accumulate("project_id", MyApplication.getInstance().getProjectId());
            jsonObject.accumulate("date", UniqueUtils.getCurrentDate(Constant.YYYY_MM_DD_HH_MM_SS));
            jsonObject.accumulate("user_id", PreferenceManager.getInstance().getUserId());

            JSONArray jsonArray = new JSONArray();
            JSONArray dataArray = new JSONArray();

            for (int i = 0; i < materialRequestData.size(); i++) {

                JSONObject object = new JSONObject();
                object.accumulate("material_id", materialRequestData.get(i).materialId);
                object.accumulate("quantity", materialRequestData.get(i).quantity);
                object.accumulate("unit", materialRequestData.get(i).unit);
                dataArray.put(object);
            }

            for (int i = 0; i < bitmaps.size(); i++) {
                JSONObject object = new JSONObject();
                object.accumulate("data", UniqueUtils.bash64(bitmaps.get(i)));
                if (locationModels.size() != 0) {
                    object.accumulate("latitude", locationModels.get(i).latitude);
                    object.accumulate("longitude", locationModels.get(i).longitude);
                    object.accumulate("location", locationModels.get(i).address);
                } else {
                    object.accumulate("latitude", 0.0);
                    object.accumulate("longitude", 0.0);
                    object.accumulate("location", "");
                }
                jsonArray.put(object);
            }
            jsonObject.accumulate("material_audit_details_data", dataArray);
            jsonObject.accumulate("images", jsonArray);

        } catch (Exception e) {
            e.printStackTrace();
        }

        JsonElement jsonObject1 = JsonParser.parseString(jsonObject.toString());

        Call<ResponseMessage> messageCall = apiInterface.materialaudit(
                PreferenceManager.getInstance().getToken(), jsonObject1
        );

        messageCall.enqueue(new Callback<ResponseMessage>() {
            @Override
            public void onResponse(Call<ResponseMessage> call, Response<ResponseMessage> response) {
                if (response.isSuccessful()) {
                    listener.onSuccessAdd(response.body().message);
                } else {
                    listener.onFailure(Constant.SERVER_ERROR);
                }
            }

            @Override
            public void onFailure(Call<ResponseMessage> call, Throwable t) {
                Log.e(TAG, "onFailure: " + t.toString());
                listener.onFailure(Constant.SERVER_ERROR);
            }
        });

    }

    public void getAdminData(String date) {

        Call<MaterialAuditModel> objectCall = apiInterface.getAdminMaterialaudit(
                PreferenceManager.getInstance().getToken(),
                PreferenceManager.getInstance().getUserId(),
                MyApplication.getInstance().getProjectId(),
                date
        );

        objectCall.enqueue(new Callback<MaterialAuditModel>() {
            @Override
            public void onResponse(Call<MaterialAuditModel> call, Response<MaterialAuditModel> response) {
                if (response.isSuccessful()) {
                    listener.onAdminDataSuccess(response.body());
                } else {
                    listener.onFailure(Constant.SERVER_ERROR);
                }
            }

            @Override
            public void onFailure(Call<MaterialAuditModel> call, Throwable t) {
                Log.e(TAG, "onFailure: " + t.toString());
                listener.onFailure(Constant.SERVER_ERROR);
            }
        });

    }


    public void UpdateMaterial(
            List<MaterialTransferModel.Datum>materialRequestData ) {

        JSONObject jsonObject = new JSONObject();
        try {


            jsonObject.accumulate("user_role", PreferenceManager.getInstance().getUserRole());
            jsonObject.accumulate("project_id", MyApplication.getInstance().getProjectId());
            jsonObject.accumulate("date", UniqueUtils.getCurrentDate(Constant.YYYY_MM_DD_HH_MM_SS));
            jsonObject.accumulate("user_id", PreferenceManager.getInstance().getUserId());

            JSONArray dataArray = new JSONArray();

            for (int i = 0; i < materialRequestData.size(); i++) {

                JSONObject object = new JSONObject();
                object.accumulate("material_audit_header_id", materialRequestData.get(i).headerId);
                object.accumulate("material_audit_detail_id", materialRequestData.get(i).detailId);
                object.accumulate("material_id", materialRequestData.get(i).materialId);
                object.accumulate("quantity", materialRequestData.get(i).quantity);
                object.accumulate("remark", materialRequestData.get(i).remark);
                dataArray.put(object);
            }


            jsonObject.accumulate("material_audit_detail_data", dataArray);

        } catch (Exception e) {
            e.printStackTrace();
        }

        JsonElement jsonObject1 = JsonParser.parseString(jsonObject.toString());

        Call<ResponseMessage> messageCall = apiInterface.updatematerialaudit(
                PreferenceManager.getInstance().getToken(), jsonObject1
        );

        messageCall.enqueue(new Callback<ResponseMessage>() {
            @Override
            public void onResponse(Call<ResponseMessage> call, Response<ResponseMessage> response) {
                if (response.isSuccessful()) {
                    listener.onSuccessAdd(response.body().message);
                } else {
                    listener.onFailure(Constant.SERVER_ERROR);
                }
            }

            @Override
            public void onFailure(Call<ResponseMessage> call, Throwable t) {
                Log.e(TAG, "onFailure: " + t.toString());
                listener.onFailure(Constant.SERVER_ERROR);
            }
        });

    }

}
