package com.vnnogile.uniquerehab.materialaudit.view;

import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import com.vnnogile.uniquerehab.R;
import com.vnnogile.uniquerehab.global.MyApplication;
import com.vnnogile.uniquerehab.global.PreferenceManager;
import com.vnnogile.uniquerehab.global.UniqueUtils;
import com.vnnogile.uniquerehab.materialtransfer.view.MaterialTransferAdminFragment;
import com.vnnogile.uniquerehab.materialtransfer.view.MaterialTransferSiteFragment;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MaterialAuditActivity extends AppCompatActivity {

    public static final String TAG = MaterialAuditActivity.class.getSimpleName();
    @BindView(R.id.toolbar_menu)
    ImageView toolbarMenu;
    @BindView(R.id.toolbar_title)
    TextView toolbarTitle;
    @BindView(R.id.toolbar_profile)
    ImageView toolbarProfile;
    @BindView(R.id.toolbar_project)
    TextView toolbarProject;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_material_audit);
        ButterKnife.bind(this);

        loadVariables();
    }

    private void loadVariables() {
        toolbarTitle.setText("Material Audit");
        toolbarProfile.setImageResource(R.drawable.profile_picture);

        toolbarProject.setText(MyApplication.getInstance().getProjectName());
        Fragment fragment=null;

        if (PreferenceManager.getInstance().getUserRole().equalsIgnoreCase(UniqueUtils.ADMIN)){
            fragment= MaterialAuditAdminFragment.newInstance();
        }else {
            fragment= MaterialAuditSiteFragment.newInstance();
        }

        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.add(R.id.frame_container    ,fragment);
        fragmentTransaction.commit();
    }


    @OnClick(R.id.toolbar_profile)
    public void onViewClicked() {
        UniqueUtils.goToHomePage(this);
    }

    @OnClick(R.id.toolbar_menu)
    public void onMenuClicked() {
        onBackPressed();
    }
}
