package com.vnnogile.uniquerehab.materialaudit.view;

import android.app.DatePickerDialog;
import android.os.Bundle;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import com.vnnogile.uniquerehab.R;
import com.vnnogile.uniquerehab.global.Constant;
import com.vnnogile.uniquerehab.global.CustomCalendar;
import com.vnnogile.uniquerehab.global.DecimalValueFilter;
import com.vnnogile.uniquerehab.global.ProgressBar;
import com.vnnogile.uniquerehab.global.UniqueUtils;
import com.vnnogile.uniquerehab.materialaudit.controller.MaterialAuditController;
import com.vnnogile.uniquerehab.materialaudit.model.MaterialAuditModel;
import com.vnnogile.uniquerehab.materialinward.adapter.ImageAdapterWithUrl;
import com.vnnogile.uniquerehab.materialinward.model.MaterialInModel;
import com.vnnogile.uniquerehab.materialtransfer.model.MaterialTransferModel;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;


public class MaterialAuditAdminFragment extends Fragment implements MaterialAuditController.MaterialAuditListener, TextWatcher {
    @BindView(R.id.image_date)
    ImageView imageDate;
    @BindView(R.id.txt_date)
    TextView txtDate;
    @BindView(R.id.txt_time)
    TextView txtTime;
    @BindView(R.id.material_rv)
    RecyclerView materialRv;
    @BindView(R.id.image_rv)
    RecyclerView imageRv;
    @BindView(R.id.btn_submit)
    Button btnSubmit;
    @BindView(R.id.edit_quantity)
    EditText editQuantity;
    @BindView(R.id.edit_remark)
    EditText editRemark;
    @BindView(R.id.edit_name)
    EditText editName;
    @BindView(R.id.edit_u)
    EditText editU;
    // TODO: Rename parameter arguments, choose names that match


    public MaterialAuditAdminFragment() {
        // Required empty public constructor
    }

    //date
    private Calendar calendar = Calendar.getInstance();
    private Calendar selectedDate = Calendar.getInstance();
    private int mYear = calendar.get(Calendar.YEAR);
    private int mMonth = calendar.get(Calendar.MONTH);
    private int mDay = calendar.get(Calendar.DAY_OF_MONTH);


    private String date = "";
    private ProgressBar progressBar;

    private MaterialAuditAdapter auditAdapter;

    private MaterialAuditController controller;

    private ImageAdapterWithUrl adapterWithUrl;

    public static MaterialAuditAdminFragment newInstance() {
        MaterialAuditAdminFragment fragment = new MaterialAuditAdminFragment();
        Bundle args = new Bundle();

        fragment.setArguments(args);
        return fragment;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_material_audit_admin, container, false);
        ButterKnife.bind(this, view);
        loadVariable();
        return view;
    }

    private void loadVariable() {

        txtDate.setText(UniqueUtils.getCurrentDate("dd MMM yyyy"));
        txtTime.setText(UniqueUtils.getCurrentDate("HH:mm"));
        date = UniqueUtils.getCurrentDate(Constant.YYYY_MM_DD);


        progressBar = new ProgressBar(getContext());

        editQuantity.setFilters(new InputFilter[]{new DecimalValueFilter(1)});
        editQuantity.addTextChangedListener(this);

        controller = new MaterialAuditController(getContext());
        controller.setListener(this);


        if (UniqueUtils.isNetworkAvailable(getContext())) {
            progressBar.show();
            controller.getAdminData(date);
        } else {
            UniqueUtils.showErrorMessage(Constant.NETWORK_ERROR_MESSAGE);
        }

    }


    @OnClick({R.id.image_date, R.id.btn_add, R.id.btn_submit})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.btn_add:
                if (TextUtils.isEmpty(editQuantity.getText().toString().trim())) {
                    UniqueUtils.showWarningMessage("Enter Quantity");
                }else if (Double.parseDouble(editQuantity.getText().toString().trim())<1||
                        Double.parseDouble(editQuantity.getText().toString().trim())>1000){
                    UniqueUtils.showWarningMessage("Please enter quantity between 1 to 1000 ");
                } else if (TextUtils.isEmpty(editRemark.getText().toString().trim())) {
                    UniqueUtils.showWarningMessage("Enter Remark");
                } else if (indexPosition == -1) {
                    UniqueUtils.showWarningMessage("Select Material");
                } else {
                    auditAdapter.updateRemark(editQuantity.getText().toString().trim(),
                            editRemark.getText().toString().trim());
                    editQuantity.setText("");
                    editRemark.setText("");

                    editU.setText("");
                    editName.setText("");
                    for (int i = 0; i < model.datumList.size(); i++) {
                        if (model.datumList.get(i).isUpdated) {
                            btnSubmit.setVisibility(View.VISIBLE);
                            break;
                        } else {
                            btnSubmit.setVisibility(View.GONE);
                        }
                    }
                }
                break;
            case R.id.date_lyr:
            case R.id.image_date:
               /* DatePickerDialog datePickerDialog = new DatePickerDialog(getContext(), R.style.DatePicker, new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                        mYear = year;
                        mMonth = month;
                        mDay = dayOfMonth;
                        selectedDate.set(mYear, mMonth, mDay);
                        txtDate.setText(UniqueUtils.getDateInFormat(selectedDate.getTime(), "dd MMM yyyy"));
                        date = UniqueUtils.getDateInFormat(selectedDate.getTime(), Constant.YYYY_MM_DD);

                        //api call

                        if (UniqueUtils.isNetworkAvailable(getContext())) {
                            progressBar.show();
                            controller.getAdminData(date);
                        } else {
                            UniqueUtils.showErrorMessage(Constant.NETWORK_ERROR_MESSAGE);
                        }
                    }
                }, mYear, mMonth, mDay);
                datePickerDialog.getDatePicker().setMaxDate(new Date().getTime());
                datePickerDialog.show();*/
                CustomCalendar customCalendar = new CustomCalendar(getContext(),"material_audit");
                customCalendar.setListener(new CustomCalendar.CustomCalendarListener() {
                    @Override
                    public void onDateSet(Date selectedDate) {
                        txtDate.setText(UniqueUtils.getDateInFormat(selectedDate, "dd MMM yyyy"));
                        date = UniqueUtils.getDateInFormat(selectedDate, Constant.YYYY_MM_DD);
                        if (UniqueUtils.isNetworkAvailable(getContext())) {
                            progressBar.show();
                            controller.getAdminData(date);
                        } else {
                            UniqueUtils.showErrorMessage(Constant.NETWORK_ERROR_MESSAGE);
                        }
                    }
                });
                customCalendar.show();
                break;
            case R.id.btn_submit:

                if (auditAdapter != null && auditAdapter.getSelected().size() != 0) {
                    progressBar.show();
                    controller.UpdateMaterial(auditAdapter.getSelected());
                } else {
                    UniqueUtils.showWarningMessage("No updated item found");
                }

                break;
        }
    }

    @Override
    public void onSuccessMaterial(List<MaterialInModel.MaterialDatum> datumList) {
        progressBar.dismiss();
    }

    @Override
    public void onFailure(String errorMessage) {
        progressBar.dismiss();
        UniqueUtils.showErrorMessage(errorMessage);
    }

    @Override
    public void onSuccessAdd(String message) {
        progressBar.dismiss();
        UniqueUtils.showSuccessMessage(message);
        progressBar.show();
        controller.getAdminData(date);
    }

    private MaterialAuditModel model;

    @Override
    public void onAdminDataSuccess(MaterialAuditModel model) {
        progressBar.dismiss();
        this.model = model;
        if (this.model != null) {

            btnSubmit.setVisibility(View.GONE);
            auditAdapter = new MaterialAuditAdapter();
            materialRv.setAdapter(auditAdapter);


            adapterWithUrl = new ImageAdapterWithUrl(model.imageData, getContext(), null);
            imageRv.setAdapter(adapterWithUrl);

            if (model.datumList.size() == 0 && model.imageData.size() == 0) {
                UniqueUtils.showWarningMessage("No Data found");
            }
        }
    }

    int indexPosition = -1;

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {

    }

    @Override
    public void afterTextChanged(Editable s) {
        if (s.length()==1){
            if (s == editQuantity.getEditableText()){
                if (s.charAt(0)=='0'||s.charAt(0)=='.'){
                    editQuantity.setText("1");
                    editQuantity.setSelection(1);
                }
            }
        }
    }

    class MaterialAuditAdapter extends RecyclerView.Adapter<MaterialAuditAdapter.MaterialTransferAdd> {

        @NonNull
        @Override
        public MaterialTransferAdd onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.cell_naterial_add, parent, false);
            return new MaterialTransferAdd(view);
        }

        @Override
        public void onBindViewHolder(@NonNull MaterialTransferAdd holder, int position) {
            MaterialTransferModel.Datum datum = model.datumList.get(position);
            holder.txtMaterialName.setText("Material :" + datum.materialName);
            holder.txtQty.setText("Qty: " + datum.quantity);
            holder.txtUnit.setText("Unit: " + datum.unit);
            holder.txt_remark.setText("Remark: " + datum.remark);
            holder.txt_inventory.setText("In(Qty): " + datum.inventory_quantity);

            holder.deleteContainer.setVisibility(View.GONE);

            holder.imageEdit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    indexPosition = position;
                    editQuantity.setText(Math.round(Float.parseFloat(datum.quantity))+"");
                    editQuantity.setSelection(editQuantity.getText().toString().trim().length());
                    editU.setText(datum.unit);
                    editName.setText(datum.materialName);
                    notifyDataSetChanged();
                }
            });

            if (indexPosition == position) {
                holder.imageEdit.setImageResource(R.drawable.ic_edit_enable);
            } else {
                holder.imageEdit.setImageResource(R.drawable.ic_edit);
            }

            if (datum.isUpdated) {
                holder.imageCheck.setVisibility(View.VISIBLE);
                holder.imageCheck.setImageResource(R.drawable.ic_check);
            }

        }

        public void updateRemark(String qty, String remark) {
            MaterialTransferModel.Datum datum = model.datumList.get(indexPosition);
            datum.quantity = qty;
            datum.remark = remark;
            datum.isUpdated = true;
            notifyItemChanged(indexPosition);
            indexPosition = -1;
        }

        public List<MaterialTransferModel.Datum> getSelected() {
            List<MaterialTransferModel.Datum> selected = new ArrayList<>();

            for (int i = 0; i < model.datumList.size(); i++) {
                if (model.datumList.get(i).isUpdated) {
                    selected.add(model.datumList.get(i));
                }
            }

            return selected;

        }

        @Override
        public int getItemCount() {
            return model.datumList.size();
        }

        class MaterialTransferAdd extends RecyclerView.ViewHolder {
            @BindView(R.id.delete_container)
            RelativeLayout deleteContainer;
            @BindView(R.id.txt_material_name)
            TextView txtMaterialName;
            @BindView(R.id.txt_qty)
            TextView txtQty;
            @BindView(R.id.txt_unit)
            TextView txtUnit;
            @BindView(R.id.txt_remark)
            TextView txt_remark;
            @BindView(R.id.txt_inventory)
            TextView txt_inventory;

            @BindView(R.id.image_edit)
            ImageView imageEdit;
            @BindView(R.id.image_check)
            ImageView imageCheck;
            @BindView(R.id.ly_con)
            LinearLayout lyCon;


            MaterialTransferAdd(@NonNull View itemView) {
                super(itemView);
                ButterKnife.bind(this, itemView);
                lyCon.setVisibility(View.VISIBLE);
                imageEdit.setVisibility(View.VISIBLE);
                txt_remark.setVisibility(View.VISIBLE);
                txt_inventory.setVisibility(View.VISIBLE);
            }
        }
    }
}
