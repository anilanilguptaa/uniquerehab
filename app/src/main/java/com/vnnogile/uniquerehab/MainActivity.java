package com.vnnogile.uniquerehab;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import androidx.appcompat.app.AppCompatActivity;

import com.vnnogile.uniquerehab.dashboardscreen.view.DashboardActivity;
import com.vnnogile.uniquerehab.global.Constant;
import com.vnnogile.uniquerehab.global.PreferenceManager;
import com.vnnogile.uniquerehab.global.UniqueUtils;
import com.vnnogile.uniquerehab.loginscreen.controller.LoginController;
import com.vnnogile.uniquerehab.loginscreen.model.LoginResponse;
import com.vnnogile.uniquerehab.loginscreen.model.User;
import com.vnnogile.uniquerehab.loginscreen.view.LoginActivity;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends AppCompatActivity implements LoginController.LoginIController {

    @BindView(R.id.btn_retry)
    Button btnRetry;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);


        if (PreferenceManager.getInstance().isLogin()) {

            if (UniqueUtils.isNetworkAvailable(this)) {
               login();
            } else {
                UniqueUtils.showErrorMessage(Constant.NETWORK_ERROR_MESSAGE);
            }

        } else {
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    startActivity(new Intent(MainActivity.this, LoginActivity.class));
                    finish();
                }
            }, 2000);

        }

    }

    private void login(){
        LoginController loginController = new LoginController(this);
        User user = new User();
        user.password = PreferenceManager.getInstance().getPassword();
        user.username = PreferenceManager.getInstance().getUsername();
        loginController.login(user);
    }


    @Override
    public void onLoginSuccess(LoginResponse loginResponse) {
        if (loginResponse != null) {
            PreferenceManager.getInstance().setIsLogin(true);
            UniqueUtils.showSuccessMessage(loginResponse.message);
            startActivity(new Intent(MainActivity.this, DashboardActivity.class));
            finish();
        }else {
            UniqueUtils.showSuccessMessage(Constant.SOMETHING_WRONG);
        }
    }

    @Override
    public void onLoginFailed(String errorMessage) {
        UniqueUtils.showErrorMessage(errorMessage);
        btnRetry.setVisibility(View.VISIBLE);
    }

    @Override
    public void onLoginError(String errorMessage) {
        btnRetry.setVisibility(View.VISIBLE);
        UniqueUtils.showErrorMessage(errorMessage);
    }

    @OnClick(R.id.btn_retry)
    public void onViewClicked() {

        if (UniqueUtils.isNetworkAvailable(this)) {
            btnRetry.setVisibility(View.GONE);
            //login();
            startActivity(new Intent(MainActivity.this, LoginActivity.class));
            PreferenceManager.getInstance().clearUserData();
            finish();
        } else {
            UniqueUtils.showErrorMessage(Constant.NETWORK_ERROR_MESSAGE);
        }
    }
}
