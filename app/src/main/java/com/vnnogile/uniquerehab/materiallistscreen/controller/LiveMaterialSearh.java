package com.vnnogile.uniquerehab.materiallistscreen.controller;

import android.content.Context;
import android.util.Log;

import com.vnnogile.uniquerehab.global.Constant;
import com.vnnogile.uniquerehab.global.MyApplication;
import com.vnnogile.uniquerehab.global.PreferenceManager;
import com.vnnogile.uniquerehab.materialinward.model.MaterialInModel;
import com.vnnogile.uniquerehab.retrofit.ApiClient;
import com.vnnogile.uniquerehab.retrofit.ApiInterface;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LiveMaterialSearh {

    public static final String TAG = LiveMaterialSearh.class.getSimpleName();

    private Context context;
    private LiveSearchListener listener;
    private ApiInterface    apiInterface;

    public LiveMaterialSearh(Context context) {
        this.context = context;
        listener =(LiveSearchListener)context;
        apiInterface = ApiClient.getClient().create(ApiInterface.class);

    }

    public interface LiveSearchListener{
        void onSuccessResult(List<MaterialInModel.MaterialDatum> datumList);
        void onFailure(String errorMessage);
    }


    public void getData(String key){

        Call<List<MaterialInModel.MaterialDatum>>  objectCall = apiInterface.getMaterialListByTypeAhead(
                PreferenceManager.getInstance().getToken(),key
        );

        objectCall.enqueue(new Callback<List<MaterialInModel.MaterialDatum>>() {
            @Override
            public void onResponse(Call<List<MaterialInModel.MaterialDatum>> call,
                                   Response<List<MaterialInModel.MaterialDatum>> response) {
                listener.onSuccessResult(response.body());
            }

            @Override
            public void onFailure(Call<List<MaterialInModel.MaterialDatum>> call, Throwable throwable) {
                Log.e(TAG, "onFailure: "+throwable.toString() );
                listener.onFailure(Constant.SERVER_ERROR);
            }
        });

    }


    public void getDataById(String key){

        Call<List<MaterialInModel.MaterialDatum>>  objectCall = apiInterface.getMaterialListFromInventoryOnTypeAhead(
                PreferenceManager.getInstance().getToken(),
                PreferenceManager.getInstance().getUserId(),
                MyApplication.getInstance().getProjectId(),key
        );

        objectCall.enqueue(new Callback<List<MaterialInModel.MaterialDatum>>() {
            @Override
            public void onResponse(Call<List<MaterialInModel.MaterialDatum>> call,
                                   Response<List<MaterialInModel.MaterialDatum>> response) {
                listener.onSuccessResult(response.body());
            }

            @Override
            public void onFailure(Call<List<MaterialInModel.MaterialDatum>> call, Throwable throwable) {
                Log.e(TAG, "onFailure: "+throwable.toString() );
                listener.onFailure(Constant.SERVER_ERROR);
            }
        });

    }
}
