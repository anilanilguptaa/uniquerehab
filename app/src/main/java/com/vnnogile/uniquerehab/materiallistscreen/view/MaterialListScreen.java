package com.vnnogile.uniquerehab.materiallistscreen.view;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.RecyclerView;

import com.vnnogile.uniquerehab.R;
import com.vnnogile.uniquerehab.global.MyApplication;
import com.vnnogile.uniquerehab.global.UniqueUtils;
import com.vnnogile.uniquerehab.materialinward.model.MaterialInModel;
import com.vnnogile.uniquerehab.materiallistscreen.controller.LiveMaterialSearh;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MaterialListScreen extends AppCompatActivity implements TextWatcher,
        LiveMaterialSearh.LiveSearchListener,
        TextView.OnEditorActionListener {

    @BindView(R.id.toolbar_menu)
    ImageView toolbarMenu;
    @BindView(R.id.toolbar_project)
    TextView toolbarProject;
    @BindView(R.id.toolbar_name)
    TextView toolbarName;
    @BindView(R.id.toolbar_profile)
    ImageView toolbarProfile;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.image_date)
    ImageView imageDate;
    @BindView(R.id.txt_date)
    TextView txtDate;
    @BindView(R.id.date_lyr)
    RelativeLayout dateLyr;
    @BindView(R.id.image_time)
    ImageView imageTime;
    @BindView(R.id.txt_time)
    TextView txtTime;
    @BindView(R.id.txt_no_data)
    TextView txt_no_data;
    @BindView(R.id.search_bar)
    EditText searchBar;
    @BindView(R.id.search_icon)
    ImageView searchIcon;
    @BindView(R.id.material_rv)
    RecyclerView material_rv;

    private LiveMaterialSearh controller;

    private boolean isMasterList = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_material_list_screen);
        ButterKnife.bind(this);
        loadVariables();
    }

    private void loadVariables() {
        toolbarName.setText("Select Material");
        toolbarProject.setText(MyApplication.getInstance().getProjectName());
        txtDate.setText(UniqueUtils.getCurrentDate("dd MMM yyyy"));
        txtTime.setText(UniqueUtils.getCurrentDate("HH:mm"));
        toolbarMenu.setImageResource(R.drawable.ic_back);
        toolbar.setNavigationIcon(R.drawable.ic_back);

        setSupportActionBar(toolbar);

        isMasterList = getIntent().getBooleanExtra("isMasterList", false);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
        material_rv.addItemDecoration(new DividerItemDecoration(this, DividerItemDecoration.VERTICAL));
        searchBar.addTextChangedListener(this);
        searchBar.setOnEditorActionListener(this);
        controller = new LiveMaterialSearh(this);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {

        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
        }

        return super.onOptionsItemSelected(item);
    }

    @OnClick({R.id.toolbar_profile, R.id.image_date, R.id.date_lyr})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.toolbar_profile:
                UniqueUtils.goToHomePage(this);
                break;

            case R.id.date_lyr:
                break;
        }
    }

    @OnClick(R.id.search_icon)
    public void onViewClicked() {

    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {
        if (s.toString().length() > 0) {
            if (isMasterList)
                controller.getData(s.toString());
            else
                controller.getDataById(s.toString());
        }
    }

    @Override
    public void afterTextChanged(Editable s) {

    }

    @Override
    public void onSuccessResult(List<MaterialInModel.MaterialDatum> datumList) {
        material_rv.setAdapter(new MaterialLiveAdapter(datumList));

        if (datumList.size()==0){
            txt_no_data.setVisibility(View.VISIBLE);
        }else {
            txt_no_data.setVisibility(View.GONE);
        }
    }

    @Override
    public void onFailure(String errorMessage) {
        UniqueUtils.showErrorMessage(errorMessage);
    }

    @Override
    public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
        if (actionId == EditorInfo.IME_ACTION_SEARCH) {
            UniqueUtils.hideKeyboard(MaterialListScreen.this);
        }
        return false;
    }

    class MaterialLiveAdapter extends RecyclerView.Adapter<MaterialLiveAdapter.MaterialLiveView> {


        private List<MaterialInModel.MaterialDatum> datumList;

        public MaterialLiveAdapter(List<MaterialInModel.MaterialDatum> datumList) {
            this.datumList = datumList;
        }

        @NonNull
        @Override
        public MaterialLiveView onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
            View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.cell_single_text, viewGroup, false);
            return new MaterialLiveView(view);
        }

        @Override
        public void onBindViewHolder(@NonNull MaterialLiveView holder, int i) {
            MaterialInModel.MaterialDatum model = datumList.get(i);
            holder.txtMaterialName.setText(model.name);
            holder.txtMaterialName.setTextColor(Color.BLACK);
            holder.txtMaterialName.setPadding(20, 30, 30, 30);
        }

        @Override
        public int getItemCount() {
            return datumList.size();
        }

        class MaterialLiveView extends RecyclerView.ViewHolder {
            @BindView(R.id.txt_boq_name)
            TextView txtMaterialName;

            MaterialLiveView(@NonNull View itemView) {
                super(itemView);
                ButterKnife.bind(this, itemView);

                itemView.setOnClickListener(v -> {
                    UniqueUtils.hideKeyboard(MaterialListScreen.this);
                    MaterialInModel.MaterialDatum model = datumList.get(getAdapterPosition());
                    Intent intent = new Intent();
                    intent.putExtra("data", model);
                    setResult(Activity.RESULT_OK, intent);
                    finish();
                });
            }
        }
    }
}
