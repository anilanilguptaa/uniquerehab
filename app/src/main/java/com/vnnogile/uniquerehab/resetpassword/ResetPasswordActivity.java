package com.vnnogile.uniquerehab.resetpassword;

import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.util.Patterns;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import androidx.appcompat.app.AppCompatActivity;

import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import com.vnnogile.uniquerehab.R;
import com.vnnogile.uniquerehab.global.Constant;
import com.vnnogile.uniquerehab.global.ProgressBar;
import com.vnnogile.uniquerehab.global.ResponseMessage;
import com.vnnogile.uniquerehab.global.UniqueUtils;
import com.vnnogile.uniquerehab.retrofit.ApiClient;
import com.vnnogile.uniquerehab.retrofit.ApiInterface;

import org.json.JSONObject;

import javax.crypto.interfaces.PBEKey;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ResetPasswordActivity extends AppCompatActivity {

    public static final String TAG = ResetPasswordActivity.class.getSimpleName();
    @BindView(R.id.image_profile)
    ImageView imageProfile;
    @BindView(R.id.edit_username)
    EditText editUsername;
    @BindView(R.id.btn_login)
    Button btnLogin;

    ApiInterface apiInterface;
    private ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reset_passward);
        ButterKnife.bind(this);

        apiInterface = ApiClient.getClient().create(ApiInterface.class);

        progressBar= new ProgressBar(this);
    }

    @OnClick(R.id.btn_login)
    public void onViewClicked() {
        if (TextUtils.isEmpty(editUsername.getText().toString().trim())) {
            UniqueUtils.showWarningMessage("Email cannot be empty.");
        }else if (!Patterns.EMAIL_ADDRESS.matcher(editUsername.getText().toString().trim()).matches()){
            UniqueUtils.showWarningMessage("Invalid email.");
        }else {

            if (UniqueUtils.isNetworkAvailable(this)){
                progressBar.show();
                reset();
            }else {
                UniqueUtils.showErrorMessage(Constant.NETWORK_ERROR_MESSAGE);
            }

        }
    }

    private void reset(){
        JSONObject jsonObject = new JSONObject();

        try {
            jsonObject.accumulate("email",editUsername.getText().toString().trim());
        }catch (Exception e){
            e.printStackTrace();
        }

        JsonElement  jsonElement = JsonParser.parseString(jsonObject.toString());

        Call<ResponseMessage> messageCall = apiInterface.resetPassword(jsonElement
        );

        messageCall.enqueue(new Callback<ResponseMessage>() {
            @Override
            public void onResponse(Call<ResponseMessage> call, Response<ResponseMessage> response) {
                progressBar.dismiss();
                editUsername.setText("");

                if (response.body()!=null){
                    if (response.body().status){
                        UniqueUtils.showSuccessMessage(response.body().message);
                    }else {
                        UniqueUtils.showErrorMessage(response.body().message);
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseMessage> call, Throwable throwable) {
                Log.e(TAG, "onFailure: "+ throwable.toString() );
                UniqueUtils.showErrorMessage(Constant.SERVER_ERROR);
            }
        });
    }
}
