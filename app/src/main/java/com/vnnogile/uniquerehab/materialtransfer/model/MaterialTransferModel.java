package com.vnnogile.uniquerehab.materialtransfer.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.vnnogile.uniquerehab.materialinward.model.MaterialInWord;

import java.util.List;

public class MaterialTransferModel implements Parcelable{


    @SerializedName("material_transfer_data")
    @Expose
    public List<Datum>  datumList;

    @SerializedName("imageData")
    @Expose
    public List<MaterialInWord.ImageDatum>  imageData;

    protected MaterialTransferModel(Parcel in) {
        datumList = in.createTypedArrayList(Datum.CREATOR);
        imageData = in.createTypedArrayList(MaterialInWord.ImageDatum.CREATOR);
    }

    public static final Creator<MaterialTransferModel> CREATOR = new Creator<MaterialTransferModel>() {
        @Override
        public MaterialTransferModel createFromParcel(Parcel in) {
            return new MaterialTransferModel(in);
        }

        @Override
        public MaterialTransferModel[] newArray(int size) {
            return new MaterialTransferModel[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeTypedList(datumList);
        dest.writeTypedList(imageData);
    }


    public static class SiteData implements Parcelable {
        @SerializedName("id")
        @Expose
        public int id;
        @SerializedName("name")
        @Expose
        public String name;
        @SerializedName("description")
        @Expose
        public String description;
        @SerializedName("address")
        @Expose
        public String address;
        @SerializedName("created_at")
        @Expose
        public String createdAt;

        protected SiteData(Parcel in) {
            id = in.readInt();
            name = in.readString();
            description = in.readString();
            address = in.readString();
            createdAt = in.readString();
        }

        public static final Creator<SiteData> CREATOR = new Creator<SiteData>() {
            @Override
            public SiteData createFromParcel(Parcel in) {
                return new SiteData(in);
            }

            @Override
            public SiteData[] newArray(int size) {
                return new SiteData[size];
            }
        };

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeInt(id);
            dest.writeString(name);
            dest.writeString(description);
            dest.writeString(address);
            dest.writeString(createdAt);
        }
    }


    public static class Datum implements Parcelable{
        @SerializedName("header_id")
        @Expose
        public int headerId;
        @SerializedName("detail_id")
        @Expose
        public int detailId;
        @SerializedName("quantity")
        @Expose
        public String quantity;
        @SerializedName("inventory_quantity")
        @Expose
        public String inventory_quantity;
        @SerializedName("is_active")
        @Expose
        public int isActive;
        @SerializedName("material_name")
        @Expose
        public String materialName;
        @SerializedName("material_id")
        @Expose
        public String materialId;
        @SerializedName("unit")
        @Expose
        public String unit;

        public String remark="";

        public boolean isUpdated = false;

        public Datum() {
        }

        protected Datum(Parcel in) {
            headerId = in.readInt();
            detailId = in.readInt();
            quantity = in.readString();
            inventory_quantity = in.readString();
            isActive = in.readInt();
            materialName = in.readString();
            materialId = in.readString();
            unit = in.readString();
            remark = in.readString();
            isUpdated = in.readByte() != 0;
        }

        public static final Creator<Datum> CREATOR = new Creator<Datum>() {
            @Override
            public Datum createFromParcel(Parcel in) {
                return new Datum(in);
            }

            @Override
            public Datum[] newArray(int size) {
                return new Datum[size];
            }
        };

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeInt(headerId);
            dest.writeInt(detailId);
            dest.writeString(quantity);
            dest.writeString(inventory_quantity);
            dest.writeInt(isActive);
            dest.writeString(materialName);
            dest.writeString(materialId);
            dest.writeString(unit);
            dest.writeString(remark);
            dest.writeByte((byte) (isUpdated ? 1 : 0));
        }
    }
}
