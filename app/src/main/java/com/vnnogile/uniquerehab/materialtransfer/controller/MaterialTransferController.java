package com.vnnogile.uniquerehab.materialtransfer.controller;

import android.content.Context;
import android.graphics.Bitmap;
import android.util.Log;
import android.util.TypedValue;

import androidx.annotation.NonNull;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import com.google.gson.reflect.TypeToken;
import com.vnnogile.uniquerehab.global.Constant;
import com.vnnogile.uniquerehab.global.LocationModel;
import com.vnnogile.uniquerehab.global.MyApplication;
import com.vnnogile.uniquerehab.global.PreferenceManager;
import com.vnnogile.uniquerehab.global.ResponseMessage;
import com.vnnogile.uniquerehab.global.UniqueUtils;
import com.vnnogile.uniquerehab.materialinward.model.MaterialInModel;
import com.vnnogile.uniquerehab.materialtransfer.model.MaterialTransferModel;
import com.vnnogile.uniquerehab.retrofit.ApiClient;
import com.vnnogile.uniquerehab.retrofit.ApiInterface;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MaterialTransferController {

    public static final String TAG = MaterialTransferController.class.getSimpleName();

    private Context context;
    private ApiInterface apiInterface;
    private MaterialTransferListener listener;

    public MaterialTransferController(Context context) {
        this.context = context;
        apiInterface = ApiClient.getClient().create(ApiInterface.class);

    }

    public void setListener(MaterialTransferListener listener) {
        this.listener = listener;
    }

    public interface MaterialTransferListener{
        void onSuccessMaterial(List<MaterialInModel.MaterialDatum> datumList);
        void onSuccessSite(List<MaterialTransferModel.SiteData> siteData );
        void onFailure(String errorMessage);
        void onSuccessAdd(String Message);

        void onSuccessAdminData(MaterialTransferModel model);
    }

    public void getMaterial(){

        Call<List<MaterialInModel.MaterialDatum>>objectCall = apiInterface.getMaterialListForMaterialTransfer(
                PreferenceManager.getInstance().getToken(),
                PreferenceManager.getInstance().getUserId(),
                MyApplication.getInstance().getProjectId(),
                UniqueUtils.getCurrentDate(Constant.YYYY_MM_DD)
        );

        objectCall.enqueue(new Callback<List<MaterialInModel.MaterialDatum>>() {

            @Override
            public void onResponse(Call<List<MaterialInModel.MaterialDatum>> call,
                                   Response<List<MaterialInModel.MaterialDatum>> response) {

                if (response.isSuccessful()){
                    listener.onSuccessMaterial(response.body());
                }else {
                    listener.onFailure(Constant.SERVER_ERROR);
                }

            }


            @Override
            public void onFailure(Call<List<MaterialInModel.MaterialDatum>> call, Throwable t) {
                Log.e(TAG, "onFailure: "+t.toString() );
                listener.onFailure(Constant.SERVER_ERROR);
            }
        });
    }


    public void getSitesData(){

        Call<List<MaterialTransferModel.SiteData>>objectCall = apiInterface.getSitesData(
                PreferenceManager.getInstance().getToken(),
                PreferenceManager.getInstance().getUserId(),
                MyApplication.getInstance().getProjectId(),
                UniqueUtils.getCurrentDate(Constant.YYYY_MM_DD)
        );

        objectCall.enqueue(new Callback<List<MaterialTransferModel.SiteData>>() {

            @Override
            public void onResponse(Call<List<MaterialTransferModel.SiteData>> call,
                                   Response<List<MaterialTransferModel.SiteData>> response) {

                if (response.isSuccessful()){

                    if (response.body()!=null){

                        listener.onSuccessSite(response.body());
                    }else {
                        listener.onFailure(Constant.SOMETHING_WRONG);
                    }


                }else {
                    listener.onFailure(Constant.SERVER_ERROR);
                }

            }


            @Override
            public void onFailure(Call<List<MaterialTransferModel.SiteData>> call, Throwable t) {
                Log.e(TAG, "onFailure: "+t.toString() );
                listener.onFailure(Constant.SERVER_ERROR);
            }
        });
    }

    public void addMaterial(String siteId, List<Bitmap> bitmaps, List<LocationModel>locationModels,
                            List<MaterialInModel.MaterialRequestDatum>materialRequestData ){

        JSONObject jsonObject=new JSONObject();
        try{


            jsonObject.accumulate("user_role",PreferenceManager.getInstance().getUserRole());
            jsonObject.accumulate("project_id", MyApplication.getInstance().getProjectId());
            jsonObject.accumulate("date",UniqueUtils.getCurrentDate(Constant.YYYY_MM_DD_HH_MM_SS));
            jsonObject.accumulate("user_id",PreferenceManager.getInstance().getUserId());
            jsonObject.accumulate("to_site_id",siteId);

            JSONArray jsonArray = new JSONArray();
            JSONArray dataArray = new JSONArray();

            for (int i = 0; i < materialRequestData.size(); i++) {

                JSONObject object = new JSONObject();
                object.accumulate("material_id",materialRequestData.get(i).materialId);
                object.accumulate("quantity",materialRequestData.get(i).quantity);
                object.accumulate("unit",materialRequestData.get(i).unit);
                dataArray.put(object);
            }

            for (int i = 0; i < bitmaps.size(); i++) {
                JSONObject object = new JSONObject();
                object.accumulate("data",UniqueUtils.bash64(bitmaps.get(i)));
                if (locationModels.size() != 0) {
                    object.accumulate("latitude", locationModels.get(i).latitude);
                    object.accumulate("longitude", locationModels.get(i).longitude);
                    object.accumulate("location", locationModels.get(i).address);
                } else {
                    object.accumulate("latitude", 0.0);
                    object.accumulate("longitude", 0.0);
                    object.accumulate("location", "");
                }
                jsonArray.put(object);
            }
            jsonObject.accumulate("material_transfer_details_data",dataArray);
            jsonObject.accumulate("images",jsonArray);

        }catch (Exception e ){
            e.printStackTrace();
        }

        JsonElement jsonObject1 = JsonParser.parseString(jsonObject.toString());

        Call<ResponseMessage> messageCall = apiInterface.materialtransfer(
                PreferenceManager.getInstance().getToken(),jsonObject1
        );

        messageCall.enqueue(new Callback<ResponseMessage>() {
            @Override
            public void onResponse(Call<ResponseMessage> call, Response<ResponseMessage> response) {
                if (response.isSuccessful()){
                    if (response.body().status){
                        UniqueUtils.showSuccessMessage(response.body().message);
                    }else {
                        UniqueUtils.showErrorMessage(response.body().message);
                    }
                    listener.onSuccessAdd(response.body().message);
                }else {
                    listener.onFailure(Constant.SERVER_ERROR);
                }
            }

            @Override
            public void onFailure(Call<ResponseMessage> call, Throwable t) {
                Log.e(TAG, "onFailure: "+t.toString() );
                listener.onFailure(Constant.SERVER_ERROR);
            }
        });

    }

    public void getmaterialtransfer(String date){

        Call<MaterialTransferModel>    objectCall = apiInterface.getmaterialtransfer(
                PreferenceManager.getInstance().getToken(),
                PreferenceManager.getInstance().getUserId(),
                MyApplication.getInstance().getProjectId(),
               date
        );

        objectCall.enqueue(new Callback<MaterialTransferModel>() {

            @Override
            public void onResponse(Call<MaterialTransferModel> call, Response<MaterialTransferModel> response) {

                if (response.isSuccessful()){
                    listener.onSuccessAdminData(response.body());
                }else {
                    listener.onFailure(Constant.SERVER_ERROR);
                }

            }

            @Override
            public void onFailure(Call<MaterialTransferModel> call, Throwable t) {
                Log.e(TAG, "onFailure: "+t.toString() );
                listener.onFailure(Constant.SERVER_ERROR);
            }
        });
    }

    public void deleteImage(String id){

        JSONObject jsonObject = new JSONObject();
        try {

            jsonObject.accumulate("material_transfer_document_id",id);
            jsonObject.accumulate("project_id", MyApplication.getInstance().getProjectId());
            jsonObject.accumulate("date", UniqueUtils.getCurrentDate(Constant.YYYY_MM_DD_HH_MM_SS));
            jsonObject.accumulate("user_id", PreferenceManager.getInstance().getUserId());

        } catch (Exception e) {
            e.printStackTrace();
        }

        JsonElement jsonObject1 = JsonParser.parseString(jsonObject.toString());


        Call<ResponseMessage>    objectCall = apiInterface.deleteMaterialTransferImage(
                PreferenceManager.getInstance().getToken(),jsonObject1

        );

        objectCall.enqueue(new Callback<ResponseMessage>() {

            @Override
            public void onResponse(Call<ResponseMessage> call, Response<ResponseMessage> response) {

                if (response.isSuccessful()){

                    if (response.body().status){
                        UniqueUtils.showSuccessMessage(response.body().message);
                    }else {
                        UniqueUtils.showErrorMessage(response.body().message);
                    }

                    listener.onSuccessAdd("");
                }else {
                    listener.onFailure(Constant.SERVER_ERROR);
                }

            }

            @Override
            public void onFailure(Call<ResponseMessage> call, Throwable t) {
                Log.e(TAG, "onFailure: "+t.toString() );
                listener.onFailure(Constant.SERVER_ERROR);
            }
        });
    }

    public void deleteMaterialTransferData(String id){

        JSONObject jsonObject = new JSONObject();
        try {

            jsonObject.accumulate("material_transfer_detail_id",id);
            jsonObject.accumulate("project_id", MyApplication.getInstance().getProjectId());
            jsonObject.accumulate("date", UniqueUtils.getCurrentDate(Constant.YYYY_MM_DD_HH_MM_SS));
            jsonObject.accumulate("user_id", PreferenceManager.getInstance().getUserId());

        } catch (Exception e) {
            e.printStackTrace();
        }

        JsonElement jsonObject1 = JsonParser.parseString(jsonObject.toString());


        Call<ResponseMessage>    objectCall = apiInterface.deleteMaterialTransferData(
                PreferenceManager.getInstance().getToken(),jsonObject1

        );

        objectCall.enqueue(new Callback<ResponseMessage>() {

            @Override
            public void onResponse(@NonNull Call<ResponseMessage> call,@NonNull Response<ResponseMessage> response) {

                if (response.isSuccessful()){

                    assert response.body() != null;
                    if (response.body().isStatus()){
                        UniqueUtils.showSuccessMessage(response.body().message);
                    }else {
                        UniqueUtils.showErrorMessage(response.body().message);
                    }

                    listener.onSuccessAdd("");
                }else {
                    listener.onFailure(Constant.SERVER_ERROR);
                }

            }

            @Override
            public void onFailure(Call<ResponseMessage> call, Throwable t) {
                Log.e(TAG, "onFailure: "+t.toString() );
                listener.onFailure(Constant.SERVER_ERROR);
            }
        });
    }
}
