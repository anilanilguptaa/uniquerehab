package com.vnnogile.uniquerehab.materialtransfer.view;


import android.app.DatePickerDialog;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import com.vnnogile.uniquerehab.R;
import com.vnnogile.uniquerehab.global.Constant;
import com.vnnogile.uniquerehab.global.CustomCalendar;
import com.vnnogile.uniquerehab.global.ProgressBar;
import com.vnnogile.uniquerehab.global.UniqueUtils;
import com.vnnogile.uniquerehab.materialinward.adapter.ImageAdapterWithUrl;
import com.vnnogile.uniquerehab.materialinward.model.MaterialInModel;
import com.vnnogile.uniquerehab.materialtransfer.controller.MaterialTransferController;
import com.vnnogile.uniquerehab.materialtransfer.model.MaterialTransferModel;

import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * A simple {@link Fragment} subclass.
 */
public class MaterialTransferAdminFragment extends Fragment implements MaterialTransferController.MaterialTransferListener, ImageAdapterWithUrl.Listener {


    @BindView(R.id.image_date)
    ImageView imageDate;
    @BindView(R.id.txt_date)
    TextView txtDate;
    @BindView(R.id.txt_time)
    TextView txtTime;
    @BindView(R.id.material_rv)
    RecyclerView materialRv;
    @BindView(R.id.image_rv)
    RecyclerView image_rv;

    public MaterialTransferAdminFragment() {
        // Required empty public constructor
    }


    //date
    private Calendar calendar = Calendar.getInstance();
    private Calendar selectedDate = Calendar.getInstance();
    private int mYear = calendar.get(Calendar.YEAR);
    private int mMonth = calendar.get(Calendar.MONTH);
    private int mDay = calendar.get(Calendar.DAY_OF_MONTH);


    private String date = "";
    private ProgressBar progressBar;

    private MaterialTransferController controller;

    private ImageAdapterWithUrl adapterWithUrl;

    private MaterialTransferAddAdapter transferAddAdapter;

    public static MaterialTransferAdminFragment newInstance() {

        Bundle args = new Bundle();

        MaterialTransferAdminFragment fragment = new MaterialTransferAdminFragment();
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_material_transfer_admin, container, false);
        ButterKnife.bind(this, view);

        loadVariable();


        return view;
    }

    private void loadVariable() {

        txtDate.setText(UniqueUtils.getCurrentDate("dd MMM yyyy"));
        txtTime.setText(UniqueUtils.getCurrentDate("HH:mm"));
        date = UniqueUtils.getCurrentDate(Constant.YYYY_MM_DD);


        progressBar = new ProgressBar(getContext());
        controller = new MaterialTransferController(getContext());
        controller.setListener(this);


        if (UniqueUtils.isNetworkAvailable(getContext())) {
            progressBar.show();
            controller.getmaterialtransfer(date);
        } else {
            UniqueUtils.showErrorMessage(Constant.NETWORK_ERROR_MESSAGE);
        }

    }

    @OnClick({R.id.image_date,R.id.date_lyr})
    public void onViewClicked() {

      /*  DatePickerDialog datePickerDialog = new DatePickerDialog(getContext(), R.style.DatePicker, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                mYear = year;
                mMonth = month;
                mDay = dayOfMonth;
                selectedDate.set(mYear, mMonth, mDay);
                txtDate.setText(UniqueUtils.getDateInFormat(selectedDate.getTime(), "dd MMM yyyy"));
                date = UniqueUtils.getDateInFormat(selectedDate.getTime(), Constant.YYYY_MM_DD);

                //api call

                if (UniqueUtils.isNetworkAvailable(getContext())) {
                    progressBar.show();
                    controller.getmaterialtransfer(date);
                } else {
                    UniqueUtils.showErrorMessage(Constant.NETWORK_ERROR_MESSAGE);
                }
            }
        }, mYear, mMonth, mDay);
         datePickerDialog.getDatePicker().setMaxDate(new Date().getTime());
        datePickerDialog.show();*/
        CustomCalendar customCalendar = new CustomCalendar(getContext(),"material_transfer");
        customCalendar.setListener(new CustomCalendar.CustomCalendarListener() {
            @Override
            public void onDateSet(Date selectedDate) {
                txtDate.setText(UniqueUtils.getDateInFormat(selectedDate, "dd MMM yyyy"));
                date = UniqueUtils.getDateInFormat(selectedDate, Constant.YYYY_MM_DD);
                if (UniqueUtils.isNetworkAvailable(getContext())) {
                    progressBar.show();
                    controller.getmaterialtransfer(date);
                } else {
                    UniqueUtils.showErrorMessage(Constant.NETWORK_ERROR_MESSAGE);
                }
            }
        });
        customCalendar.show();

    }

    @Override
    public void onSuccessMaterial(List<MaterialInModel.MaterialDatum> datumList) {

    }

    @Override
    public void onSuccessSite(List<MaterialTransferModel.SiteData> siteData) {

    }

    @Override
    public void onFailure(String errorMessage) {
        progressBar.dismiss();
        UniqueUtils.showErrorMessage(errorMessage);
    }

    @Override
    public void onSuccessAdd(String Message) {
        progressBar.dismiss();
        progressBar.show();
        controller.getmaterialtransfer(date);
    }

    @Override
    public void onSuccessAdminData(MaterialTransferModel model) {
        progressBar.dismiss();

        transferAddAdapter = new MaterialTransferAddAdapter(model);
        materialRv.setAdapter(transferAddAdapter);

        adapterWithUrl = new ImageAdapterWithUrl(model.imageData,getContext(),MaterialTransferAdminFragment.this);
        image_rv.setAdapter(adapterWithUrl);

        if (model.datumList.size()==0 && model.imageData.size()==0){
            UniqueUtils.showWarningMessage("No Data found");
        }
    }

    @Override
    public void onDeleteImage(int id) {
        if (UniqueUtils.isNetworkAvailable(Objects.requireNonNull(getContext()))){
            progressBar.show();
            controller.deleteImage(String.valueOf(id));
        }else {
            UniqueUtils.showErrorMessage(Constant.NETWORK_ERROR_MESSAGE);
        }
    }

    class MaterialTransferAddAdapter extends RecyclerView.Adapter<MaterialTransferAddAdapter.MaterialTransferAdd> {

        private MaterialTransferModel transferModel;

        MaterialTransferAddAdapter(MaterialTransferModel transferModel) {
            this.transferModel = transferModel;
        }

        @NonNull
        @Override
        public MaterialTransferAdd onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.cell_naterial_add, parent, false);
            return new MaterialTransferAdd(view);
        }

        @Override
        public void onBindViewHolder(@NonNull MaterialTransferAdd holder, int position) {

            MaterialTransferModel.Datum datum = transferModel.datumList.get(position);
            holder.txtMaterialName.setText("Material :" + datum.materialName);
            holder.txtQty.setText("Qty: " + datum.quantity);
            holder.txtUnit.setText("Unit: " + datum.unit);

            holder.deleteContainer.setOnClickListener(v->{
                if (UniqueUtils.isNetworkAvailable(Objects.requireNonNull(getContext()))){
                    progressBar.show();
                    controller.deleteMaterialTransferData(String.valueOf(datum.detailId));
                }else {
                    UniqueUtils.showErrorMessage(Constant.NETWORK_ERROR_MESSAGE);
                }
            });
        }

        @Override
        public int getItemCount() {
            return transferModel.datumList.size();
        }

        class MaterialTransferAdd extends RecyclerView.ViewHolder {
            @BindView(R.id.delete_container)
            RelativeLayout deleteContainer;
            @BindView(R.id.txt_material_name)
            TextView txtMaterialName;
            @BindView(R.id.txt_qty)
            TextView txtQty;
            @BindView(R.id.txt_unit)
            TextView txtUnit;

            MaterialTransferAdd(@NonNull View itemView) {
                super(itemView);
                ButterKnife.bind(this, itemView);
            }
        }
    }

}
