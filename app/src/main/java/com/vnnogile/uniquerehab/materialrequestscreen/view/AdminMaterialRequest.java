package com.vnnogile.uniquerehab.materialrequestscreen.view;


import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.fragment.app.Fragment;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.recyclerview.widget.RecyclerView;

import com.vnnogile.uniquerehab.R;
import com.vnnogile.uniquerehab.global.Constant;
import com.vnnogile.uniquerehab.global.CustomCalendar;
import com.vnnogile.uniquerehab.global.DecimalValueFilter;
import com.vnnogile.uniquerehab.global.ProgressBar;
import com.vnnogile.uniquerehab.global.UniqueUtils;
import com.vnnogile.uniquerehab.materialinward.model.MaterialInModel;
import com.vnnogile.uniquerehab.materialrequestscreen.adapter.MaterialRequestAdapter;
import com.vnnogile.uniquerehab.materialrequestscreen.controller.MaterialRequestController;
import com.vnnogile.uniquerehab.materialrequestscreen.service.UpdateIntentService;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.vnnogile.uniquerehab.materialrequestscreen.adapter.MaterialRequestAdapter.DELETE;
import static com.vnnogile.uniquerehab.materialrequestscreen.adapter.MaterialRequestAdapter.EDIT;

/**
 * A simple {@link Fragment} subclass.
 */
public class AdminMaterialRequest extends Fragment implements MaterialRequestController.MaterialRequestListener,
        MaterialRequestAdapter.MaterialAdListener, TextWatcher {

    public static final String TAG = AdminMaterialRequest.class.getSimpleName();

    @BindView(R.id.image_date)
    ImageView imageDate;
    @BindView(R.id.txt_date)
    TextView txtDate;
    @BindView(R.id.txt_time)
    TextView txtTime;
    @BindView(R.id.edit_qty)
    EditText editQty;
    @BindView(R.id.edit_price)
    EditText editPrice;
    @BindView(R.id.btn_update)
    Button btnUpdate;
    @BindView(R.id.material_rv)
    RecyclerView materialRv;
    @BindView(R.id.btn_submit)
    Button btnSubmit;


    public AdminMaterialRequest() {

    }

    private String date = "";
    private ProgressBar progressBar;
    private MaterialRequestController controller;
    private MaterialInModel datumList;
    private MaterialRequestAdapter materialRequestAdapter;
    MaterialInModel.MaterialRequestDatum updateModel = null;
    MyReceiver myReceiver;

    public static AdminMaterialRequest newInstance() {

        Bundle args = new Bundle();

        AdminMaterialRequest fragment = new AdminMaterialRequest();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_admin_material_request, container, false);
        ButterKnife.bind(this, view);
        loadVariables();
        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        setReceiver();
    }

    @Override
    public void onResume() {
        super.onResume();
        getData();
    }

    @Override
    public void onStop() {
        super.onStop();
        LocalBroadcastManager.getInstance(getContext()).unregisterReceiver(myReceiver);
    }


    private void setReceiver() {
        myReceiver = new MyReceiver();
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(TAG);

        LocalBroadcastManager.getInstance(getContext()).registerReceiver(myReceiver, intentFilter);
    }

    private void startService(String mode) {
        if (getContext() != null) {
            ArrayList<MaterialInModel.MaterialRequestDatum> data = new ArrayList<>(materialRequestAdapter.getSelected());
            Intent intent = new Intent(getContext(), UpdateIntentService.class);
            intent.putExtra("mode", mode);
            intent.putParcelableArrayListExtra("data", data);
            getContext().startService(intent);
        }
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {

    }

    @Override
    public void afterTextChanged(Editable s) {
        if (s.length()==1){
            if (s == editQty.getEditableText())
            if (s.charAt(0)=='0'||s.charAt(0)=='.'){
                editQty.setText("1");
                editQty.setSelection(1);
            }
            /* if (s == editPrice.getEditableText())
            if (s.charAt(0)=='.'){
                editPrice.setText("0");
                editPrice.setSelection(1);
            }*/


        }
    }


    private class MyReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            boolean success = intent.getBooleanExtra("success", false);
            Log.e(TAG, "onReceive: " + success);
            if (success) {
                getData();
            }
        }
    }

    private void loadVariables() {
        txtDate.setText(UniqueUtils.getCurrentDate("dd MMM yyyy"));
        txtTime.setText(UniqueUtils.getCurrentDate("HH:mm"));

        date = UniqueUtils.getCurrentDate(Constant.YYYY_MM_DD);

        controller = new MaterialRequestController(getContext());
        controller.setListener(this);

        progressBar = new ProgressBar(getContext());

        editQty.setFilters(new InputFilter[]{new DecimalValueFilter(1)});

        editQty.addTextChangedListener(this);
   //     editPrice.addTextChangedListener(this);

    }

    private void getData() {
        if (UniqueUtils.isNetworkAvailable(getContext())) {
            progressBar.show();
            controller.getDropDown(date);
        } else {
            UniqueUtils.showErrorMessage(Constant.NETWORK_ERROR_MESSAGE);
        }
    }

    private boolean appInstalledOrNot() {
        PackageManager pm = getActivity().getPackageManager();
        boolean app_installed;
        try {
            pm.getPackageInfo("com.whatsapp", PackageManager.GET_ACTIVITIES);
            app_installed = true;
        } catch (PackageManager.NameNotFoundException e) {
            app_installed = false;
        }
        return app_installed;
    }

    private String message = "";
    private String htmlmsg = "";

    private void showBottomSheet() {
        View view = LayoutInflater.from(getContext()).inflate(R.layout.dialog_select_share, null);
        Dialog dialog = new Dialog(getContext());
        dialog.setContentView(view);


        ImageView imageView = view.findViewById(R.id.image_gmail);
        ImageView whatsapp = view.findViewById(R.id.image_whats);


        if (dialog.getWindow() != null) {
            dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
            dialog.getWindow().setGravity(Gravity.BOTTOM);
            dialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;

        }

        String div = "-------------------------------------------------------";
        message = "Sr.\t\tMaterial \t\tUnit\t\tQty\n" + div + "\n";

        for (int i = 0; i < materialRequestAdapter.getSelected().size(); i++) {
            message += (i + 1) + "." + "\t\t" + materialRequestAdapter.getSelected().get(i).materialName + "\t\t" +
                    materialRequestAdapter.getSelected().get(i).unit + "\t\t" +
                    materialRequestAdapter.getSelected().get(i).quantity + "\n" +
                      div + "\n";
        }


        htmlmsg = "Sr.\t\tMaterial \t\tUnit\t\tQty\t\tPrice<br/>" + div + "<br/>";

        for (int i = 0; i < materialRequestAdapter.getSelected().size(); i++) {
            htmlmsg += (i + 1) + "." + "\t\t" + materialRequestAdapter.getSelected().get(i).materialName + "\t\t" +
                    materialRequestAdapter.getSelected().get(i).unit + "\t\t" +
                    materialRequestAdapter.getSelected().get(i).quantity + "\n"
                  + "<br/>" + div + "<br/>";
        }

        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                startService("email");
                Uri uri = Uri.parse("mailto:")
                        .buildUpon()
                        .appendQueryParameter("subject", "Material Request")
                        .appendQueryParameter("body", htmlmsg)
                        .appendQueryParameter("type", "text/html")
                        .build();

                Intent emailIntent = new Intent(Intent.ACTION_SENDTO, uri);
                //If you are using HTML in your body text

                startActivity(Intent.createChooser(emailIntent, "Select Email Client"));
            }
        });

        whatsapp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();

                if (appInstalledOrNot()) {
                    try {
                        startService("whatsapp");
                        Intent sendIntent = new Intent();
                        sendIntent.setAction(Intent.ACTION_SEND);
                        sendIntent.putExtra(Intent.EXTRA_TEXT, message);
                        sendIntent.setType("text/plain");
                        // Put this line here
                        sendIntent.setPackage("com.whatsapp");
                        //
                        startActivity(sendIntent);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else {
                    UniqueUtils.showWarningMessage("Whatsapp No Installed.");
                }
            }
        });

        dialog.show();

    }

    @OnClick({R.id.image_date,R.id.date_lyr})
    public void onViewClicked(View view) {

               /* DatePickerDialog datePickerDialog = new DatePickerDialog(getContext(), R.style.DatePicker, new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                        mYear = year;
                        mMonth = month;
                        mDay = dayOfMonth;
                        selectedDate.set(mYear, mMonth, mDay);
                        txtDate.setText(UniqueUtils.getDateInFormat(selectedDate.getTime(), "dd MMM yyyy"));
                        date = UniqueUtils.getDateInFormat(selectedDate.getTime(), Constant.YYYY_MM_DD);

                        //api call

                        getData();
                    }
                }, mYear, mMonth, mDay);
                datePickerDialog.getDatePicker().setMaxDate(new Date().getTime());
                datePickerDialog.show();*/
                CustomCalendar customCalendar = new CustomCalendar(getContext(),"material_request");
                customCalendar.setListener(new CustomCalendar.CustomCalendarListener() {
                    @Override
                    public void onDateSet(Date selectedDate) {
                        txtDate.setText(UniqueUtils.getDateInFormat(selectedDate, "dd MMM yyyy"));
                        date = UniqueUtils.getDateInFormat(selectedDate, Constant.YYYY_MM_DD);
                        getData();
                    }
                });
                customCalendar.show();

    }

    @OnClick({R.id.btn_update, R.id.btn_submit})
    public void onClicked(View view) {
        switch (view.getId()) {
            case R.id.btn_update:
                if (editQty.getText().toString().trim().isEmpty()) {
                    UniqueUtils.showWarningMessage("Enter Quantity");
                } else if (Double.parseDouble(editQty.getText().toString().trim())<1||
                        Double.parseDouble(editQty.getText().toString().trim())>1000){
                    UniqueUtils.showWarningMessage("Please enter quantity between 1 to 1000 ");
                }/*else if (editPrice.getText().toString().trim().isEmpty()) {
                    UniqueUtils.showWarningMessage("Enter Price");
                } else if (editPrice.getText().toString().trim().charAt(0) == '.') {
                    UniqueUtils.showWarningMessage("Price Cannot start with dot.");
                }*/ else if (updateModel == null) {
                    UniqueUtils.showWarningMessage("Edit Material");
                } else {
                    updateModel.quantity = editQty.getText().toString().trim();
                    updateModel.price = /*editPrice.getText().toString().trim()*/"";
                    if (UniqueUtils.isNetworkAvailable(getContext())) {
                        progressBar.show();
                        controller.addMaterial(updateModel);
                       // editPrice.setText("");
                        editQty.setText("");
                    } else {
                        UniqueUtils.showErrorMessage(Constant.NETWORK_ERROR_MESSAGE);
                    }
                }
                break;
            case R.id.btn_submit:

                if (materialRequestAdapter != null && materialRequestAdapter.getSelected().size() > 0) {
                    try {
                        showBottomSheet();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else {
                    UniqueUtils.showWarningMessage("No material selected");
                }
                break;
        }
    }

    @Override
    public void onFailure(String errorMessage) {
        progressBar.dismiss();
        UniqueUtils.showErrorMessage(errorMessage);
    }

    @Override
    public void onSuccessDropDown(MaterialInModel datumList) {
        progressBar.dismiss();
        this.datumList = datumList;

        if (datumList.materialRequestData.size() == 0) {
            UniqueUtils.showWarningMessage("No Data");
        }
        materialRequestAdapter = new MaterialRequestAdapter(getContext(), datumList, this);
        materialRv.setAdapter(materialRequestAdapter);

    }

    @Override
    public void onSuccessAdd(String message) {
        progressBar.dismiss();
        updateModel = null;
        UniqueUtils.showSuccessMessage(message);
        getData();
    }

    @Override
    public void onItemClick(int type, MaterialInModel.MaterialRequestDatum datum) {
        updateModel = null;
        switch (type) {
            case EDIT:
                editQty.setText(datum.quantity+"");
                editQty.setSelection(editQty.getText().toString().trim().length());
                updateModel = datum;
                break;
            case DELETE:

                if (UniqueUtils.isNetworkAvailable(getContext())) {
                    progressBar.show();
                    controller.deleteMaterial(datum);
                } else {
                    UniqueUtils.showErrorMessage(Constant.NETWORK_ERROR_MESSAGE);
                }
                break;
        }

    }
}
