package com.vnnogile.uniquerehab.materialrequestscreen.view;


import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import com.vnnogile.uniquerehab.R;
import com.vnnogile.uniquerehab.global.Constant;
import com.vnnogile.uniquerehab.global.DecimalValueFilter;
import com.vnnogile.uniquerehab.global.ProgressBar;
import com.vnnogile.uniquerehab.global.UniqueUtils;
import com.vnnogile.uniquerehab.materialinward.adapter.MaterialInwardAdapter;
import com.vnnogile.uniquerehab.materialinward.model.MaterialInModel;
import com.vnnogile.uniquerehab.materialinward.model.MaterialInWord;
import com.vnnogile.uniquerehab.materiallistscreen.view.MaterialListScreen;
import com.vnnogile.uniquerehab.materialrequestscreen.controller.MaterialRequestController;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static android.app.Activity.RESULT_OK;

/**
 * A simple {@link Fragment} subclass.
 */
public class SiteEnggMaterialFragment extends Fragment implements MaterialRequestController.MaterialRequestListener,
        MaterialInwardAdapter.MaterialListener, TextWatcher {

    public static final String TAG = SiteEnggMaterialFragment.class.getSimpleName();

    @BindView(R.id.image_date)
    ImageView imageDate;
    @BindView(R.id.txt_date)
    TextView txtDate;
    @BindView(R.id.txt_time)
    TextView txtTime;
    @BindView(R.id.image_time)
    ImageView imageTime;
    @BindView(R.id.material_spinner)
    Spinner materialSpinner;
    @BindView(R.id.container)
    LinearLayout container;
    @BindView(R.id.edit_quantity)
    EditText editQuantity;
    @BindView(R.id.edit_unit)
    EditText editUnit;
    @BindView(R.id.edit_material)
    EditText edit_material;
    @BindView(R.id.btn_add)
    Button btnAdd;
    @BindView(R.id.material_rv)
    RecyclerView materialRv;
    @BindView(R.id.btn_submit)
    Button btnSubmit;

    public SiteEnggMaterialFragment() {
        // Required empty public constructor
    }

    private MaterialRequestController controller;
    private ProgressBar progressBar;
    private String date="";
    private int materialId=-1;
    private static int REQUEST_mATERIAL = 2;
    private MaterialInModel.MaterialDatum materialData=null;
    private MaterialInwardAdapter materialInwardAdapter;
    private List<MaterialInWord.DetailsDatum> inwardsData = new ArrayList<>();

    public static SiteEnggMaterialFragment newInstance() {

        Bundle args = new Bundle();

        SiteEnggMaterialFragment fragment = new SiteEnggMaterialFragment();
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_site_engg_material, container, false);
        ButterKnife.bind(this, view);
        loadVariables();
        return view;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK)
        if (requestCode == REQUEST_mATERIAL&&data!=null){
            Log.e(TAG, "onActivityResult: search data");
            materialData=data.getParcelableExtra("data");
            editUnit.setText(materialData.unit);
            edit_material.setText(materialData.name);
        }
    }

    private void loadVariables() {
        txtDate.setText(UniqueUtils.getCurrentDate("dd MMM yyyy"));
        txtTime.setText(UniqueUtils.getCurrentDate("HH:mm"));
        editUnit.setFocusable(false);
        edit_material.setFocusable(false);
        date = UniqueUtils.getCurrentDate(Constant.YYYY_MM_DD);
        controller = new MaterialRequestController(getContext());
        controller.setListener(this);
        progressBar= new ProgressBar(getContext());

        editQuantity.setFilters(new InputFilter[]{new DecimalValueFilter(1)});
        editQuantity.addTextChangedListener(this);

       /* if (UniqueUtils.isNetworkAvailable(getContext())){
            progressBar.show();
            controller.getDropDown(date);
        }else {
            UniqueUtils.showErrorMessage(Constant.NETWORK_ERROR_MESSAGE);
        }*/

        materialInwardAdapter = new MaterialInwardAdapter(inwardsData, getContext(), R.layout.cell_material_in_regular);
        materialInwardAdapter.setListener(this);
        materialRv.setAdapter(materialInwardAdapter);

    }

    private void setSpinnerData(List<MaterialInModel.MaterialDatum> datumList) {

        ArrayList<String> data = new ArrayList<>();
        data.add("--Select Material--");

        for (int i = 0; i < datumList.size(); i++) {
            data.add(datumList.get(i).name);
        }

        // Creating adapter for spinner
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(getContext(), android.R.layout.simple_spinner_item, data);
        // Drop down layout style - list view with radio button
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        // attaching data adapter to spinner
        materialSpinner.setAdapter(dataAdapter);

        materialSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position != 0) {
                    // Toasty.success(getActivity(), data.get(position)).show();
                   editUnit.setText(datumList.get(position-1).unit);
                   materialId = datumList.get(position-1).id;
                } else {
                    materialId=-1;
                   editUnit.setText("");
                    TextView textView = (TextView) view;
                    if (textView != null)
                        textView.setTextColor(getResources().getColor(R.color.colorDarkGrey));
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

    }

    @OnClick(R.id.image_date)
    public void onViewClicked() {
    }

    @OnClick({R.id.btn_add, R.id.btn_submit,R.id.edit_material})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.btn_add:
                if (materialData ==null){
                    UniqueUtils.showWarningMessage("Select Material");
                }else if (TextUtils.isEmpty(editQuantity.getText().toString().trim())){
                    UniqueUtils.showWarningMessage("Enter Quantity");
                }else if (Double.parseDouble(editQuantity.getText().toString().trim())<1||
                        Double.parseDouble(editQuantity.getText().toString().trim())>1000){
                    UniqueUtils.showWarningMessage("Please enter quantity between 1 to 1000 ");
                }else if (TextUtils.isEmpty(editUnit.getText().toString().trim())){
                    UniqueUtils.showWarningMessage("Enter Unit");
                }else {
                    double qty = Double.parseDouble(editQuantity.getText().toString().trim());
                    if (qty==0){
                        UniqueUtils.showWarningMessage("Quantity cannot be zero");
                    }else {
                        MaterialInWord.DetailsDatum data = new MaterialInWord.DetailsDatum();
                        data.materialId = materialData.id;
                        data.materialName = materialData.name;
                        data.quantity = editQuantity.getText().toString().trim();
                        data.unit = editUnit.getText().toString().trim();
                        data.status = "pending";
                        data.price = "";
                        inwardsData.add(data);
                        materialInwardAdapter.notifyDataSetChanged();
                        editQuantity.setText("");
                        editUnit.setText("");
                        edit_material.setText("");
                        materialData=null;
                      //  materialSpinner.setSelection(0);
                    }
                }
                break;
            case R.id.btn_submit:

                if (inwardsData.size()==0){
                    UniqueUtils.showWarningMessage("Add Material");
                }else {
                    progressBar.show();
                    controller.addMaterial(inwardsData);
                }


                break;
            case R.id.edit_material:
                Intent intent = new Intent(getContext(), MaterialListScreen.class);
                intent.putExtra("isMasterList",true);
                startActivityForResult(intent,REQUEST_mATERIAL);
                break;
        }
    }

    @Override
    public void onFailure(String errorMessage) {
        progressBar.dismiss();
        UniqueUtils.showErrorMessage(errorMessage);
    }

    @Override
    public void onSuccessDropDown(MaterialInModel datumList) {
        progressBar.dismiss();
        setSpinnerData(datumList.materialData);
    }

    @Override
    public void onSuccessAdd(String message) {
        progressBar.dismiss();
        UniqueUtils.showSuccessMessage(message);
        inwardsData.clear();
        materialInwardAdapter.notifyDataSetChanged();
    }

    @Override
    public void onClickItem(MaterialInWord.DetailsDatum datum) {
        Log.e("jc", "onClickItem: ");
    }

    @Override
    public void onItemDelete(MaterialInWord.DetailsDatum datum) {
        inwardsData.remove(datum);
        materialInwardAdapter.notifyDataSetChanged();
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {

    }

    @Override
    public void afterTextChanged(Editable s) {
        if (s.length()==1){
            if (s.charAt(0)=='0'||s.charAt(0)=='.'){
                editQuantity.setText("1");
                editQuantity.setSelection(1);
            }
        }
    }
}
