package com.vnnogile.uniquerehab.materialrequestscreen.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.vnnogile.uniquerehab.R;
import com.vnnogile.uniquerehab.global.UniqueUtils;
import com.vnnogile.uniquerehab.materialinward.model.MaterialInModel;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MaterialRequestAdapter extends RecyclerView.Adapter<MaterialRequestAdapter.MaterialRequestView> {

    public static final String TAG = MaterialRequestAdapter.class.getSimpleName();

    public static final int EDIT = 1;
    public static final int DELETE = 2;


    private Context context;
    private MaterialInModel materialInModel;
    private MaterialAdListener listener;
    private int indexPosition = -1;

    public interface MaterialAdListener {
        void onItemClick(int type, MaterialInModel.MaterialRequestDatum datum);
    }

    public MaterialRequestAdapter(Context context, MaterialInModel materialInModel, MaterialAdListener listener) {
        this.context = context;
        this.materialInModel = materialInModel;
        this.listener = listener;
    }


    @NonNull
    @Override
    public MaterialRequestView onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.cell_material_request, parent, false);
        return new MaterialRequestView(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MaterialRequestView holder, int position) {
        MaterialInModel.MaterialRequestDatum datum = materialInModel.materialRequestData.get(position);

        holder.txtUnit.setText("Unit:" + UniqueUtils.removeChar(datum.unit));
        holder.txtMaterialName.setText("Material:" + UniqueUtils.removeChar(datum.materialName));
        holder.txtQty.setText("Qty:" + datum.quantity);
        holder.txtPrice.setText("Price:" + datum.price);


        holder.deleteContainer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (listener != null) {
                    listener.onItemClick(DELETE, datum);
                }
            }
        });


        if (datum.status.equalsIgnoreCase("pending")) {

            holder.deleteContainer.setVisibility(View.VISIBLE);
            holder.imageEdit.setVisibility(View.VISIBLE);
        } else {

            holder.deleteContainer.setVisibility(View.GONE);
            holder.imageEdit.setVisibility(View.GONE);
        }

        holder.imageEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                indexPosition = position;
                notifyDataSetChanged();
                if (listener != null) {
                    listener.onItemClick(EDIT, datum);
                }
            }
        });

        if (indexPosition == position) {
            holder.imageEdit.setImageResource(R.drawable.ic_edit_enable);
        } else {
            holder.imageEdit.setImageResource(R.drawable.ic_edit);
        }

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                datum.isChecked = !datum.isChecked;
                holder.imageCheck.setVisibility(datum.isChecked ? View.VISIBLE : View.GONE);
            }
        });


    }

    public List<MaterialInModel.MaterialRequestDatum> getSelected() {
        List<MaterialInModel.MaterialRequestDatum> selected = new ArrayList<>();
        for (int i = 0; i < materialInModel.materialRequestData.size(); i++) {
            if (materialInModel.materialRequestData.get(i).isChecked) {
                selected.add(materialInModel.materialRequestData.get(i));
            }
        }
        return selected;
    }

    @Override
    public int getItemCount() {
        return materialInModel.materialRequestData.size();
    }

    class MaterialRequestView extends RecyclerView.ViewHolder {
        @BindView(R.id.delete_container)
        RelativeLayout deleteContainer;
        @BindView(R.id.image_edit)
        ImageView imageEdit;
        @BindView(R.id.image_check)
        ImageView imageCheck;
        @BindView(R.id.txt_material_name)
        TextView txtMaterialName;
        @BindView(R.id.txt_qty)
        TextView txtQty;
        @BindView(R.id.txt_unit)
        TextView txtUnit;
        @BindView(R.id.txt_price)
        TextView txtPrice;

        MaterialRequestView(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            txtPrice.setVisibility(View.GONE);
        }
    }
}
