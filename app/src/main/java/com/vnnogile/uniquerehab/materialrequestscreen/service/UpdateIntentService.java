package com.vnnogile.uniquerehab.materialrequestscreen.service;

import android.app.IntentService;
import android.content.Intent;
import android.util.Log;

import androidx.annotation.Nullable;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import com.vnnogile.uniquerehab.global.Constant;
import com.vnnogile.uniquerehab.global.MyApplication;
import com.vnnogile.uniquerehab.global.PreferenceManager;
import com.vnnogile.uniquerehab.global.ResponseMessage;
import com.vnnogile.uniquerehab.global.UniqueUtils;
import com.vnnogile.uniquerehab.materialinward.model.MaterialInModel;
import com.vnnogile.uniquerehab.retrofit.ApiClient;
import com.vnnogile.uniquerehab.retrofit.ApiInterface;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class UpdateIntentService extends IntentService {

    public static final String TAG = UpdateIntentService.class.getSimpleName();

    public UpdateIntentService() {
        super(TAG);
    }

    String mode="";
    ApiInterface    apiInterface;
    Intent intent;
    ArrayList<MaterialInModel.MaterialRequestDatum>data;
    @Override
    protected void onHandleIntent(@Nullable Intent intent) {
        this.intent=intent;
        apiInterface = ApiClient.getClient().create(ApiInterface.class);
        mode = intent.getStringExtra("mode");
        data = intent.getParcelableArrayListExtra("data");
        doInBackground();
//        LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(intent.putExtra("broadcastMessage", echoMessage));
    }

    private void doInBackground() {

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.accumulate("user_id", PreferenceManager.getInstance().getUserId());
            jsonObject.accumulate("project_id", MyApplication.getInstance().getProjectId());
            jsonObject.accumulate("date", UniqueUtils.getCurrentDate(Constant.YYYY_MM_DD_HH_MM_SS));

            JSONArray jsonArray = new JSONArray();

            for (int i = 0; i < data.size(); i++) {
                JSONObject jsonOb = new JSONObject();
                jsonOb.accumulate("material_id", data.get(i).materialId);
                jsonOb.accumulate("header_id",data.get(i).headerId);
                jsonOb.accumulate("detail_id", data.get(i).detailId);
                jsonOb.accumulate("mode",mode);

                jsonArray.put(jsonOb);
            }

            jsonObject.accumulate("material_request_details_data", jsonArray);

        } catch (Exception e) {
            e.printStackTrace();
        }

        JsonElement jsonObject1 = JsonParser.parseString(jsonObject.toString());

        Call<ResponseMessage> messageCall = apiInterface.materialUpdaterequest(
                PreferenceManager.getInstance().getToken(),jsonObject1
        );

        messageCall.enqueue(new Callback<ResponseMessage>() {
            @Override
            public void onResponse(Call<ResponseMessage> call, Response<ResponseMessage> response) {
                if (response.isSuccessful()){
                    LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(intent.putExtra("success", true));

                }else {
                    LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(intent.putExtra("success", false));

                }
            }

            @Override
            public void onFailure(Call<ResponseMessage> call, Throwable t) {
                Log.e(TAG, "onFailure: "+t.toString() );
                LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(intent.putExtra("success", false));
            }
        });
    }
}
