package com.vnnogile.uniquerehab.materialrequestscreen.controller;

import android.content.Context;
import android.util.Log;

import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import com.vnnogile.uniquerehab.global.Constant;
import com.vnnogile.uniquerehab.global.MyApplication;
import com.vnnogile.uniquerehab.global.PreferenceManager;
import com.vnnogile.uniquerehab.global.ResponseMessage;
import com.vnnogile.uniquerehab.global.UniqueUtils;
import com.vnnogile.uniquerehab.materialinward.model.MaterialInModel;
import com.vnnogile.uniquerehab.materialinward.model.MaterialInWord;
import com.vnnogile.uniquerehab.retrofit.ApiClient;
import com.vnnogile.uniquerehab.retrofit.ApiInterface;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MaterialRequestController {

    public static final String TAG = MaterialRequestController.class.getSimpleName();

    private Context context;
    private ApiInterface apiInterface;
    private MaterialRequestListener listener;

    public MaterialRequestController(Context context) {
        this.context = context;
        apiInterface = ApiClient.getClient().create(ApiInterface.class);

    }

    public void setListener(MaterialRequestListener listener) {
        this.listener = listener;
    }

    public interface MaterialRequestListener {
        void onFailure(String errorMessage);

        void onSuccessDropDown(MaterialInModel datumList);

        void onSuccessAdd(String message);
    }

    public void getDropDown(String date) {

        Call<MaterialInModel> listCall = apiInterface.
                materialrequest(
                        PreferenceManager.getInstance().getToken(),
                        PreferenceManager.getInstance().getUserId(),
                        MyApplication.getInstance().getProjectId(),
                        date
                );

        listCall.enqueue(new Callback<MaterialInModel>() {
            @Override
            public void onResponse(Call<MaterialInModel> call,
                                   Response<MaterialInModel> response) {

                if (response.isSuccessful()) {
                    listener.onSuccessDropDown(response.body());
                } else {
                    listener.onFailure(Constant.SERVER_ERROR);
                }

            }

            @Override
            public void onFailure(Call<MaterialInModel> call, Throwable t) {
                listCall.cancel();
                listener.onFailure(Constant.SERVER_ERROR);
            }
        });
    }

    public void addMaterial(List<MaterialInWord.DetailsDatum> inwardsData) {

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.accumulate("user_role", PreferenceManager.getInstance().getUserRole());
            jsonObject.accumulate("user_id", PreferenceManager.getInstance().getUserId());
            jsonObject.accumulate("project_id", MyApplication.getInstance().getProjectId());
            jsonObject.accumulate("date", UniqueUtils.getCurrentDate(Constant.YYYY_MM_DD_HH_MM_SS));

            JSONArray jsonArray = new JSONArray();

            for (int i = 0; i < inwardsData.size(); i++) {
                JSONObject jsonOb = new JSONObject();
                jsonOb.accumulate("material_id", inwardsData.get(i).materialId);
                jsonOb.accumulate("quantity", inwardsData.get(i).quantity);
                jsonOb.accumulate("unit", inwardsData.get(i).unit);
                jsonOb.accumulate("price", inwardsData.get(i).price);

                jsonArray.put(jsonOb);
            }

            jsonObject.accumulate("material_request_details_data", jsonArray);

        } catch (Exception e) {
            e.printStackTrace();
        }

        JsonElement jsonObject1 = JsonParser.parseString(jsonObject.toString());


        Call<ResponseMessage> messageCall = apiInterface.materialAddrequest(PreferenceManager.getInstance().getToken(),
                jsonObject1);

        messageCall.enqueue(new Callback<ResponseMessage>() {
            @Override
            public void onResponse(Call<ResponseMessage> call, Response<ResponseMessage> response) {
                if (response.isSuccessful()){
                    listener.onSuccessAdd(response.body().message);
                }else {
                    listener.onFailure(Constant.SERVER_ERROR);
                }
            }

            @Override
            public void onFailure(Call<ResponseMessage> call, Throwable t) {
                Log.e(TAG, "onFailure: "+t.toString() );
                listener.onFailure(Constant.SERVER_ERROR);
            }
        });
    }


    public void addMaterial(MaterialInModel.MaterialRequestDatum inwardsData) {

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.accumulate("user_id", PreferenceManager.getInstance().getUserId());
            jsonObject.accumulate("project_id", MyApplication.getInstance().getProjectId());
            jsonObject.accumulate("date", UniqueUtils.getCurrentDate(Constant.YYYY_MM_DD_HH_MM_SS));

            JSONArray jsonArray = new JSONArray();

            for (int i = 0; i < 1; i++) {
                JSONObject jsonOb = new JSONObject();
                jsonOb.accumulate("material_id", inwardsData.materialId);
                jsonOb.accumulate("quantity", inwardsData.quantity);
                jsonOb.accumulate("header_id", inwardsData.headerId);
                jsonOb.accumulate("detail_id", inwardsData.detailId);
                jsonOb.accumulate("price","");

                jsonArray.put(jsonOb);
            }
            jsonObject.accumulate("material_request_details_data", jsonArray);

        } catch (Exception e) {
            e.printStackTrace();
        }

        JsonElement jsonObject1 = JsonParser.parseString(jsonObject.toString());


        Call<ResponseMessage> messageCall = apiInterface.updatePriceOfMaterialRequest(PreferenceManager.getInstance().getToken(),
                jsonObject1);

        messageCall.enqueue(new Callback<ResponseMessage>() {
            @Override
            public void onResponse(Call<ResponseMessage> call, Response<ResponseMessage> response) {
                if (response.isSuccessful()){
                    listener.onSuccessAdd(response.body().message);
                }else {
                    listener.onFailure(Constant.SERVER_ERROR);
                }
            }

            @Override
            public void onFailure(Call<ResponseMessage> call, Throwable t) {
                Log.e(TAG, "onFailure: "+t.toString() );
                listener.onFailure(Constant.SERVER_ERROR);
            }
        });
    }

    public void deleteMaterial(MaterialInModel.MaterialRequestDatum inwardsData) {

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.accumulate("user_id", PreferenceManager.getInstance().getUserId());
            jsonObject.accumulate("project_id", MyApplication.getInstance().getProjectId());
            jsonObject.accumulate("material_request_detail_id", inwardsData.detailId);
            jsonObject.accumulate("date", UniqueUtils.getCurrentDate(Constant.YYYY_MM_DD_HH_MM_SS));


        } catch (Exception e) {
            e.printStackTrace();
        }

        JsonElement jsonObject1 = JsonParser.parseString(jsonObject.toString());


        Call<ResponseMessage> messageCall = apiInterface.deletematerialrequest(PreferenceManager.getInstance().getToken(),
                jsonObject1);

        messageCall.enqueue(new Callback<ResponseMessage>() {
            @Override
            public void onResponse(Call<ResponseMessage> call, Response<ResponseMessage> response) {
                if (response.isSuccessful()){
                    listener.onSuccessAdd(response.body().message);
                }else {
                    listener.onFailure(Constant.SERVER_ERROR);
                }
            }

            @Override
            public void onFailure(Call<ResponseMessage> call, Throwable t) {
                Log.e(TAG, "onFailure: "+t.toString() );
                listener.onFailure(Constant.SERVER_ERROR);
            }
        });
    }

}
