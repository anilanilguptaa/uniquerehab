package com.vnnogile.uniquerehab.typescreen.view;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.vnnogile.uniquerehab.R;
import com.vnnogile.uniquerehab.loginscreen.view.LoginActivity;
import com.vnnogile.uniquerehab.typescreen.adapter.ProjectTypeAdapter;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ProjectTypeActivity extends AppCompatActivity {

    public static final String TAG = ProjectTypeActivity.class.getSimpleName();

    private ArrayList<String> projectType = new ArrayList<>();

    @BindView(R.id.toolbar_menu)
    ImageView toolbarMenu;
    @BindView(R.id.toolbar_profile)
    ImageView toolbarProfile;
    @BindView(R.id.toolbar)
    RelativeLayout toolbar;
    @BindView(R.id.project_rv)
    RecyclerView projectRv;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_project_type);
        ButterKnife.bind(this);

        toolbarProfile.setImageResource(R.drawable.profile_picture);
        loadData();
    }

    private void loadData() {
        projectType.add("Projects");
        projectType.add("Services");
        projectType.add("Awards");
        projectType.add("Certification");
        projectRv.setAdapter(new ProjectTypeAdapter(projectType));
    }

    @OnClick({R.id.toolbar_menu, R.id.toolbar_profile})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.toolbar_menu:
                break;
            case R.id.toolbar_profile:
                startActivity(new Intent(this, LoginActivity.class));
                break;
        }
    }
}
