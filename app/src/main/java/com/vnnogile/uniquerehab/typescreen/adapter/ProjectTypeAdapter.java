package com.vnnogile.uniquerehab.typescreen.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.vnnogile.uniquerehab.R;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ProjectTypeAdapter extends RecyclerView.Adapter<ProjectTypeAdapter.ProjectTypeViewHolder> {


    private ArrayList<String> arrayList;

    public ProjectTypeAdapter(ArrayList<String> arrayList) {
        this.arrayList = arrayList;
    }

    @NonNull
    @Override
    public ProjectTypeViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.cell_project_type, parent, false);
        return new ProjectTypeViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ProjectTypeViewHolder holder, int position) {
        String data = arrayList.get(position);
        holder.txtLbl.setText(data);
    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    class ProjectTypeViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.txt_lbl)
        TextView txtLbl;
        ProjectTypeViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
