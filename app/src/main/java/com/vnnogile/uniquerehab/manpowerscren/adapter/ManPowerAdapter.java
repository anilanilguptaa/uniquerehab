package com.vnnogile.uniquerehab.manpowerscren.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;
import com.vnnogile.uniquerehab.R;
import com.vnnogile.uniquerehab.manpowerscren.model.ManPowerModel;
import com.vnnogile.uniquerehab.zoomimageview.view.ZoomImageViewActivity;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ManPowerAdapter extends RecyclerView.Adapter<ManPowerAdapter.ManPowerViewHolder> {

    private List<ManPowerModel.ManpowerDatum> manpowerData;
    private ManpowerListener listener;
    private Context context;

    public ManPowerAdapter(List<ManPowerModel.ManpowerDatum> manpowerData, Context context,ManpowerListener listener) {
        this.manpowerData = manpowerData;
        this.context=context;
        this.listener = listener;
    }

    public interface ManpowerListener{
        void onDeleteManpower(ManPowerModel.ManpowerDatum datum);
    }

    @NonNull
    @Override
    public ManPowerViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.cell_man_power, parent, false);
        return new ManPowerViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ManPowerViewHolder holder, int position) {
        ManPowerModel.ManpowerDatum datum = manpowerData.get(position);

        holder.txtContractor.setText("Contractor:"+datum.firstName);
        holder.txtCount.setText("Count:"+datum.noOfWorkers);

        Picasso.get().load(datum.imagePath).into(holder.imageMan);

        holder.cardDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (listener!=null)
                    listener.onDeleteManpower(datum);
            }
        });

        holder.image_click_view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, ZoomImageViewActivity.class);
                intent.putExtra("singlePhoto",datum.imagePath);
                intent.putExtra("location",datum.location);
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return manpowerData.size();
    }

    class ManPowerViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.image_man)
        ImageView imageMan;
        @BindView(R.id.image_click_view)
        ImageView image_click_view;
        @BindView(R.id.txt_contractor)
        TextView txtContractor;
        @BindView(R.id.txt_count)
        TextView txtCount;
        @BindView(R.id.card_delete)
        CardView cardDelete;

        ManPowerViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
