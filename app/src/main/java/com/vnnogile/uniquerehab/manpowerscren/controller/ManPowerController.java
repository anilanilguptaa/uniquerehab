package com.vnnogile.uniquerehab.manpowerscren.controller;

import android.content.Context;
import android.graphics.Bitmap;
import android.util.Log;

import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import com.vnnogile.uniquerehab.global.Constant;
import com.vnnogile.uniquerehab.global.LocationModel;
import com.vnnogile.uniquerehab.global.MyApplication;
import com.vnnogile.uniquerehab.global.PreferenceManager;
import com.vnnogile.uniquerehab.global.ResponseMessage;
import com.vnnogile.uniquerehab.global.UniqueUtils;
import com.vnnogile.uniquerehab.manpowerscren.model.ManPowerModel;
import com.vnnogile.uniquerehab.retrofit.ApiClient;
import com.vnnogile.uniquerehab.retrofit.ApiInterface;

import org.json.JSONArray;
import org.json.JSONObject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ManPowerController {

    public static final String TAG = ManPowerController.class.getSimpleName();

    private Context context;
    private ManPowerListener listener;
    private ApiInterface apiInterface;

    public ManPowerController(Context context) {
        this.context = context;
        listener = (ManPowerListener) context;
        apiInterface = ApiClient.getClient().create(ApiInterface.class);
    }

    public interface ManPowerListener {
        void onSuccess(ManPowerModel model);

        void onSuccessAdd();

        void onSuccessDelete();

        void onFailure(String errorMessage);
    }

    public void getManPowerData(String date) {
        Call<ManPowerModel> objectCall = apiInterface.getManpower(
                PreferenceManager.getInstance().getToken(),
                PreferenceManager.getInstance().getUserDetails().userDetail.id + "",
                MyApplication.getInstance().getProjectId(),
                date
        );

        objectCall.enqueue(new Callback<ManPowerModel>() {

            @Override
            public void onResponse(Call<ManPowerModel> call, Response<ManPowerModel> response) {
                if (response.isSuccessful()) {
                    listener.onSuccess(response.body());
                } else {
                    listener.onFailure(Constant.SERVER_ERROR);
                }
            }

            @Override
            public void onFailure(Call<ManPowerModel> call, Throwable t) {
                Log.e(TAG, "onFailure: " + t.toString());
                listener.onFailure(Constant.SERVER_ERROR);
            }
        });
    }

    public void addManPowerData(int contractorId, String count, Bitmap bitmap,
                                LocationModel locationModel, String date) {

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.accumulate("project_id", MyApplication.getInstance().getProjectId());
            jsonObject.accumulate("user_id", PreferenceManager.getInstance().getUserDetails().userDetail.id);
            jsonObject.accumulate("date", /*UniqueUtils.getCurrentDate(Constant.YYYY_MM_DD_HH_MM_SS)*/date);

            JSONArray jsonArray = new JSONArray();

            for (int i = 0; i < 1; i++) {
                JSONObject object = new JSONObject();
                object.accumulate("contractor_id", contractorId);
                object.accumulate("no_of_workers", count);
                jsonArray.put(object);
            }

            jsonObject.accumulate("manpower_data", jsonArray);

            JSONArray array = new JSONArray();

            for (int i = 0; i < 1; i++) {
                JSONObject object = new JSONObject();
                object.accumulate("data", UniqueUtils.bash64(bitmap));
                object.accumulate("latitude", locationModel.latitude);
                object.accumulate("longitude", locationModel.longitude);
                object.accumulate("location", locationModel.address);
                array.put(object);

            }

            jsonObject.put("images", array);
        } catch (Exception e) {
            e.printStackTrace();
        }

        JsonElement jsonElement = JsonParser.parseString(jsonObject.toString());

        Call<ResponseMessage> objectCall = apiInterface.uploadManPower(PreferenceManager.getInstance().getToken(),
                jsonElement);

        objectCall.enqueue(new Callback<ResponseMessage>() {

            @Override
            public void onResponse(Call<ResponseMessage> call, Response<ResponseMessage> response) {
                if (response.isSuccessful()) {
                    if (response.body().isStatus()) {
                        UniqueUtils.showSuccessMessage(response.body().message);
                    }else {
                        UniqueUtils.showErrorMessage(response.body().message);
                    }
                    listener.onSuccessAdd();
                } else {
                    listener.onFailure(Constant.SERVER_ERROR);
                }
            }

            @Override
            public void onFailure(Call<ResponseMessage> call, Throwable t) {
                Log.e(TAG, "onFailure: " + t.toString());
                listener.onFailure(Constant.SERVER_ERROR);
            }
        });

    }

    public void deleteManpower(String id) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.accumulate("id", id);
            jsonObject.accumulate("user_id", PreferenceManager.getInstance().getUserId());
            jsonObject.accumulate("project_id", MyApplication.getInstance().getProjectId());
        } catch (Exception e) {
            e.printStackTrace();
        }

        JsonElement jsonElement = JsonParser.parseString(jsonObject.toString());

        Call<Object> objectCall = apiInterface.deleteManPower(PreferenceManager.getInstance().getToken(),
                jsonElement);

        objectCall.enqueue(new Callback<Object>() {

            @Override
            public void onResponse(Call<Object> call, Response<Object> response) {
                if (response.isSuccessful()) {
                    listener.onSuccessDelete();
                } else {
                    listener.onFailure(Constant.SERVER_ERROR);
                }
            }

            @Override
            public void onFailure(Call<Object> call, Throwable t) {
                Log.e(TAG, "onFailure: " + t.toString());
                listener.onFailure(Constant.SERVER_ERROR);
            }
        });
    }
}
