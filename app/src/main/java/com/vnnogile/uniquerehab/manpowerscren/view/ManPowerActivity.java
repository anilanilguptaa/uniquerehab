package com.vnnogile.uniquerehab.manpowerscren.view;

import android.Manifest;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.location.Location;
import android.os.Bundle;
import android.provider.MediaStore;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;
import com.vnnogile.uniquerehab.R;
import com.vnnogile.uniquerehab.global.Constant;
import com.vnnogile.uniquerehab.global.CustomCalendar;
import com.vnnogile.uniquerehab.global.LocationModel;
import com.vnnogile.uniquerehab.global.MyApplication;
import com.vnnogile.uniquerehab.global.PreferenceManager;
import com.vnnogile.uniquerehab.global.ProgressBar;
import com.vnnogile.uniquerehab.global.UniqueLocationTrack;
import com.vnnogile.uniquerehab.global.UniqueUtils;
import com.vnnogile.uniquerehab.global.WarningDialog;
import com.vnnogile.uniquerehab.manpowerscren.adapter.ManPowerAdapter;
import com.vnnogile.uniquerehab.manpowerscren.controller.ManPowerController;
import com.vnnogile.uniquerehab.manpowerscren.model.ManPowerModel;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ManPowerActivity extends AppCompatActivity implements ManPowerController.ManPowerListener,
        ManPowerAdapter.ManpowerListener, UniqueLocationTrack.LocationManagerListener, TextWatcher {

    public static final String TAG = ManPowerActivity.class.getSimpleName();
    private static int REQUEST_CAMERA = 1;
    @BindView(R.id.toolbar_menu)
    ImageView toolbarMenu;
    @BindView(R.id.toolbar_name)
    TextView toolbarName;
    @BindView(R.id.toolbar_profile)
    ImageView toolbarProfile;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.image_date)
    ImageView imageDate;
    @BindView(R.id.txt_date)
    TextView txtDate;
    @BindView(R.id.txt_time)
    TextView txtTime;
    @BindView(R.id.edit_count)
    EditText editCount;
    @BindView(R.id.btn_upload_photo)
    Button btnUploadPhoto;
    @BindView(R.id.man_pawor_rv)
    RecyclerView manPaworRv;
    @BindView(R.id.emp_type_spinner)
    Spinner empTypeSpinner;

    @BindView(R.id.toolbar_project)
    TextView toolbarProject;

    //
    private ImageView image;


    private Calendar calendar = Calendar.getInstance();
    private Calendar selectedCalendar = Calendar.getInstance();
    private String date = "";
    private int contractorId = -1;

    private ManPowerController controller;
    private ProgressBar progressBar;
    private ManPowerModel manPowerModel;
    private ManPowerAdapter manPowerAdapter;
    private UniqueLocationTrack uniqueLocationTrack;
    private Bitmap bitmapList;
    private LocationModel locationModel = new LocationModel();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_man_pawor);
        ButterKnife.bind(this);

        loadVariables();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK) {
            if (requestCode == REQUEST_CAMERA) {
                Bitmap bitmap = (Bitmap) data.getExtras().get("data");
                bitmapList = bitmap;
                image.setImageBitmap(bitmap);
                image.setVisibility(View.VISIBLE);
            }
        }
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {

        if (item.getItemId() == android.R.id.home)
            onBackPressed();

        return super.onOptionsItemSelected(item);
    }

    private void loadVariables() {
        progressBar = new ProgressBar(this);
        controller = new ManPowerController(this);
        toolbar.setNavigationIcon(R.drawable.ic_back);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        editCount.addTextChangedListener(this);

        toolbarProfile.setImageResource(R.drawable.profile_picture);
        toolbarName.setText("Manpower");
        toolbarProject.setText(MyApplication.getInstance().getProjectName());
        txtDate.setText(UniqueUtils.getCurrentDate("dd MMM yyyy"));
        txtTime.setText(UniqueUtils.getCurrentDate("HH:mm"));

        manPaworRv.setLayoutManager(new GridLayoutManager(this, 2));

        date = UniqueUtils.getDateInFormat(calendar.getTime(), Constant.YYYY_MM_DD);

        getManPowerData();
    }

    private void setSpinnerData() {

        ArrayList<String> data = new ArrayList<>();
        data.add("Select Contractor");

        for (int i = 0; i < manPowerModel.contractorData.size(); i++) {
            data.add(UniqueUtils.removeChar(manPowerModel.contractorData.get(i).firstName) + " " +
                    manPowerModel.contractorData.get(i).lastName);
        }

        // Creating adapter for spinner
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, data);
        // Drop down layout style - list view with radio button
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        // attaching data adapter to spinner
        empTypeSpinner.setAdapter(dataAdapter);

        empTypeSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                TextView textView = (TextView) view;
                if (position != 0) {
                    contractorId = manPowerModel.contractorData.get(position - 1).id;
                    if (textView != null)
                        textView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 14);
                } else {
                    contractorId = -1;
                    if (textView != null) {
                        textView.setTextColor(getResources().getColor(R.color.colorDarkGrey));
                        textView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 14);
                    }
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

    }

    private void getManPowerData() {
        if (UniqueUtils.isNetworkAvailable(this)) {
            progressBar.show();
            controller.getManPowerData(date);
        } else {
            UniqueUtils.showErrorMessage(Constant.NETWORK_ERROR_MESSAGE);
        }
    }

    private void showSettingsDialog() {
        WarningDialog warningDialog = new WarningDialog(this, new WarningDialog.WarningDialogListener() {
            @Override
            public void onPositiveClick() {
                UniqueUtils.openSettings(ManPowerActivity.this);
            }

            @Override
            public void onNegativeClick() {

            }
        });
        warningDialog.setMessage("This app needs permission to use this feature. You can grant them in app settings.");
        warningDialog.setPositiveButtonName("Setting");
        warningDialog.setNegativeButtonName("Cancel");
        warningDialog.show();

    }

    private void tackPictureDialog() {

        Dialog dialog = new Dialog(this);
        View view = LayoutInflater.from(this).inflate(R.layout.dialog_tack_picture, null);

        dialog.setContentView(view);

        TextView txtBoqName = view.findViewById(R.id.txt_boq_name);
        Button button = view.findViewById(R.id.btn_submit);
        LinearLayout layout = view.findViewById(R.id.btn_click);
        RecyclerView image_rv = view.findViewById(R.id.image_rv);
        image = view.findViewById(R.id.image);
        image.setVisibility(View.GONE);

        if (dialog.getWindow() != null) {
            dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
            dialog.getWindow().setGravity(Gravity.CENTER);
        }

        txtBoqName.setVisibility(View.GONE);
        image_rv.setVisibility(View.GONE);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                if (bitmapList != null) {
                    progressBar.show();
                    controller.addManPowerData(contractorId, editCount.getText().toString().trim(),
                            bitmapList, locationModel,
                            UniqueUtils.getDateInFormat(selectedCalendar.getTime(), Constant.YYYY_MM_DD_HH_MM_SS));

                } else
                    UniqueUtils.showWarningMessage("No image found.");
            }
        });

        layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (UniqueUtils.isLocationEnabled(ManPowerActivity.this)) {
                    uniqueLocationTrack = new UniqueLocationTrack(ManPowerActivity.this);
                    uniqueLocationTrack.setListener(ManPowerActivity.this);
                    openCamera();
                } else
                    UniqueUtils.showWarningMessage(Constant.ENABLE_LOCATION);
            }
        });


        dialog.show();
    }

    private void openCamera() {
        Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(cameraIntent, REQUEST_CAMERA);
    }

    @OnClick(R.id.btn_upload_photo)
    public void onViewClicked() {

        if (empTypeSpinner.getSelectedItemPosition() == 0) {
            UniqueUtils.showWarningMessage("Select Contractor");
        } else if (TextUtils.isEmpty(editCount.getText().toString().trim())) {
            UniqueUtils.showWarningMessage("Enter count.");

        } else if (Integer.parseInt(editCount.getText().toString().trim()) < 1 ||
                Integer.parseInt(editCount.getText().toString().trim()) > 20) {
            UniqueUtils.showWarningMessage("Please enter count between 1 to 20");
        } else {
            Dexter.withActivity(this)
                    .withPermissions(Manifest.permission.CAMERA,
                            Manifest.permission.READ_EXTERNAL_STORAGE,
                            Manifest.permission.ACCESS_COARSE_LOCATION,
                            Manifest.permission.ACCESS_FINE_LOCATION)
                    .withListener(new MultiplePermissionsListener() {
                        @Override
                        public void onPermissionsChecked(MultiplePermissionsReport report) {
                            if (report.areAllPermissionsGranted()) {
                                tackPictureDialog();
                            } else {
                                showSettingsDialog();
                            }
                        }

                        @Override
                        public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {
                            token.continuePermissionRequest();
                        }
                    })
                    .onSameThread()
                    .check();

        }

    }

    @OnClick({R.id.image_date, R.id.date_lyr})
    public void onDateClicked() {
        if (PreferenceManager.getInstance().getUserRole().equalsIgnoreCase(UniqueUtils.ADMIN)) {
            CustomCalendar customCalendar = new CustomCalendar(this, "manpower");
            customCalendar.setListener(new CustomCalendar.CustomCalendarListener() {
                @Override
                public void onDateSet(Date selectedDate) {
                    txtDate.setText(UniqueUtils.getDateInFormat(selectedDate, "dd MMM yyyy"));
                    date = UniqueUtils.getDateInFormat(selectedDate, Constant.YYYY_MM_DD);
                    selectedCalendar.setTime(selectedDate);
                    getManPowerData();
                }
            });
            customCalendar.show();
        }
    }

    @Override
    public void onSuccess(ManPowerModel model) {
        progressBar.dismiss();
        manPowerModel = model;
        setSpinnerData();

        manPowerAdapter = new ManPowerAdapter(manPowerModel.manpowerData, this, this);
        manPaworRv.setAdapter(manPowerAdapter);
    }

    @Override
    public void onSuccessAdd() {
        progressBar.dismiss();
        empTypeSpinner.setSelection(0);
        editCount.setText("");
        bitmapList = null;
        locationModel = null;
        contractorId = -1;
        // UniqueUtils.showSuccessMessage("Successfully added.");
        getManPowerData();
    }

    @Override
    public void onSuccessDelete() {
        progressBar.dismiss();
        empTypeSpinner.setSelection(0);
        editCount.setText("");
        bitmapList = null;
        locationModel = null;
        contractorId = -1;
        UniqueUtils.showSuccessMessage("Successfully Deleted.");
        getManPowerData();
    }

    @Override
    public void onFailure(String errorMessage) {
        progressBar.dismiss();
    }


    @Override
    public void onDeleteManpower(ManPowerModel.ManpowerDatum datum) {
        if (UniqueUtils.isNetworkAvailable(this)) {
            if (PreferenceManager.getInstance().getUserRole().equalsIgnoreCase(UniqueUtils.ADMIN) ||
                    PreferenceManager.getInstance().getUserRole().equalsIgnoreCase(UniqueUtils.Site_Engineer)) {
                progressBar.show();
                controller.deleteManpower(String.valueOf(datum.manpowerId));
            } else {
                UniqueUtils.showWarningMessage("Not allowed to delete");
            }
        } else {
            UniqueUtils.showErrorMessage(Constant.NETWORK_ERROR_MESSAGE);
        }
    }

    @Override
    public void onLocationResult(Location location) {
        if (uniqueLocationTrack != null) {
            Log.e(TAG, "onLocationResult: " + location.getLongitude());
            Log.e(TAG, "onLocationResult: " + location.getLatitude());
            locationModel = new LocationModel();
            locationModel.latitude = location.getLatitude();
            locationModel.longitude = location.getLongitude();
            locationModel.address = uniqueLocationTrack.getAddress(location.getLatitude(), location.getLongitude());
        }

    }

    @OnClick(R.id.toolbar_profile)
    public void onClicked() {
        UniqueUtils.goToHomePage(this);
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {

    }

    @Override
    public void afterTextChanged(Editable s) {
        if (s.length() == 1) {
            if (s.charAt(0) == '0') {
                editCount.setText("1");
                editCount.setSelection(1);
            }
        }
    }
}
