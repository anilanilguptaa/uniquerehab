package com.vnnogile.uniquerehab.manpowerscren.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class ManPowerModel implements Parcelable{

    @SerializedName("contractor_data")
    @Expose
    public List<ContractorDatum> contractorData = new ArrayList<>();
    @SerializedName("manpower_data")
    @Expose
    public List<ManpowerDatum> manpowerData = new ArrayList<>();

    protected ManPowerModel(Parcel in) {
        contractorData = in.createTypedArrayList(ContractorDatum.CREATOR);
        manpowerData = in.createTypedArrayList(ManpowerDatum.CREATOR);
    }

    public static final Creator<ManPowerModel> CREATOR = new Creator<ManPowerModel>() {
        @Override
        public ManPowerModel createFromParcel(Parcel in) {
            return new ManPowerModel(in);
        }

        @Override
        public ManPowerModel[] newArray(int size) {
            return new ManPowerModel[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeTypedList(contractorData);
        dest.writeTypedList(manpowerData);
    }


    public static class ContractorDatum implements Parcelable {
        @SerializedName("id")
        @Expose
        public int id;
        @SerializedName("first_name")
        @Expose
        public String firstName;
        @SerializedName("last_name")
        @Expose
        public String lastName;
        @SerializedName("age")
        @Expose
        public int age;
        @SerializedName("gender")
        @Expose
        public String gender;
        @SerializedName("email")
        @Expose
        public String email;
        @SerializedName("phone")
        @Expose
        public String phone;
        @SerializedName("address")
        @Expose
        public String address;
        @SerializedName("is_active")
        @Expose
        public String isActive;
        @SerializedName("contractor_number")
        @Expose
        public String contractorNumber;


        protected ContractorDatum(Parcel in) {
            id = in.readInt();
            firstName = in.readString();
            lastName = in.readString();
            age = in.readInt();
            gender = in.readString();
            email = in.readString();
            phone = in.readString();
            address = in.readString();
            isActive = in.readString();
            contractorNumber = in.readString();
        }

        public static final Creator<ContractorDatum> CREATOR = new Creator<ContractorDatum>() {
            @Override
            public ContractorDatum createFromParcel(Parcel in) {
                return new ContractorDatum(in);
            }

            @Override
            public ContractorDatum[] newArray(int size) {
                return new ContractorDatum[size];
            }
        };

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeInt(id);
            dest.writeString(firstName);
            dest.writeString(lastName);
            dest.writeInt(age);
            dest.writeString(gender);
            dest.writeString(email);
            dest.writeString(phone);
            dest.writeString(address);
            dest.writeString(isActive);
            dest.writeString(contractorNumber);
        }
    }

    public static class ManpowerDatum implements Parcelable{
        @SerializedName("manpower_id ")
        @Expose
        public int manpowerId;
        @SerializedName("first_name")
        @Expose
        public String firstName;
        @SerializedName("last_name")
        @Expose
        public String lastName;
        @SerializedName("no_of_workers")
        @Expose
        public int noOfWorkers;
        @SerializedName("image_path")
        @Expose
        public String imagePath;

        @SerializedName("location")
        @Expose
        public String location;


        protected ManpowerDatum(Parcel in) {
            manpowerId = in.readInt();
            firstName = in.readString();
            lastName = in.readString();
            noOfWorkers = in.readInt();
            imagePath = in.readString();
            location = in.readString();
        }

        public static final Creator<ManpowerDatum> CREATOR = new Creator<ManpowerDatum>() {
            @Override
            public ManpowerDatum createFromParcel(Parcel in) {
                return new ManpowerDatum(in);
            }

            @Override
            public ManpowerDatum[] newArray(int size) {
                return new ManpowerDatum[size];
            }
        };

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeInt(manpowerId);
            dest.writeString(firstName);
            dest.writeString(lastName);
            dest.writeInt(noOfWorkers);
            dest.writeString(imagePath);
            dest.writeString(location);
        }
    }
}
