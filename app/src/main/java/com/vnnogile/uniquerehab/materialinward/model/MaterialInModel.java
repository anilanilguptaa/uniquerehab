package com.vnnogile.uniquerehab.materialinward.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class MaterialInModel implements Parcelable {


    @SerializedName("id")
    @Expose
    public int id;
    @SerializedName("name")
    @Expose
    public String name;
    @SerializedName("description")
    @Expose
    public String description;
    @SerializedName("project_id")
    @Expose
    public int projectId;
    @SerializedName("created_date")
    @Expose
    public String createdDate;
    @SerializedName("geolocation")
    @Expose
    public String geolocation;
    @SerializedName("status")
    @Expose
    public String status;
    @SerializedName("chalan_number")
    @Expose
    public String chalanNumber;
    @SerializedName("is_active")
    @Expose
    public int isActive;
    @SerializedName("grn_number")
    @Expose
    public String grnNumber;
    @SerializedName("site_id")
    @Expose
    public int siteId;

    @SerializedName("material_list_data")
    @Expose
    public List<MaterialDatum> materialData = null;

    @SerializedName("material_request_data")
    @Expose
    public List<MaterialRequestDatum> materialRequestData = null;

    public MaterialInModel() {
    }




    protected MaterialInModel(Parcel in) {
        id = in.readInt();
        name = in.readString();
        description = in.readString();
        projectId = in.readInt();
        createdDate = in.readString();
        geolocation = in.readString();
        status = in.readString();
        chalanNumber = in.readString();
        isActive = in.readInt();
        grnNumber = in.readString();
        siteId = in.readInt();
        materialData = in.createTypedArrayList(MaterialDatum.CREATOR);
        materialRequestData = in.createTypedArrayList(MaterialRequestDatum.CREATOR);
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeString(name);
        dest.writeString(description);
        dest.writeInt(projectId);
        dest.writeString(createdDate);
        dest.writeString(geolocation);
        dest.writeString(status);
        dest.writeString(chalanNumber);
        dest.writeInt(isActive);
        dest.writeString(grnNumber);
        dest.writeInt(siteId);
        dest.writeTypedList(materialData);
        dest.writeTypedList(materialRequestData);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<MaterialInModel> CREATOR = new Creator<MaterialInModel>() {
        @Override
        public MaterialInModel createFromParcel(Parcel in) {
            return new MaterialInModel(in);
        }

        @Override
        public MaterialInModel[] newArray(int size) {
            return new MaterialInModel[size];
        }
    };

    public static class MaterialDatum implements Parcelable {

        //{
        //  "id": 4,
        //  "name": "concrete",
        //  "description": "",
        //  "code": "mat4",
        //  "created_at": "2020-05-12 14:15:25",
        //  "unit": "KG",
        //  "material_id": "4"
        //}

        @SerializedName("id")
        @Expose
        public int id;
        @SerializedName("name")
        @Expose
        public String name="";
        @SerializedName("description")
        @Expose
        public String description="";
        @SerializedName("code")
        @Expose
        public String code;
        @SerializedName("unit_id")
        @Expose
        public int unitId;
        @SerializedName("unit")
        @Expose
        public String unit;
        @SerializedName("material_id")
        @Expose
        public String material_id;

        protected MaterialDatum(Parcel in) {
            id = in.readInt();
            name = in.readString();
            description = in.readString();
            code = in.readString();
            unitId = in.readInt();
            unit = in.readString();
            material_id = in.readString();
        }

        public static final Creator<MaterialDatum> CREATOR = new Creator<MaterialDatum>() {
            @Override
            public MaterialDatum createFromParcel(Parcel in) {
                return new MaterialDatum(in);
            }

            @Override
            public MaterialDatum[] newArray(int size) {
                return new MaterialDatum[size];
            }
        };

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeInt(id);
            dest.writeString(name);
            dest.writeString(description);
            dest.writeString(code);
            dest.writeInt(unitId);
            dest.writeString(unit);
            dest.writeString(material_id);
        }
    }

    public static class MaterialRequestDatum implements Parcelable{
        @SerializedName("header_id")
        @Expose
        public int headerId;
        @SerializedName("mode")
        @Expose
        public String mode;
        @SerializedName("status")
        @Expose
        public String status;
        @SerializedName("detail_id")
        @Expose
        public int detailId;
        @SerializedName("quantity")
        @Expose
        public String quantity;
        @SerializedName("material_name")
        @Expose
        public String materialName;
        @SerializedName("unit")
        @Expose
        public String unit;
        @SerializedName("price")
        @Expose
        public String price="";
        @SerializedName("material_id")
        @Expose
        public String materialId;

        public String siteName="";
        public String siteId="";

        public boolean isChecked=false;

        public MaterialRequestDatum() {
        }

        protected MaterialRequestDatum(Parcel in) {
            headerId = in.readInt();
            mode = in.readString();
            status = in.readString();
            detailId = in.readInt();
            quantity = in.readString();
            materialName = in.readString();
            unit = in.readString();
            price = in.readString();
            materialId = in.readString();
            siteName = in.readString();
            siteId = in.readString();
            isChecked = in.readByte() != 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeInt(headerId);
            dest.writeString(mode);
            dest.writeString(status);
            dest.writeInt(detailId);
            dest.writeString(quantity);
            dest.writeString(materialName);
            dest.writeString(unit);
            dest.writeString(price);
            dest.writeString(materialId);
            dest.writeString(siteName);
            dest.writeString(siteId);
            dest.writeByte((byte) (isChecked ? 1 : 0));
        }

        @Override
        public int describeContents() {
            return 0;
        }

        public static final Creator<MaterialRequestDatum> CREATOR = new Creator<MaterialRequestDatum>() {
            @Override
            public MaterialRequestDatum createFromParcel(Parcel in) {
                return new MaterialRequestDatum(in);
            }

            @Override
            public MaterialRequestDatum[] newArray(int size) {
                return new MaterialRequestDatum[size];
            }
        };
    }
}
