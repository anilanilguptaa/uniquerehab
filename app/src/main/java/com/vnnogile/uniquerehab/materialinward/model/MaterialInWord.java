package com.vnnogile.uniquerehab.materialinward.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class MaterialInWord implements Parcelable{


    @SerializedName("detailsData")
    @Expose
    public List<DetailsDatum> detailsData = new ArrayList<>();
    @SerializedName("imageData")
    @Expose
    public List<ImageDatum> imageData = new ArrayList<>();


    protected MaterialInWord(Parcel in) {
        detailsData = in.createTypedArrayList(DetailsDatum.CREATOR);
        imageData = in.createTypedArrayList(ImageDatum.CREATOR);
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeTypedList(detailsData);
        dest.writeTypedList(imageData);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<MaterialInWord> CREATOR = new Creator<MaterialInWord>() {
        @Override
        public MaterialInWord createFromParcel(Parcel in) {
            return new MaterialInWord(in);
        }

        @Override
        public MaterialInWord[] newArray(int size) {
            return new MaterialInWord[size];
        }
    };


    public static class DetailsDatum implements Parcelable{
        @SerializedName("id")
        @Expose
        public int id;
        @SerializedName("material_inwards_header_id")
        @Expose
        public int materialInwardsHeaderId;
        @SerializedName("material_id")
        @Expose
        public int materialId;
        @SerializedName("quantity")
        @Expose
        public String quantity;
        @SerializedName("material_name")
        @Expose
        public String materialName="";
        @SerializedName("unit")
        @Expose
        public String unit="";
        @SerializedName("consumed")
        @Expose
        public String consumed;
        @SerializedName("created_date")
        @Expose
        public String createdDate;
        @SerializedName("is_active")
        @Expose
        public int isActive;
        @SerializedName("status")
        @Expose
        public String status;
        @SerializedName("price")
        @Expose
        public String price="";
        @SerializedName("site_id")
        @Expose
        public String siteId;
        @SerializedName("gst_amount")
        @Expose
        public String gst_amount="";

        public DetailsDatum() {
        }

        protected DetailsDatum(Parcel in) {
            id = in.readInt();
            materialInwardsHeaderId = in.readInt();
            materialId = in.readInt();
            quantity = in.readString();
            materialName = in.readString();
            unit = in.readString();
            consumed = in.readString();
            createdDate = in.readString();
            isActive = in.readInt();
            status = in.readString();
            price = in.readString();
            siteId = in.readString();
            gst_amount = in.readString();
        }

        public static final Creator<DetailsDatum> CREATOR = new Creator<DetailsDatum>() {
            @Override
            public DetailsDatum createFromParcel(Parcel in) {
                return new DetailsDatum(in);
            }

            @Override
            public DetailsDatum[] newArray(int size) {
                return new DetailsDatum[size];
            }
        };

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeInt(id);
            dest.writeInt(materialInwardsHeaderId);
            dest.writeInt(materialId);
            dest.writeString(quantity);
            dest.writeString(materialName);
            dest.writeString(unit);
            dest.writeString(consumed);
            dest.writeString(createdDate);
            dest.writeInt(isActive);
            dest.writeString(status);
            dest.writeString(price);
                dest.writeString(siteId);
            dest.writeString(gst_amount);
        }
    }


    public static class ImageDatum implements Parcelable {
        @SerializedName("id")
        @Expose
        public int id = 0;
        @SerializedName("document_id")
        @Expose
        public int document_id;
        @SerializedName("name")
        @Expose
        public String name;
        @SerializedName("description")
        @Expose
        public String description;
        @SerializedName("path")
        @Expose
        public String path;
        @SerializedName("document_type")
        @Expose
        public String documentType;
        @SerializedName("material_inwards_header_id")
        @Expose
        public int materialInwardsHeaderId;
        @SerializedName("created_date")
        @Expose
        public String createdDate;
        @SerializedName("is_active")
        @Expose
        public int isActive;
        @SerializedName("latitude")
        @Expose
        public String latitude;
        @SerializedName("longitude")
        @Expose
        public String longitude;
        @SerializedName("location")
        @Expose
        public String location;
        @SerializedName("image_path")
        @Expose
        public String image_path;

        public ImageDatum() {

        }

        protected ImageDatum(Parcel in) {
            id = in.readInt();
            document_id = in.readInt();
            name = in.readString();
            description = in.readString();
            path = in.readString();
            documentType = in.readString();
            materialInwardsHeaderId = in.readInt();
            createdDate = in.readString();
            isActive = in.readInt();
            latitude = in.readString();
            longitude = in.readString();
            location = in.readString();
            image_path = in.readString();
        }

        public static final Creator<ImageDatum> CREATOR = new Creator<ImageDatum>() {
            @Override
            public ImageDatum createFromParcel(Parcel in) {
                return new ImageDatum(in);
            }

            @Override
            public ImageDatum[] newArray(int size) {
                return new ImageDatum[size];
            }
        };

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeInt(id);
            dest.writeInt(document_id);
            dest.writeString(name);
            dest.writeString(description);
            dest.writeString(path);
            dest.writeString(documentType);
            dest.writeInt(materialInwardsHeaderId);
            dest.writeString(createdDate);
            dest.writeInt(isActive);
            dest.writeString(latitude);
            dest.writeString(longitude);
            dest.writeString(location);
            dest.writeString(image_path);
        }
    }
}
