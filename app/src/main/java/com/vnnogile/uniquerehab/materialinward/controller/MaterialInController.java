package com.vnnogile.uniquerehab.materialinward.controller;

import android.content.Context;
import android.graphics.Bitmap;
import android.util.Log;

import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import com.vnnogile.uniquerehab.global.Constant;
import com.vnnogile.uniquerehab.global.LocationModel;
import com.vnnogile.uniquerehab.global.MyApplication;
import com.vnnogile.uniquerehab.global.PreferenceManager;
import com.vnnogile.uniquerehab.global.ResponseMessage;
import com.vnnogile.uniquerehab.global.UniqueUtils;
import com.vnnogile.uniquerehab.materialinward.model.MaterialInModel;
import com.vnnogile.uniquerehab.materialinward.model.MaterialInWord;
import com.vnnogile.uniquerehab.retrofit.ApiClient;
import com.vnnogile.uniquerehab.retrofit.ApiInterface;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MaterialInController {

    public static final String TAG = MaterialInController.class.getSimpleName();

    private Context context;
    private MaterialInListener listener;
    private ApiInterface apiInterface;

    public MaterialInController(Context context) {
        this.context = context;
        // listener =(MaterialInListener) context;
        apiInterface = ApiClient.getClient().create(ApiInterface.class);
    }

    public void setListener(MaterialInListener listener) {
        this.listener = listener;
    }

    public interface MaterialInListener {
        void onSuccessMaterialIn(MaterialInModel model, List<MaterialInModel> inModels);

        void onSuccessMaterialData(MaterialInWord material);

        void onSuccessAdd();

        void onSuccessDelete(String mg);

        void onFailed(String errorMessage);
    }

    public void getMaterialIn() {
        Call<MaterialInModel> objectCall = apiInterface.getMaterialIn(
                PreferenceManager.getInstance().getToken(),
                String.valueOf(PreferenceManager.getInstance().getUserDetails().userDetail.id),
                MyApplication.getInstance().getProjectId(),
                UniqueUtils.getCurrentDate("yyyy-MM-dd")
        );

        objectCall.enqueue(new Callback<MaterialInModel>() {

            @Override
            public void onResponse(Call<MaterialInModel> call, Response<MaterialInModel> response) {
                if (response.isSuccessful()) {
                    listener.onSuccessMaterialIn(response.body(), null);
                } else {
                    listener.onFailed(Constant.SERVER_ERROR);
                }
            }

            @Override
            public void onFailure(Call<MaterialInModel> call, Throwable t) {
                Log.e(TAG, "onFailure: " + t.toString());
                listener.onFailed(Constant.SERVER_ERROR);
            }
        });
    }


    public void addMaterialData(List<MaterialInWord.DetailsDatum> inwardsData,
                                List<Bitmap> bitmapList, List<LocationModel> locationModels) {
        JSONObject jsonObject = new JSONObject();
        try {


            jsonObject.accumulate("site_id", "");
            jsonObject.accumulate("project_id", MyApplication.getInstance().getProjectId());
            jsonObject.accumulate("date", UniqueUtils.getCurrentDate(Constant.YYYY_MM_DD_HH_MM_SS));
            jsonObject.accumulate("user_id", PreferenceManager.getInstance().getUserId());

            JSONArray jsonArray = new JSONArray();
            JSONArray dataArray = new JSONArray();

            for (int i = 0; i < inwardsData.size(); i++) {

                JSONObject object = new JSONObject();
                object.accumulate("material_id", inwardsData.get(i).materialId);
                object.accumulate("quantity", inwardsData.get(i).quantity);
                object.accumulate("unit", inwardsData.get(i).unit);
                dataArray.put(object);
            }

            for (int i = 0; i < bitmapList.size(); i++) {
                JSONObject object = new JSONObject();
                object.accumulate("data", UniqueUtils.bash64(bitmapList.get(i)));
                if (locationModels.size() != 0) {
                    object.accumulate("latitude", locationModels.get(i).latitude);
                    object.accumulate("longitude", locationModels.get(i).longitude);
                    object.accumulate("location", locationModels.get(i).address);
                } else {
                    object.accumulate("latitude", 0.0);
                    object.accumulate("longitude", 0.0);
                    object.accumulate("location", "");
                }
                jsonArray.put(object);
            }
            jsonObject.accumulate("material_inwards_details_data", dataArray);
            jsonObject.accumulate("images", jsonArray);

        } catch (Exception e) {
            e.printStackTrace();
        }

        JsonElement jsonObject1 = JsonParser.parseString(jsonObject.toString());

        Call<ResponseMessage> objectCall = apiInterface.addMaterial(PreferenceManager.getInstance().getToken(), jsonObject1);

        objectCall.enqueue(new Callback<ResponseMessage>() {

            @Override
            public void onResponse(Call<ResponseMessage> call, Response<ResponseMessage> response) {

                if (response.isSuccessful()) {
                    if (response.body().status)
                        UniqueUtils.showSuccessMessage(response.body().message);
                    else
                        UniqueUtils.showErrorMessage(response.body().message);
                    listener.onSuccessAdd();
                } else {
                    listener.onFailed(Constant.SERVER_ERROR);
                }

            }

            @Override
            public void onFailure(Call<ResponseMessage> call, Throwable t) {
                Log.e(TAG, "onFailure: " + t.toString());
                listener.onFailed(Constant.SERVER_ERROR);
            }
        });
    }

    public void getListOfMaterialInwards(String date) {

        Call<List<MaterialInModel>> objectCall = apiInterface.getMaterialInWardsTime(PreferenceManager.getInstance().getToken(),
                PreferenceManager.getInstance().getUserDetails().userDetail.id + "",
                MyApplication.getInstance().getProjectId(),
                date);

        objectCall.enqueue(new Callback<List<MaterialInModel>>() {
            @Override
            public void onResponse(Call<List<MaterialInModel>> call, Response<List<MaterialInModel>> response) {
                if (response.isSuccessful()) {
                    listener.onSuccessMaterialIn(null, response.body());
                } else {
                    listener.onFailed(Constant.SERVER_ERROR);
                }
            }

            @Override
            public void onFailure(Call<List<MaterialInModel>> call, Throwable t) {
                Log.e(TAG, "onFailure: " + t.toString());
                listener.onFailed(Constant.SERVER_ERROR);
            }
        });

    }


    public void getData(String id, String projectId) {

        Call<MaterialInWord> objectCall = apiInterface.getMaterialInWardsList(PreferenceManager.getInstance().getToken(),
                PreferenceManager.getInstance().getUserId(),
                projectId, id);

        objectCall.enqueue(new Callback<MaterialInWord>() {
            @Override
            public void onResponse(Call<MaterialInWord> call, Response<MaterialInWord> response) {
                if (response.isSuccessful()) {
                    listener.onSuccessMaterialData(response.body());
                } else {
                    listener.onFailed(Constant.SERVER_ERROR);
                }
            }

            @Override
            public void onFailure(Call<MaterialInWord> call, Throwable t) {
                Log.e(TAG, "onFailure: " + t.toString());
                listener.onFailed(t.toString());
            }
        });

    }

    public void updateMaterial(List<MaterialInWord.DetailsDatum> datum,
                               List<Bitmap> bitmaps, List<LocationModel> locationModels) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.accumulate("user_role", "Admin");
            jsonObject.accumulate("project_id", MyApplication.getInstance().getProjectId());
            jsonObject.accumulate("date", UniqueUtils.getCurrentDate(Constant.YYYY_MM_DD_HH_MM_SS));
            jsonObject.accumulate("user_id", PreferenceManager.getInstance().getUserId());

            JSONArray jsonArray = new JSONArray();
            JSONArray dataArray = new JSONArray();

            for (int i = 0; i < datum.size(); i++) {

                JSONObject object = new JSONObject();
                object.accumulate("material_inwards_header_id", datum.get(i).materialInwardsHeaderId);
                object.accumulate("material_id", datum.get(i).materialId);
                object.accumulate("site_id", datum.get(i).siteId);
                object.accumulate("id", datum.get(i).id);
                object.accumulate("quantity", datum.get(i).quantity);
                object.accumulate("price", datum.get(i).price);
                object.accumulate("unit", datum.get(i).unit);
                object.accumulate("gst_amount", datum.get(i).gst_amount);
                dataArray.put(object);
            }

            for (int i = 0; i < bitmaps.size(); i++) {
                JSONObject object = new JSONObject();
                object.accumulate("data", UniqueUtils.bash64(bitmaps.get(i)));
                if (locationModels.size() != 0) {
                    object.accumulate("latitude", locationModels.get(i).latitude);
                    object.accumulate("longitude", locationModels.get(i).longitude);
                    object.accumulate("location", locationModels.get(i).address);
                } else {
                    object.accumulate("latitude", 0.0);
                    object.accumulate("longitude", 0.0);
                    object.accumulate("location", "");
                }
                jsonArray.put(object);
            }
            jsonObject.accumulate("material_inwards_detail_data", dataArray);
            jsonObject.accumulate("images", jsonArray);


        } catch (Exception e) {
            e.printStackTrace();
        }

        JsonElement jsonObject1 = JsonParser.parseString(jsonObject.toString());

        Call<ResponseMessage> objectCall = apiInterface.addMaterial(PreferenceManager.getInstance().getToken(), jsonObject1);

        objectCall.enqueue(new Callback<ResponseMessage>() {

            @Override
            public void onResponse(Call<ResponseMessage> call, Response<ResponseMessage> response) {

                if (response.isSuccessful()) {
                    UniqueUtils.showSuccessMessage(response.body().message);
                    listener.onSuccessAdd();
                } else {
                    listener.onFailed(Constant.SERVER_ERROR);
                }

            }

            @Override
            public void onFailure(Call<ResponseMessage> call, Throwable t) {
                Log.e(TAG, "onFailure: " + t.toString());
                listener.onFailed(Constant.SERVER_ERROR);
            }
        });
    }


    public void deleteImage(int id) {
        JSONObject jsonObject = new JSONObject();
        try {

            jsonObject.accumulate("user_id", PreferenceManager.getInstance().getUserId());
            jsonObject.accumulate("project_id", MyApplication.getInstance().getProjectId());
            jsonObject.accumulate("material_inwards_document_id", id);

        } catch (Exception e) {
            e.printStackTrace();
        }
        JsonElement jsonObject1 = JsonParser.parseString(jsonObject.toString());

        Call<ResponseMessage> messageCall = apiInterface.deleteMaterialphoto(
                PreferenceManager.getInstance().getToken(), jsonObject1
        );

        messageCall.enqueue(new Callback<ResponseMessage>() {
            @Override
            public void onResponse(Call<ResponseMessage> call, Response<ResponseMessage> response) {
                if (response.isSuccessful()) {
                    listener.onSuccessDelete(response.body().message);
                } else {
                    listener.onFailed(Constant.SERVER_ERROR);
                }
            }

            @Override
            public void onFailure(Call<ResponseMessage> call, Throwable t) {
                Log.e(TAG, "onFailure: " + t.toString());
                listener.onFailed(Constant.SERVER_ERROR);
            }
        });
    }

    public void deletedeletematerialinwards(int id) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.accumulate("user_id", PreferenceManager.getInstance().getUserId());
            jsonObject.accumulate("project_id", MyApplication.getInstance().getProjectId());
            jsonObject.accumulate("material_inwards_details_id", id);

        } catch (Exception e) {
            e.printStackTrace();
        }
        JsonElement jsonObject1 = JsonParser.parseString(jsonObject.toString());

        Call<ResponseMessage> messageCall = apiInterface.deletematerialinwards(
                PreferenceManager.getInstance().getToken(), jsonObject1
        );

        messageCall.enqueue(new Callback<ResponseMessage>() {
            @Override
            public void onResponse(Call<ResponseMessage> call, Response<ResponseMessage> response) {
                if (response.isSuccessful()) {
                    listener.onSuccessDelete(response.body().message);
                } else {
                    listener.onFailed(Constant.SERVER_ERROR);
                }
            }

            @Override
            public void onFailure(Call<ResponseMessage> call, Throwable t) {
                Log.e(TAG, "onFailure: " + t.toString());
                listener.onFailed(Constant.SERVER_ERROR);
            }
        });
    }
}
