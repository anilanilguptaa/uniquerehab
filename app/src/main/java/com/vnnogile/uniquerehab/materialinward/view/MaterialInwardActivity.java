package com.vnnogile.uniquerehab.materialinward.view;

import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.vnnogile.uniquerehab.R;
import com.vnnogile.uniquerehab.global.MyApplication;
import com.vnnogile.uniquerehab.global.PreferenceManager;
import com.vnnogile.uniquerehab.global.UniqueUtils;
import com.vnnogile.uniquerehab.loginscreen.model.LoginResponse;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MaterialInwardActivity extends AppCompatActivity {

    public static final String TAG = MaterialInwardActivity.class.getSimpleName();
    @BindView(R.id.toolbar_menu)
    ImageView toolbarMenu;
    @BindView(R.id.toolbar_title)
    TextView toolbarName;
    @BindView(R.id.toolbar_profile)
    ImageView toolbarProfile;
    @BindView(R.id.toolbar_project)
    TextView toolbarProject;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_material_inward);
        ButterKnife.bind(this);

        loadVariables();
    }


    private void loadVariables() {
        toolbarProject.setText(MyApplication.getInstance().getProjectName());
        toolbarProfile.setImageResource(R.drawable.profile_picture);
        toolbarName.setText("Material Inward");
        LoginResponse loginResponse = PreferenceManager.getInstance().getUserDetails();

        Fragment fragment = null;
        if (loginResponse.roleName.equalsIgnoreCase(UniqueUtils.ADMIN)) {
            fragment = AdminFragment.newInstance();
        } else {
            fragment = SiteEngineerFragment.newInstance();
        }

        FragmentManager fragmentManager = getSupportFragmentManager();

        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

        fragmentTransaction.add(R.id.frame_container, fragment);

        fragmentTransaction.commit();

    }

    @OnClick(R.id.toolbar_profile)
    public void onViewClicked() {
        UniqueUtils.goToHomePage(this);
    }

    @OnClick(R.id.toolbar_menu)
    public void onMenuClicked() {
        onBackPressed();
    }
}
