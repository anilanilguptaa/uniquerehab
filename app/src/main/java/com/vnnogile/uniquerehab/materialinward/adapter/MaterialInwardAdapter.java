package com.vnnogile.uniquerehab.materialinward.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.vnnogile.uniquerehab.R;
import com.vnnogile.uniquerehab.global.UniqueUtils;
import com.vnnogile.uniquerehab.materialinward.model.MaterialInWord;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MaterialInwardAdapter extends RecyclerView.Adapter<MaterialInwardAdapter.MaterialViewHolder> {


    private List<MaterialInWord.DetailsDatum> inwardsData;
    private List<MaterialInWord.DetailsDatum> selectedList = new ArrayList<>();
    private Context context;
    private int indexPosition = -1;
    private MaterialListener listener;
    private int resId = -1;

    public MaterialInwardAdapter(List<MaterialInWord.DetailsDatum> inwardsData, Context context) {
        this.inwardsData = inwardsData;
        this.context = context;
    }

    public MaterialInwardAdapter(List<MaterialInWord.DetailsDatum> inwardsData, Context context, int resID) {
        this.inwardsData = inwardsData;
        this.context = context;
        resId = resID;
    }

    public interface MaterialListener {
        void onClickItem(MaterialInWord.DetailsDatum datum);

        void onItemDelete(MaterialInWord.DetailsDatum datum);
    }

    public void setListener(MaterialListener listener) {
        this.listener = listener;
    }

    public void reset() {
        indexPosition = -1;
        notifyDataSetChanged();
        selectedList.clear();
    }

    public void updateList(String qty, String price, String gst) {
        MaterialInWord.DetailsDatum datum = inwardsData.get(indexPosition);
        datum.quantity = qty;
        datum.price = price;
        datum.gst_amount = gst;
        notifyItemChanged(indexPosition);
        addToList();
    }

    private void addToList() {
        MaterialInWord.DetailsDatum datum = inwardsData.get(indexPosition);
        for (int i = 0; i < selectedList.size(); i++) {
            if (selectedList.get(i).materialId == datum.materialId) {
                selectedList.set(i, datum);
            } else {
                selectedList.add(datum);
            }
        }

        if (selectedList.size() == 0) {
            selectedList.add(datum);
        }

    }

    public List<MaterialInWord.DetailsDatum> getSelectedList() {
        return selectedList;
    }


    @NonNull
    @Override
    public MaterialViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = null;
        if (resId != -1) {
            view = LayoutInflater.from(parent.getContext()).inflate(resId, parent, false);
        } else {
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.cell_material_in_admin, parent, false);
        }
        return new MaterialViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MaterialViewHolder holder, int position) {
        MaterialInWord.DetailsDatum datum = inwardsData.get(position);

        holder.txtUnit.setText("Unit:" + UniqueUtils.removeChar(datum.unit));
        holder.txtMaterialName.setText("Material:" + UniqueUtils.removeChar(datum.materialName));
        holder.txtQty.setText("Qty:" + datum.quantity);
        holder.txtPrice.setText("Price:" + datum.price);
        holder.txt_gst_amount.setText("GST Amount:" + datum.gst_amount);

        holder.imageCheck.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                indexPosition = position;
                notifyDataSetChanged();
                if (listener != null) {
                    listener.onClickItem(datum);
                }
            }
        });
        holder.deleteContainer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (listener != null) {
                    listener.onItemDelete(datum);
                }
            }
        });

        if (indexPosition == position) {
            holder.imageCheck.setImageResource(R.drawable.ic_edit_enable);
        } else {
            holder.imageCheck.setImageResource(R.drawable.ic_edit);
        }

        if (datum.status.equalsIgnoreCase("pending")) {

            holder.deleteContainer.setVisibility(View.VISIBLE);
        } else {

            holder.deleteContainer.setVisibility(View.GONE);
        }


    }

    @Override
    public int getItemCount() {
        return inwardsData.size();
    }

    public List<MaterialInWord.DetailsDatum> getData() {
        return inwardsData;
    }

    public void removeItem(int position) {
        inwardsData.remove(position);
        notifyItemRemoved(position);
    }

    class MaterialViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.delete_container)
        RelativeLayout deleteContainer;
        @BindView(R.id.txt_material_name)
        TextView txtMaterialName;
        @BindView(R.id.txt_unit)
        TextView txtUnit;
        @BindView(R.id.txt_qty)
        TextView txtQty;
        @BindView(R.id.image_check)
        ImageView imageCheck;
        @BindView(R.id.txt_price)
        TextView txtPrice;
        @BindView(R.id.txt_gst_amount)
        TextView txt_gst_amount;

        MaterialViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            if (resId == -1)
                txt_gst_amount.setVisibility(View.VISIBLE);
        }
    }
}
