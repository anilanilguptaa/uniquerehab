package com.vnnogile.uniquerehab.materialinward.view;

import android.Manifest;
import android.app.DatePickerDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.location.Location;
import android.os.Bundle;
import android.provider.MediaStore;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;
import com.vnnogile.uniquerehab.R;
import com.vnnogile.uniquerehab.dailyprogressscreen.adapter.ImageAdapter;
import com.vnnogile.uniquerehab.global.Constant;
import com.vnnogile.uniquerehab.global.CustomCalendar;
import com.vnnogile.uniquerehab.global.DecimalValueFilter;
import com.vnnogile.uniquerehab.global.LocationModel;
import com.vnnogile.uniquerehab.global.ProgressBar;
import com.vnnogile.uniquerehab.global.UniqueLocationTrack;
import com.vnnogile.uniquerehab.global.UniqueUtils;
import com.vnnogile.uniquerehab.global.WarningDialog;
import com.vnnogile.uniquerehab.materialinward.adapter.ImageAdapterWithUrl;
import com.vnnogile.uniquerehab.materialinward.adapter.MaterialInwardAdapter;
import com.vnnogile.uniquerehab.materialinward.controller.MaterialInController;
import com.vnnogile.uniquerehab.materialinward.model.MaterialInModel;
import com.vnnogile.uniquerehab.materialinward.model.MaterialInWord;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static android.app.Activity.RESULT_OK;

public class AdminFragment extends Fragment implements MaterialInController.MaterialInListener,
        MaterialInwardAdapter.MaterialListener, ImageAdapterWithUrl.Listener,
        UniqueLocationTrack.LocationManagerListener,
        TextWatcher {

    public static final String TAG = AdminFragment.class.getSimpleName();
    @BindView(R.id.image_date)
    ImageView imageDate;
    @BindView(R.id.txt_date)
    TextView txtDate;
    @BindView(R.id.txt_time)
    TextView txtTime;
    @BindView(R.id.time_spinner)
    Spinner timeSpinner;
    @BindView(R.id.edit_count)
    EditText editCount;
    @BindView(R.id.material_rv)
    RecyclerView materialRv;
    @BindView(R.id.btn_submit)
    Button btnSubmit;
    @BindView(R.id.image_rv)
    RecyclerView imageRv;
    @BindView(R.id.edit_qty)
    EditText editQty;
    @BindView(R.id.btn_update)
    Button btnUpdate;
    @BindView(R.id.container)
    LinearLayout container;
    @BindView(R.id.container1)
    LinearLayout container1;
    @BindView(R.id.btn_click)
    ImageView btnClick;
    @BindView(R.id.image_click_rv)
    RecyclerView imageClickRv;
    @BindView(R.id.edit_gst)
    EditText editGst;

    //date
    private Calendar calendar = Calendar.getInstance();
    private Calendar selectedDate = Calendar.getInstance();
    private int mYear = calendar.get(Calendar.YEAR);
    private int mMonth = calendar.get(Calendar.MONTH);
    private int mDay = calendar.get(Calendar.DAY_OF_MONTH);


    private String date = "";
    private String siteId = "";

    private MaterialInController controller;
    private ProgressBar progressBar;
    private MaterialInwardAdapter materialInwardAdapter;
    private ImageAdapterWithUrl adapterWithUrl;
    private List<MaterialInModel> inModels = new ArrayList<>();
    private MaterialInWord materialInWords;
    private MaterialInModel materialInModel;

    public AdminFragment() {
        // Required empty public constructor
    }


    public static AdminFragment newInstance() {
        AdminFragment fragment = new AdminFragment();
        Bundle args = new Bundle();

        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.e(TAG, "onCreate: ");

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_admin, container, false);
        ButterKnife.bind(this, view);
        loadVariables();
        return view;
    }

    private void loadVariables() {
        progressBar = new ProgressBar(getContext());
        controller = new MaterialInController(getContext());
        controller.setListener(this);
        date = UniqueUtils.getCurrentDate("yyyy-MM-dd");
        txtDate.setText(UniqueUtils.getCurrentDate("dd MMM yyyy"));
        txtTime.setText(UniqueUtils.getCurrentDate("HH:mm"));

        editQty.setFilters(new InputFilter[]{new DecimalValueFilter(4)});
        editGst.setFilters(new InputFilter[]{new DecimalValueFilter(4)});
        editQty.addTextChangedListener(this);
        editCount.addTextChangedListener(this);
        editGst.addTextChangedListener(this);

        imageAdapter = new ImageAdapter(bitmapList, getContext(), R.layout.cell_material_image,locationModelList);
        imageAdapter.setListener(new ImageAdapter.ImageClickListener() {
            @Override
            public void onDelete(int position) {
                bitmapList.remove(position);
                locationModelList.remove(position);
                imageAdapter.notifyDataSetChanged();
            }
        });
        imageClickRv.setAdapter(imageAdapter);

        getList(date);

        setSpinnerData();
    }

    private void getList(String dateTime) {

        if (UniqueUtils.isNetworkAvailable(getContext())) {
            progressBar.show();
            controller.getListOfMaterialInwards(dateTime);
        } else {
            UniqueUtils.showErrorMessage(Constant.NETWORK_ERROR_MESSAGE);
        }
    }


    private void getMList(MaterialInModel model) {
        progressBar.show();
        controller.getData(model.id + "", model.projectId + "");
    }

    private void setSpinnerData() {

        ArrayList<String> data = new ArrayList<>();
        data.add("--Select--");

        for (int i = 0; i < inModels.size(); i++) {
            data.add(UniqueUtils.convertDate(inModels.get(i).createdDate, "hh:mm:ss", "hh:mm"));
        }

        // Creating adapter for spinner
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(getContext(), android.R.layout.simple_spinner_item, data);
        // Drop down layout style - list view with radio button
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        // attaching data adapter to spinner
        timeSpinner.setAdapter(dataAdapter);

        timeSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position != 0) {
                    // Toasty.success(getActivity(), data.get(position)).show();

                    materialInModel = inModels.get(position - 1);
                    getMList(inModels.get(position - 1));
                    siteId = String.valueOf(inModels.get(position - 1).siteId);
                } else {
                    siteId = "";
                    TextView textView = (TextView) view;
                    if (textView != null)
                        textView.setTextColor(getResources().getColor(R.color.colorDarkGrey));
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

    }


    @OnClick({R.id.image_date,R.id.date_lyr})
    public void onViewClicked() {
        CustomCalendar customCalendar = new CustomCalendar(getContext(),"material_inwards");
        customCalendar.setListener(new CustomCalendar.CustomCalendarListener() {
            @Override
            public void onDateSet(Date selectedDate) {
                txtDate.setText(UniqueUtils.getDateInFormat(selectedDate, "dd MMM yyyy"));
                date = UniqueUtils.getDateInFormat(selectedDate, Constant.YYYY_MM_DD);
                getList(date);
            }
        });
        customCalendar.show();
    }

    @Override
    public void onSuccessMaterialIn(MaterialInModel model, List<MaterialInModel> inModels) {
        //don't use model

        progressBar.dismiss();

        this.inModels.clear();
        this.inModels.addAll(inModels);
        setSpinnerData();

        if (inModels.size() > 0) {
            timeSpinner.setSelection(1);
        }else {
            materialInwardAdapter = new MaterialInwardAdapter(new ArrayList<>(), getContext());
            materialInwardAdapter.setListener(this);
            materialRv.setAdapter(materialInwardAdapter);

            adapterWithUrl = new ImageAdapterWithUrl(new ArrayList<>(), getContext(), this);
            imageRv.setAdapter(adapterWithUrl);
        }
    }

    @Override
    public void onSuccessMaterialData(MaterialInWord material) {
        progressBar.dismiss();

        materialInWords = (material);
        materialInwardAdapter = new MaterialInwardAdapter(materialInWords.detailsData, getContext());
        materialInwardAdapter.setListener(this);
        materialRv.setAdapter(materialInwardAdapter);

        adapterWithUrl = new ImageAdapterWithUrl(materialInWords.imageData, getContext(), this);
        imageRv.setAdapter(adapterWithUrl);
    }

    @Override
    public void onSuccessAdd() {
        progressBar.dismiss();
        detailsDatum = null;
       // UniqueUtils.showSuccessMessage("Successfully updated.");
        editCount.setText("");
        editQty.setText("");
        editGst.setText("");
        getMList(materialInModel);
        materialInwardAdapter.reset();
        bitmapList.clear();
        locationModelList.clear();
        imageAdapter.notifyDataSetChanged();
    }

    @Override
    public void onSuccessDelete(String mg) {
        progressBar.dismiss();
        UniqueUtils.showSuccessMessage(mg);
        progressBar.show();
        getMList(materialInModel);
    }

    @Override
    public void onFailed(String errorMessage) {
        progressBar.dismiss();
        UniqueUtils.showErrorMessage(errorMessage);

    }


    @OnClick(R.id.btn_submit)
    public void onSubmitClicked() {

        if (materialInwardAdapter != null && materialInwardAdapter.getSelectedList().size() == 0) {
            UniqueUtils.showWarningMessage("No updated item found");
        } else if (materialInwardAdapter != null && materialInwardAdapter.getSelectedList().size() != 0) {
            progressBar.show();
            controller.updateMaterial(materialInwardAdapter.getSelectedList(), bitmapList, locationModelList);
        }

    }

    private MaterialInWord.DetailsDatum detailsDatum = null;

    boolean isApproved= false;
    @Override
    public void onClickItem(MaterialInWord.DetailsDatum datum) {
        detailsDatum = datum;
        editQty.setText(datum.quantity+"");
        editQty.setSelection(editQty.getText().toString().trim().length());
        if (datum.status.equalsIgnoreCase("pending")){
            isApproved = false;
            editQty.setFocusable(true);
        }else {
            isApproved = true;
            editQty.setFocusable(false);
        }

    }

    @Override
    public void onItemDelete(MaterialInWord.DetailsDatum datum) {
        if (UniqueUtils.isNetworkAvailable(getContext())) {
            progressBar.show();
            controller.deletedeletematerialinwards(datum.id);
        } else {
            UniqueUtils.showErrorMessage(Constant.NETWORK_ERROR_MESSAGE);
        }
    }

    @Override
    public void onDeleteImage(int id) {
        progressBar.show();
        controller.deleteImage(id);
    }

    @OnClick(R.id.btn_update)
    public void onUpdateClicked() {
        if (detailsDatum == null) {
            UniqueUtils.showWarningMessage("Select material from list.");
        } else if (TextUtils.isEmpty(editCount.getText().toString().trim())) {
            UniqueUtils.showWarningMessage("Enter price");
        } else if (!isApproved&& TextUtils.isEmpty(editQty.getText().toString().trim())) {
            UniqueUtils.showWarningMessage("Enter Quantity");
        } else if (!isApproved && (Double.parseDouble(editQty.getText().toString().trim())<0.0001||
                Double.parseDouble(editQty.getText().toString().trim())>1000)){
            UniqueUtils.showWarningMessage("Please enter quantity between 0.0001 to 1000 ");
        }else if (TextUtils.isEmpty(editGst.getText().toString().trim())) {
            UniqueUtils.showWarningMessage("Enter GST amount");
        }
        else {
            materialInwardAdapter.updateList(editQty.getText().toString().trim(),
                    editCount.getText().toString().trim(),editGst.getText().toString().trim());
            clear();
        }

    }

    private void clear() {
        editCount.setText("");
        editQty.setText("");
        editGst.setText("");
    }

    private void showSettingsDialog() {
        WarningDialog warningDialog = new WarningDialog(getContext(), new WarningDialog.WarningDialogListener() {
            @Override
            public void onPositiveClick() {
                UniqueUtils.openSettings(getActivity());
            }

            @Override
            public void onNegativeClick() {

            }
        });
        warningDialog.setMessage("This app needs permission to use this feature. You can grant them in app settings.");
        warningDialog.setPositiveButtonName("Setting");
        warningDialog.setNegativeButtonName("Cancel");
        warningDialog.show();

    }

    private static int REQUEST_CAMERA = 1;

    private List<Bitmap> bitmapList = new ArrayList<>();
    private List<LocationModel> locationModelList = new ArrayList<>();
    private UniqueLocationTrack locationTrack;
    private ImageAdapter imageAdapter;

    private void openCamera() {
        Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(cameraIntent, REQUEST_CAMERA);

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (requestCode == REQUEST_CAMERA) {
                Bitmap bitmap = (Bitmap) data.getExtras().get("data");
                bitmapList.add(bitmap);

                imageAdapter.notifyDataSetChanged();
            }
        }
    }

    @OnClick(R.id.btn_click)
    public void onPhotoClicked() {

        Dexter.withActivity(getActivity())
                .withPermissions(Manifest.permission.CAMERA, Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION)
                .withListener(new MultiplePermissionsListener() {
                    @Override
                    public void onPermissionsChecked(MultiplePermissionsReport report) {
                        if (report.areAllPermissionsGranted()) {
                            if (UniqueUtils.isLocationEnabled(getContext())) {
                                locationTrack = new UniqueLocationTrack(getActivity());
                                locationTrack.setListener(AdminFragment.this);
                                openCamera();
                            }else {
                                UniqueUtils.showWarningMessage(Constant.ENABLE_LOCATION);
                            }
                        } else {
                            showSettingsDialog();
                        }
                    }

                    @Override
                    public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {
                        token.continuePermissionRequest();
                    }
                })
                .onSameThread()
                .check();
    }

    @Override
    public void onLocationResult(Location location) {
        if (locationTrack != null) {
            LocationModel locationModel = new LocationModel();
            locationModel.latitude = location.getLatitude();
            locationModel.longitude = location.getLongitude();
            locationModel.address = locationTrack.getAddress(location.getLatitude(), location.getLongitude());
            locationModelList.add(locationModel);
        }
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {

    }

    @Override
    public void afterTextChanged(Editable s) {

        if (s.length()==1){
            if (s == editQty.getEditableText()){
                if (s.charAt(0)=='.'){
                    editQty.setText("1");
                    editQty.setSelection(1);
                }
            }else  if (s == editCount.getEditableText()){
                if (s.charAt(0)=='.'){
                    editCount.setText("0");
                    editCount.setSelection(1);
                }
            }else  if (s == editGst.getEditableText()){
                if (s.charAt(0)=='.'){
                    editGst.setText("0");
                    editGst.setSelection(1);
                }
            }
        }

    }
}
