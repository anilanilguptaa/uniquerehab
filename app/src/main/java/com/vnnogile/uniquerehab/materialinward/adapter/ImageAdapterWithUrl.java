package com.vnnogile.uniquerehab.materialinward.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;
import com.vnnogile.uniquerehab.R;
import com.vnnogile.uniquerehab.dailyprogressscreen.adapter.ImageAdapter;
import com.vnnogile.uniquerehab.dailyprogressscreen.model.DPPModel;
import com.vnnogile.uniquerehab.materialconsumptionscreen.model.MaterialConsumptionData;
import com.vnnogile.uniquerehab.materialinward.model.MaterialInWord;
import com.vnnogile.uniquerehab.zoomimageview.view.ZoomImageViewActivity;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ImageAdapterWithUrl extends RecyclerView.Adapter<ImageAdapterWithUrl.ImageViewHolder> {


    private List<MaterialInWord.ImageDatum> imageData = new ArrayList<>();
    private Context context;
    private Listener  listener;
    public ImageAdapterWithUrl(List<MaterialInWord.ImageDatum> imageData,Context context,Listener listener) {
        this.imageData = imageData;
        this.context=context;
        this.listener= listener;
    }
    public ImageAdapterWithUrl(MaterialConsumptionData data, Context context, Listener listener,String test) {

        for (int i = 0; i < data.imageData.size(); i++) {
            MaterialInWord.ImageDatum  datum = new MaterialInWord.ImageDatum();
            datum.id = data.imageData.get(i).documentId;
            datum.path = data.imageData.get(i).imagePath;
            datum.location = data.imageData.get(i).location;
            imageData.add(datum);
        }

        this.context=context;
        this.listener= listener;
    }

    public interface Listener{
        void onDeleteImage(int id);
    }

    @NonNull
    @Override
    public ImageViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.cell_material_image, parent, false);
        return new ImageViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ImageViewHolder holder, int position) {
        MaterialInWord.ImageDatum datum = imageData.get(position);




        if (datum.path!=null && !datum.path.isEmpty()){
            Picasso.get().load(datum.path).into(holder.imageView);
        }else {
            Picasso.get().load(datum.image_path).into(holder.imageView);
        }

        holder.imageDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (listener!=null)
                    listener.onDeleteImage(datum.id==0?datum.document_id:datum.id);
            }
        });
    }

    @Override
    public int getItemCount() {
        return imageData.size();
    }

    class ImageViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.image_view)
        ImageView imageView;
        @BindView(R.id.image_location)
        ImageView imageLocation;
        @BindView(R.id.image_delete)
        ImageView imageDelete;
        @BindView(R.id.pic_view)
        ImageView picView;
        ImageViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

            if (listener==null){
                imageDelete.setVisibility(View.GONE);
            }else {
                imageDelete.setVisibility(View.VISIBLE);
            }

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    MaterialInWord.ImageDatum datum = imageData.get(getAdapterPosition());
                    Intent intent = new Intent(context, ZoomImageViewActivity.class);
                    intent.putExtra("data", datum);
                    context.startActivity(intent);
                }
            });
        }
    }
}
