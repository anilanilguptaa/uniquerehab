package com.vnnogile.uniquerehab.dashboardscreen.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class LandingPageModel implements Parcelable {


        @SerializedName("id")
        @Expose
        public Integer id;
        @SerializedName("name")
        @Expose
        public String name;
        @SerializedName("description")
        @Expose
        public String description;
        @SerializedName("start_date")
        @Expose
        public String startDate;
        @SerializedName("end_date")
        @Expose
        public String endDate;
        @SerializedName("client_user_id")
        @Expose
        public Integer clientUserId;
        @SerializedName("site_engineer_user_id")
        @Expose
        public Integer siteEngineerUserId;
        @SerializedName("schedule_pdf")
        @Expose
        public Integer schedulePdf;
        @SerializedName("tender_doc")
        @Expose
        public Integer tenderDoc;
        @SerializedName("photos")
        @Expose
        public String photos;
        @SerializedName("status")
        @Expose
        public String status;

    protected LandingPageModel(Parcel in) {
        if (in.readByte() == 0) {
            id = null;
        } else {
            id = in.readInt();
        }
        name = in.readString();
        description = in.readString();
        startDate = in.readString();
        endDate = in.readString();
        if (in.readByte() == 0) {
            clientUserId = null;
        } else {
            clientUserId = in.readInt();
        }
        if (in.readByte() == 0) {
            siteEngineerUserId = null;
        } else {
            siteEngineerUserId = in.readInt();
        }
        if (in.readByte() == 0) {
            schedulePdf = null;
        } else {
            schedulePdf = in.readInt();
        }
        if (in.readByte() == 0) {
            tenderDoc = null;
        } else {
            tenderDoc = in.readInt();
        }
        photos = in.readString();
        status = in.readString();
    }

    public static final Creator<LandingPageModel> CREATOR = new Creator<LandingPageModel>() {
        @Override
        public LandingPageModel createFromParcel(Parcel in) {
            return new LandingPageModel(in);
        }

        @Override
        public LandingPageModel[] newArray(int size) {
            return new LandingPageModel[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        if (id == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(id);
        }
        dest.writeString(name);
        dest.writeString(description);
        dest.writeString(startDate);
        dest.writeString(endDate);
        if (clientUserId == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(clientUserId);
        }
        if (siteEngineerUserId == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(siteEngineerUserId);
        }
        if (schedulePdf == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(schedulePdf);
        }
        if (tenderDoc == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(tenderDoc);
        }
        dest.writeString(photos);
        dest.writeString(status);
    }
}
