package com.vnnogile.uniquerehab.dashboardscreen.controller;

import android.content.Context;
import android.util.Log;

import androidx.annotation.NonNull;

import com.vnnogile.uniquerehab.dashboardscreen.model.LandingPageModel;
import com.vnnogile.uniquerehab.dashboardscreen.model.NotificationModel;
import com.vnnogile.uniquerehab.global.Constant;
import com.vnnogile.uniquerehab.global.MyApplication;
import com.vnnogile.uniquerehab.global.PreferenceManager;
import com.vnnogile.uniquerehab.loginscreen.model.LoginResponse;
import com.vnnogile.uniquerehab.retrofit.ApiClient;
import com.vnnogile.uniquerehab.retrofit.ApiInterface;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LandingPageController {

    public static final String TAG = LandingPageController.class.getSimpleName();

    private Context context;
    private ApiInterface apiInterface;
    private LandingPageListener  listener;

    public LandingPageController(Context context) {
        this.context = context;

        apiInterface = ApiClient.getClient().create(ApiInterface.class);

        listener = (LandingPageListener) context;
    }


    public void getLandingPageProject() {
        LoginResponse loginResponse = PreferenceManager.getInstance().getUserDetails();
        Call<List<LandingPageModel>> stringCall = apiInterface.getLandingPage("Bearer " + loginResponse.userDetail.apiToken,
                String.valueOf(loginResponse.userDetail.id));

        stringCall.enqueue(new Callback<List<LandingPageModel>>() {
            @Override
            public void onResponse(@NonNull Call<List<LandingPageModel>> call, @NonNull Response<List<LandingPageModel>> response) {
                Log.e(TAG, "onResponse: " + response.body().size());

                if (response.isSuccessful()){
                    if (response.body()!=null){
                        listener.onSuccessLandingPage(response.body());
                    }else {
                        listener.onFailedLandingPage(Constant.SOMETHING_WRONG);
                    }
                }else {
                    listener.onFailedLandingPage(Constant.SERVER_ERROR);
                }
            }

            @Override
            public void onFailure(@NonNull Call<List<LandingPageModel>> call, @NonNull Throwable t) {
                stringCall.cancel();
                Log.e(TAG, "onFailure: " + t.toString());
                listener.onFailedLandingPage(Constant.SERVER_ERROR);
            }
        });

    }

    public void getUnreadProject(){

        Call<List<NotificationModel>>    objectCall = apiInterface.getUnreadProject(
                PreferenceManager.getInstance().getToken(),
                PreferenceManager.getInstance().getUserId(),
                MyApplication.getInstance().getProjectId()
        );

        objectCall.enqueue(new Callback<List<NotificationModel>>() {
            @Override
            public void onResponse(Call<List<NotificationModel>> call, Response<List<NotificationModel>> response) {
                if (response.isSuccessful()){
                    listener.onSuccessUnread(response.body());
                }else {
                    listener.onFailedLandingPage(Constant.SERVER_ERROR);
                }
            }

            @Override
            public void onFailure(Call<List<NotificationModel>> call, Throwable throwable) {
                Log.e(TAG, "onFailure: "+throwable.toString() );
                listener.onFailedLandingPage(Constant.SERVER_ERROR);
            }
        });

    }

    public interface LandingPageListener {
        void onSuccessLandingPage(List<LandingPageModel> list);

        void onSuccessUnread(List<NotificationModel> list);

        void onFailedLandingPage(String errorMessage);
    }
}
