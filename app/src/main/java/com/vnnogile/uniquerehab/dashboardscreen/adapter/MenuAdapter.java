package com.vnnogile.uniquerehab.dashboardscreen.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.vnnogile.uniquerehab.R;
import com.vnnogile.uniquerehab.dashboardscreen.model.MenuItem;
import com.vnnogile.uniquerehab.dashboardscreen.model.MenuProvider;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MenuAdapter extends RecyclerView.Adapter<MenuAdapter.MenuViewHolder> {

    private ArrayList<MenuItem> menuItems= MenuProvider.getDashboardMenus();
    private MenuAdapterListener listener;
    private int resId;

    public MenuAdapter(MenuAdapterListener listener,int resId) {
        this.listener = listener;
        this.resId = resId;
    }

    public interface MenuAdapterListener {
        void onMenuItemClick(MenuItem menuItem);
    }

    public void updateMenus(ArrayList<MenuItem> menuItems){
        this.menuItems = new ArrayList<>();
        this.menuItems.clear();
        this.menuItems = menuItems;
        notifyDataSetChanged();
    }


    public  void  resetMenus(){
        this.menuItems.clear();
        this.menuItems = MenuProvider.getDashboardMenus();
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public MenuViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(resId, parent, false);
        return new MenuViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MenuViewHolder holder, int position) {
        MenuItem menuItem = menuItems.get(position);

        holder.icon.setImageResource(menuItem.icon);
        holder.txtTitle.setText(menuItem.title);


        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (listener != null)
                    listener.onMenuItemClick(menuItem);
            }
        });

        if (menuItem.status.equalsIgnoreCase("read")){
            holder.indicator.setVisibility(View.GONE);
        }else {
            holder.indicator.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public int getItemCount() {
        return menuItems.size();
    }

    class MenuViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.icon)
        ImageView icon;
        @BindView(R.id.txt_title)
        TextView txtTitle;
        @BindView(R.id.indicator)
        View indicator;

        MenuViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
