package com.vnnogile.uniquerehab.dashboardscreen.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class NotificationModel implements Parcelable {

    @SerializedName("id")
    @Expose
    public int id;
    @SerializedName("role_subscription_master_id")
    @Expose
    public String roleSubscriptionMasterId;
    @SerializedName("project_id")
    @Expose
    public int projectId;
    @SerializedName("user_id")
    @Expose
    public int userId;
    @SerializedName("status")
    @Expose
    public String status;
    @SerializedName("created_at")
    @Expose
    public String createdAt;
    @SerializedName("is_active")
    @Expose
    public int isActive;
    @SerializedName("subscription_master_code")
    @Expose
    public String subscriptionMasterCode;
    @SerializedName("role_name")
    @Expose
    public String  roleName;

    protected NotificationModel(Parcel in) {
        id = in.readInt();
        roleSubscriptionMasterId = in.readString();
        projectId = in.readInt();
        userId = in.readInt();
        status = in.readString();
        createdAt = in.readString();
        isActive = in.readInt();
        subscriptionMasterCode = in.readString();
        roleName = in.readString();
    }

    public static final Creator<NotificationModel> CREATOR = new Creator<NotificationModel>() {
        @Override
        public NotificationModel createFromParcel(Parcel in) {
            return new NotificationModel(in);
        }

        @Override
        public NotificationModel[] newArray(int size) {
            return new NotificationModel[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeString(roleSubscriptionMasterId);
        dest.writeInt(projectId);
        dest.writeInt(userId);
        dest.writeString(status);
        dest.writeString(createdAt);
        dest.writeInt(isActive);
        dest.writeString(subscriptionMasterCode);
        dest.writeString(roleName);
    }
}
