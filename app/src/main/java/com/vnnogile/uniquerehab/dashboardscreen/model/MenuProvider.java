package com.vnnogile.uniquerehab.dashboardscreen.model;

import com.vnnogile.uniquerehab.R;
import com.vnnogile.uniquerehab.global.PreferenceManager;
import com.vnnogile.uniquerehab.global.UniqueUtils;

import java.util.ArrayList;

public class MenuProvider {

    public static final int MENU_MATERIAL_AUDIT = 11;
    public static final int MENU_MATERIAL_TRANSFER = 12;
    private static MenuItem menuItem;
    public static final int MENU_DPR = 1;
    public static final int MENU_MANPOWER = 2;
    public static final int MENU_DAILY_PHOTO = 3;
    public static final int MENU_MATERIAL_REQ = 4;
    public static final int MENU_MATERIAL_IN = 5;
    public static final int MENU_MATERIAL_CON = 6;
    public static final int MENU_METING = 7;
    public static final int MENU_PETTY_CASH = 8;
    public static final int MENU_RECORD_ATTENDANCE = 9;
    public static final int MENU_COMPLAINT = 10;

    public static ArrayList<MenuItem> getDashboardMenus() {
        ArrayList<MenuItem> arrayList = new ArrayList<>();



        if (PreferenceManager.getInstance().getUserRole().equalsIgnoreCase(UniqueUtils.Client)){

            //1
            menuItem = new MenuItem();
            menuItem.icon = R.drawable.ic_dpr;
            menuItem.code = MENU_DPR;
            menuItem.title = "DPR";
            menuItem.menuCode ="dpr";
            menuItem.status ="read";
            arrayList.add(menuItem);

            //3
            menuItem = new MenuItem();
            menuItem.icon = R.drawable.ic_daily_photos;
            menuItem.code = MENU_DAILY_PHOTO;
            menuItem.title = "Daily Photos";
            menuItem.menuCode="dpp";
            menuItem.status ="read";
            arrayList.add(menuItem);

            //8
            menuItem = new MenuItem();
            menuItem.icon = R.drawable.ic_complaint_register;
            menuItem.code = MENU_COMPLAINT;
            menuItem.title = "Complaint\nRegister";
            menuItem.menuCode="complaint";
            menuItem.status ="read";
            arrayList.add(menuItem);


            //9
            menuItem = new MenuItem();
            menuItem.icon = R.drawable.ic_minutes_of_meeting;
            menuItem.code = MENU_METING;
            menuItem.title = "Minutes of\nMeeting";
            menuItem.menuCode ="minute_of_meeting";
            menuItem.status ="read";
            arrayList.add(menuItem);
        }else {
            //1
            menuItem = new MenuItem();
            menuItem.icon = R.drawable.ic_dpr;
            menuItem.code = MENU_DPR;
            menuItem.title = "DPR";
            menuItem.menuCode = "dpr";
            menuItem.status ="read";
            arrayList.add(menuItem);


            //3
            menuItem = new MenuItem();
            menuItem.icon = R.drawable.ic_daily_photos;
            menuItem.code = MENU_DAILY_PHOTO;
            menuItem.title = "Daily Photos";
            menuItem.menuCode="dpp";
            menuItem.status ="read";
            arrayList.add(menuItem);

            //2
            menuItem = new MenuItem();
            menuItem.icon = R.drawable.ic_manpower;
            menuItem.code = MENU_MANPOWER;
            menuItem.title = "Manpower";
            menuItem.menuCode = "manpower";
            menuItem.status ="read";
            arrayList.add(menuItem);



            //4
            menuItem = new MenuItem();
            menuItem.icon = R.drawable.ic_material_request;
            menuItem.code = MENU_MATERIAL_REQ;
            menuItem.title = "Material\nRequest";
            menuItem.menuCode="material_request";
            menuItem.status ="read";
            arrayList.add(menuItem);


            //5
            menuItem = new MenuItem();
            menuItem.icon = R.drawable.ic_material_in;
            menuItem.code = MENU_MATERIAL_IN;
            menuItem.title = "Material In";
            menuItem.menuCode="material_inwards";
            menuItem.status ="read";
            arrayList.add(menuItem);


            //6
            menuItem = new MenuItem();
            menuItem.icon = R.drawable.ic_material_consumption;
            menuItem.code = MENU_MATERIAL_CON;
            menuItem.title = "Material\nConsumption";
            menuItem.menuCode="material_consumption";
            menuItem.status ="read";
            arrayList.add(menuItem);


            //6
            menuItem = new MenuItem();
            menuItem.icon = R.drawable.ic_material_consumption;
            menuItem.code = MENU_MATERIAL_TRANSFER;
            menuItem.title = "Material\nTransfer";
            menuItem.menuCode="material_transfer";
            menuItem.status ="read";
            arrayList.add(menuItem);

            //6
            menuItem = new MenuItem();
            menuItem.icon = R.drawable.ic_material_consumption;
            menuItem.code = MENU_MATERIAL_AUDIT;
            menuItem.title = "Material\nAudit";
            menuItem.menuCode="material_audit";
            menuItem.status ="read";
            arrayList.add(menuItem);




            //8
            menuItem = new MenuItem();
            menuItem.icon = R.drawable.ic_complaint_register;
            menuItem.code = MENU_COMPLAINT;
            menuItem.title = "Complaint\nRegister";
            menuItem.menuCode="complaint";
            menuItem.status ="read";
            arrayList.add(menuItem);


            //9
            menuItem = new MenuItem();
            menuItem.icon = R.drawable.ic_minutes_of_meeting;
            menuItem.code = MENU_METING;
            menuItem.title = "Minutes of\nMeeting";
            menuItem.menuCode="minute_of_meeting";
            menuItem.status ="read";
            arrayList.add(menuItem);


            //10
            menuItem = new MenuItem();
            menuItem.icon = R.drawable.ic_record_attendance;
            menuItem.code = MENU_RECORD_ATTENDANCE;
            menuItem.title = "Record\nAttendance";
            menuItem.menuCode="attendance";
            menuItem.status ="read";
            arrayList.add(menuItem);

            //7
            menuItem = new MenuItem();
            menuItem.icon = R.drawable.ic_petty_cash;
            menuItem.code = MENU_PETTY_CASH;
            menuItem.title = "Petty Cash";
            menuItem.menuCode="petty_cash";
            menuItem.status ="read";
            arrayList.add(menuItem);
        }

        return arrayList;

    }
}
