package com.vnnogile.uniquerehab.dashboardscreen.controller;

import android.content.Context;
import android.util.Log;

import com.vnnogile.uniquerehab.dashboardscreen.model.DoumentModel;
import com.vnnogile.uniquerehab.global.Constant;
import com.vnnogile.uniquerehab.global.MyApplication;
import com.vnnogile.uniquerehab.global.PreferenceManager;
import com.vnnogile.uniquerehab.retrofit.ApiClient;
import com.vnnogile.uniquerehab.retrofit.ApiInterface;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DownloadProjectController {

    public static final String TAG = DownloadProjectController.class.getSimpleName();

    private Context context;
    private ApiInterface apiInterface;
    private DownloadProjectListener listener;

    public DownloadProjectController(Context context) {
        this.context = context;
        apiInterface = ApiClient.getClient().create(ApiInterface.class);
        listener =(DownloadProjectListener)context;
    }


    public  interface DownloadProjectListener{
        void onSuccessResponse(DoumentModel doumentModel);
        void onFailure(String errorMessage);
    }


    public void downloadTender(){


        Call<DoumentModel>  objectCall = apiInterface.downloadTenderDoc(
                PreferenceManager.getInstance().getToken(),
                PreferenceManager.getInstance().getUserId(),
                MyApplication.getInstance().getProjectId()
        );

        objectCall.enqueue(new Callback<DoumentModel>() {

            @Override
            public void onResponse(Call<DoumentModel> call, Response<DoumentModel> response) {
                if (response.isSuccessful()){
                    listener.onSuccessResponse(response.body());
                }else {
                    listener.onFailure(Constant.SERVER_ERROR);
                }
            }

            @Override
            public void onFailure(Call<DoumentModel> call, Throwable throwable) {
                Log.e(TAG, "onFailure: "+throwable.toString());
                listener.onFailure(Constant.SERVER_ERROR);
            }
        });

    }


    public void downloadProject(){


        Call<DoumentModel>  objectCall = apiInterface.downloadProjectDoc(
                PreferenceManager.getInstance().getToken(),
                PreferenceManager.getInstance().getUserId(),
                MyApplication.getInstance().getProjectId()
        );

        objectCall.enqueue(new Callback<DoumentModel>() {

            @Override
            public void onResponse(Call<DoumentModel> call, Response<DoumentModel> response) {
                if (response.isSuccessful()){
                    listener.onSuccessResponse(response.body());
                }else {
                    listener.onFailure(Constant.SERVER_ERROR);
                }
            }

            @Override
            public void onFailure(Call<DoumentModel> call, Throwable throwable) {
                Log.e(TAG, "onFailure: "+throwable.toString());
                listener.onFailure(Constant.SERVER_ERROR);
            }
        });

    }


}
