package com.vnnogile.uniquerehab.dashboardscreen.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DoumentModel implements Parcelable{
    @SerializedName("message")
    @Expose
    public String message;
    @SerializedName("data")
    @Expose
    public Datum data;
    @SerializedName("status")
    @Expose
    public boolean status;

    protected DoumentModel(Parcel in) {
        message = in.readString();
        data = in.readParcelable(Datum.class.getClassLoader());
        status = in.readByte() != 0;
    }

    public static final Creator<DoumentModel> CREATOR = new Creator<DoumentModel>() {
        @Override
        public DoumentModel createFromParcel(Parcel in) {
            return new DoumentModel(in);
        }

        @Override
        public DoumentModel[] newArray(int size) {
            return new DoumentModel[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(message);
        dest.writeParcelable(data, flags);
        dest.writeByte((byte) (status ? 1 : 0));
    }


 public    static class Datum implements Parcelable {
        @SerializedName("id")
        @Expose
        private int id;
        @SerializedName("name")
        @Expose
        public String name;
        @SerializedName("description")
        @Expose
        private String description;
        @SerializedName("path")
        @Expose
        public String path;
        @SerializedName("document_type")
        @Expose
        private String documentType;
        @SerializedName("entity_id")
        @Expose
        private int entityId;
        @SerializedName("entity_name")
        @Expose
        private String entityName;
        @SerializedName("created_date")
        @Expose
        private String createdDate;
        @SerializedName("created_at")
        @Expose
        private String createdAt;
        @SerializedName("is_active")
        @Expose
        private int isActive;
        @SerializedName("lattitude")
        @Expose
        private String lattitude;
        @SerializedName("longitude")
        @Expose
        private String longitude;
        @SerializedName("address")
        @Expose
        private String address;

        protected Datum(Parcel in) {
            id = in.readInt();
            name = in.readString();
            description = in.readString();
            path = in.readString();
            documentType = in.readString();
            entityId = in.readInt();
            entityName = in.readString();
            createdDate = in.readString();
            createdAt = in.readString();
            isActive = in.readInt();
            lattitude = in.readString();
            longitude = in.readString();
            address = in.readString();
        }

        public static final Creator<Datum> CREATOR = new Creator<Datum>() {
            @Override
            public Datum createFromParcel(Parcel in) {
                return new Datum(in);
            }

            @Override
            public Datum[] newArray(int size) {
                return new Datum[size];
            }
        };

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeInt(id);
            dest.writeString(name);
            dest.writeString(description);
            dest.writeString(path);
            dest.writeString(documentType);
            dest.writeInt(entityId);
            dest.writeString(entityName);
            dest.writeString(createdDate);
            dest.writeString(createdAt);
            dest.writeInt(isActive);
            dest.writeString(lattitude);
            dest.writeString(longitude);
            dest.writeString(address);
        }
    }
}
