package com.vnnogile.uniquerehab.dashboardscreen.model;

import android.os.Parcel;
import android.os.Parcelable;

public class MenuItem implements Parcelable {

    public int icon;
    public int code;
    public String title;
    public String menuCode;
    public String status;

    public MenuItem() {
    }

    protected MenuItem(Parcel in) {
        icon = in.readInt();
        code = in.readInt();
        title = in.readString();
        menuCode = in.readString();
        status = in.readString();
    }

    public static final Creator<MenuItem> CREATOR = new Creator<MenuItem>() {
        @Override
        public MenuItem createFromParcel(Parcel in) {
            return new MenuItem(in);
        }

        @Override
        public MenuItem[] newArray(int size) {
            return new MenuItem[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(icon);
        dest.writeInt(code);
        dest.writeString(title);
        dest.writeString(menuCode);
        dest.writeString(status);
    }
}
