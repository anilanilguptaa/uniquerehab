package com.vnnogile.uniquerehab.dashboardscreen.view;

import android.Manifest;
import android.app.DownloadManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatSpinner;
import androidx.appcompat.widget.Toolbar;
import androidx.cardview.widget.CardView;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.navigation.NavigationView;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionDeniedResponse;
import com.karumi.dexter.listener.PermissionGrantedResponse;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.single.PermissionListener;
import com.squareup.picasso.Picasso;
import com.vnnogile.uniquerehab.R;
import com.vnnogile.uniquerehab.attendancescreen.view.AttendanceActivity;
import com.vnnogile.uniquerehab.dailyprogressscreen.view.DailyProgressPhotoActivity;
import com.vnnogile.uniquerehab.dashboardscreen.adapter.MenuAdapter;
import com.vnnogile.uniquerehab.dashboardscreen.controller.DownloadProjectController;
import com.vnnogile.uniquerehab.dashboardscreen.controller.LandingPageController;
import com.vnnogile.uniquerehab.dashboardscreen.model.DoumentModel;
import com.vnnogile.uniquerehab.dashboardscreen.model.LandingPageModel;
import com.vnnogile.uniquerehab.dashboardscreen.model.MenuItem;
import com.vnnogile.uniquerehab.dashboardscreen.model.MenuProvider;
import com.vnnogile.uniquerehab.dashboardscreen.model.NotificationModel;
import com.vnnogile.uniquerehab.dprscreen.view.DPRActivity;
import com.vnnogile.uniquerehab.dprscreen.view.DPRClientActivity;
import com.vnnogile.uniquerehab.global.Constant;
import com.vnnogile.uniquerehab.global.MyApplication;
import com.vnnogile.uniquerehab.global.PreferenceManager;
import com.vnnogile.uniquerehab.global.ProgressBar;
import com.vnnogile.uniquerehab.global.UniqueUtils;
import com.vnnogile.uniquerehab.global.WarningDialog;
import com.vnnogile.uniquerehab.loginscreen.model.LoginResponse;
import com.vnnogile.uniquerehab.loginscreen.view.LoginActivity;
import com.vnnogile.uniquerehab.manpowerscren.view.ManPowerActivity;
import com.vnnogile.uniquerehab.materialaudit.view.MaterialAuditActivity;
import com.vnnogile.uniquerehab.materialconsumptionscreen.view.MaterialConsumptionActivity;
import com.vnnogile.uniquerehab.materialinward.view.MaterialInwardActivity;
import com.vnnogile.uniquerehab.materialrequestscreen.view.MaterialRequestActivity;
import com.vnnogile.uniquerehab.materialtransfer.view.MaterialTransferActivity;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class DashboardActivity extends AppCompatActivity implements MenuAdapter.MenuAdapterListener,
        LandingPageController.LandingPageListener, DownloadProjectController.DownloadProjectListener {

    public static final String TAG = DashboardActivity.class.getSimpleName();
    @BindView(R.id.type_sp)
    Spinner typeSp;
    private ActionBarDrawerToggle actionBarDrawerToggle;
    private ArrayList<String> pType = new ArrayList<>();
    private DownloadProjectController controller;
    LandingPageController landingPageController;

    MenuAdapter gridAdapter;
    MenuAdapter cellAdapter;

    @BindView(R.id.toolbar_menu)
    ImageView toolbarMenu;
    @BindView(R.id.toolbar_profile)
    ImageView toolbarProfile;
    @BindView(R.id.menu_rv)
    RecyclerView menuRv;
    @BindView(R.id.image_schedule)
    ImageView imageSchedule;
    @BindView(R.id.layout_schedule)
    LinearLayout layoutSchedule;
    @BindView(R.id.image_tender)
    ImageView imageTender;
    @BindView(R.id.layout_tender)
    LinearLayout layoutTender;
    @BindView(R.id.txt_schedule_lbl)
    TextView txtScheduleLbl;
    @BindView(R.id.txt_tender_lbl)
    TextView txtTenderLbl;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.nav_image)
    ImageView navImage;
    @BindView(R.id.nav_username)
    TextView navUsername;
    @BindView(R.id.nav_view)
    NavigationView navView;
    @BindView(R.id.nav_menu_rv)
    RecyclerView navMenuRv;
    @BindView(R.id.drawer_layout)
    DrawerLayout drawerLayout;
    @BindView(R.id.toolbar_name)
    TextView toolbarName;
    @BindView(R.id.container)
    CardView container;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);


        setToolbar();
        loadVariables();

      /*  if (!MyApplication.getInstance().isGetTheProject())

        else
            setUpSpinnerReason(MyApplication.getInstance().getList());*/

        registerReceiver(onDownloadComplete,new IntentFilter(DownloadManager.ACTION_DOWNLOAD_COMPLETE));

    }

    @Override
    protected void onResume() {
        super.onResume();
        getData();
       // typeSp.setSelection(MyApplication.getInstance().getSelectedPosition());

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(onDownloadComplete);

    }

    private void setToolbar() {
        LoginResponse loginResponse = PreferenceManager.getInstance().getUserDetails();

        toolbarProfile.setImageResource(R.drawable.profile_picture);
        toolbarName.setText("Project Name");

        navUsername.setText(loginResponse.userDetail.firstName +" " +loginResponse.userDetail.lastName);
        Picasso.get().load(loginResponse.userProfileDetail.photo).into(navImage);

        drawerLayout.addDrawerListener(actionBarDrawerToggle);
        actionBarDrawerToggle = new ActionBarDrawerToggle(this, drawerLayout, toolbar, R.string.open, R.string.close);

        actionBarDrawerToggle.setDrawerSlideAnimationEnabled(true);
        actionBarDrawerToggle.setToolbarNavigationClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                drawerLayout.openDrawer(GravityCompat.START);
            }
        });
        actionBarDrawerToggle.setDrawerIndicatorEnabled(false);
        actionBarDrawerToggle.setHomeAsUpIndicator(R.drawable.ic_menu_icon);
        actionBarDrawerToggle.syncState();
        cellAdapter = new MenuAdapter(this, R.layout.cell_menu);
        navMenuRv.addItemDecoration(new DividerItemDecoration(this, DividerItemDecoration.VERTICAL));
        navMenuRv.setAdapter(cellAdapter);
    }

    @Override
    public void onBackPressed() {

        final WarningDialog warningDialog = new WarningDialog(this, new WarningDialog.WarningDialogListener() {
            @Override
            public void onPositiveClick() {
                finish();
            }

            @Override
            public void onNegativeClick() {

            }
        });
        warningDialog.setMessage("Do you want to exit app?");
        warningDialog.show();
    }

    private void loadVariables() {
        controller = new DownloadProjectController(this);
        menuRv.setLayoutManager(new GridLayoutManager(this, 2));
        //  menuRv.setNestedScrollingEnabled(false);
        menuRv.setHasFixedSize(true);
        gridAdapter = new MenuAdapter(this, R.layout.grid_dashboard);
        menuRv.setAdapter(gridAdapter);
    }

    private void showSettingsDialog() {
        WarningDialog warningDialog = new WarningDialog(this, new WarningDialog.WarningDialogListener() {
            @Override
            public void onPositiveClick() {
                UniqueUtils.openSettings(DashboardActivity.this);
            }

            @Override
            public void onNegativeClick() {

            }
        });
        warningDialog.setMessage("This app needs permission to use this feature. You can grant them in app settings.");
        warningDialog.setPositiveButtonName("Setting");
        warningDialog.setNegativeButtonName("Cancel");
        warningDialog.show();

    }


    private void getData() {
        landingPageController = new LandingPageController(this);
        landingPageController.getLandingPageProject();
        //landingPageController.getUnreadProject();
    }


    public void setUpSpinnerReason(List<LandingPageModel> reasonList) {

        pType.clear();


        if (reasonList.size() > 0) {
            for (LandingPageModel datum : reasonList) {
                pType.add(datum.name);
            }
        }

        SpinnerAdapter dataAdapter = new SpinnerAdapter(this,reasonList);

//        // Creating adapter for spinner
//        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, pType);
//        // Drop down layout style - list view with radio button
     // dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        // attaching data adapter to spinner
        typeSp.setAdapter(dataAdapter);

        typeSp.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
               /* if (position != 0) {
                    selectedReasonId = reasonresponse.getData().get(position - 1).getId() + "";
                    //payload.setRemark( reasonresponse.getData().get(position-1).getName());
                } else {
                    selectedReasonId = null;
                    // payload.setRemark("");
                }*/
                toolbarName.setText(pType.get(position));
                MyApplication.getInstance().setProjectId(reasonList.get(position).id);
                MyApplication.getInstance().setProjectName(reasonList.get(position).name);
                MyApplication.getInstance().setSelectedPosition(position);
                landingPageController.getUnreadProject();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        if (reasonList.size() > 0) {
            typeSp.setSelection(MyApplication.getInstance().getSelectedPosition());
        }

    }

    @Override
    public void onMenuItemClick(MenuItem menuItem) {

        switch (menuItem.code) {
            case MenuProvider.MENU_DPR:
                if (PreferenceManager.getInstance().getUserRole().equalsIgnoreCase(UniqueUtils.Client) ||
                        PreferenceManager.getInstance().getUserRole().equalsIgnoreCase(UniqueUtils.ADMIN)) {
                    if (MyApplication.getInstance().getProjectId().equals("0"))
                        UniqueUtils.showWarningMessage(Constant.SELECT_PROJECT);
                    else
                        startActivity(new Intent(this, DPRClientActivity.class));
                } else {
                    if (MyApplication.getInstance().getProjectId().equals("0"))
                        UniqueUtils.showWarningMessage(Constant.SELECT_PROJECT);
                    else
                        startActivity(new Intent(this, DPRActivity.class));
                }

                break;
            case MenuProvider.MENU_MANPOWER:
                if (MyApplication.getInstance().getProjectId().equals("0"))
                    UniqueUtils.showWarningMessage(Constant.SELECT_PROJECT);
                else
                    startActivity(new Intent(this, ManPowerActivity.class));
                break;
            case MenuProvider.MENU_DAILY_PHOTO:
                if (MyApplication.getInstance().getProjectId().equals("0"))
                    UniqueUtils.showWarningMessage(Constant.SELECT_PROJECT);
                else
                    startActivity(new Intent(this, DailyProgressPhotoActivity.class));
                break;
            case MenuProvider.MENU_MATERIAL_REQ:
                if (MyApplication.getInstance().getProjectId().equals("0"))
                    UniqueUtils.showWarningMessage(Constant.SELECT_PROJECT);
                else
                    startActivity(new Intent(this, MaterialRequestActivity.class));
                break;
            case MenuProvider.MENU_MATERIAL_IN:
                if (MyApplication.getInstance().getProjectId().equals("0"))
                    UniqueUtils.showWarningMessage(Constant.SELECT_PROJECT);
                else
                    startActivity(new Intent(this, MaterialInwardActivity.class));
                break;
            case MenuProvider.MENU_MATERIAL_CON:
                if (MyApplication.getInstance().getProjectId().equals("0"))
                    UniqueUtils.showWarningMessage(Constant.SELECT_PROJECT);
                else
                    startActivity(new Intent(this, MaterialConsumptionActivity.class));
                break;
            case MenuProvider.MENU_MATERIAL_AUDIT:
                if (MyApplication.getInstance().getProjectId().equals("0"))
                    UniqueUtils.showWarningMessage(Constant.SELECT_PROJECT);
                else
                    startActivity(new Intent(this, MaterialAuditActivity.class));
                break;
            case MenuProvider.MENU_MATERIAL_TRANSFER:
                if (MyApplication.getInstance().getProjectId().equals("0"))
                    UniqueUtils.showWarningMessage(Constant.SELECT_PROJECT);
                else
                    startActivity(new Intent(this, MaterialTransferActivity.class));
                break;
            case MenuProvider.MENU_PETTY_CASH:
                break;
            case MenuProvider.MENU_COMPLAINT:
                break;
            case MenuProvider.MENU_METING:
                break;
            case MenuProvider.MENU_RECORD_ATTENDANCE:
                if (MyApplication.getInstance().getProjectId().equals("0"))
                    UniqueUtils.showWarningMessage(Constant.SELECT_PROJECT);
                else
                    startActivity(new Intent(this, AttendanceActivity.class));
                break;
        }

        if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
            drawerLayout.closeDrawers();
        }
    }

    @OnClick({R.id.layout_schedule, R.id.layout_tender})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.layout_schedule:
                Dexter.withActivity(this)
                        .withPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE)
                        .withListener(new PermissionListener() {
                            @Override
                            public void onPermissionGranted(PermissionGrantedResponse response) {
                                if (UniqueUtils.isNetworkAvailable(DashboardActivity.this)) {
                                    controller.downloadProject();
                                } else {
                                    UniqueUtils.showErrorMessage(Constant.NETWORK_ERROR_MESSAGE);
                                }
                            }

                            @Override
                            public void onPermissionDenied(PermissionDeniedResponse response) {
                                if (response.isPermanentlyDenied()) {
                                    showSettingsDialog();
                                }
                            }

                            @Override
                            public void onPermissionRationaleShouldBeShown(PermissionRequest permission, PermissionToken token) {
                                token.continuePermissionRequest();
                            }
                        })
                        .onSameThread()
                        .check();
                break;
            case R.id.layout_tender:
                Dexter.withActivity(this)
                        .withPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE)
                        .withListener(new PermissionListener() {
                            @Override
                            public void onPermissionGranted(PermissionGrantedResponse response) {
                                if (UniqueUtils.isNetworkAvailable(DashboardActivity.this)) {
                                    controller.downloadTender();
                                } else {
                                    UniqueUtils.showErrorMessage(Constant.NETWORK_ERROR_MESSAGE);
                                }
                            }

                            @Override
                            public void onPermissionDenied(PermissionDeniedResponse response) {
                                if (response.isPermanentlyDenied()) {
                                    showSettingsDialog();
                                }
                            }

                            @Override
                            public void onPermissionRationaleShouldBeShown(PermissionRequest permission, PermissionToken token) {
                                token.continuePermissionRequest();
                            }
                        })
                        .onSameThread()
                        .check();
                break;
        }
    }

    @Override
    public void onSuccessLandingPage(List<LandingPageModel> list) {

        if (list.size() > 0) {

            toolbarName.setText(list.get(0).name);
            setUpSpinnerReason(list);
            MyApplication.getInstance().setProjectId(list.get(0).id);
            MyApplication.getInstance().setProjectName(list.get(0).name);
            MyApplication.getInstance().setGetTheProject(true);
            MyApplication.getInstance().setList(list);
            // MyApplication.getInstance().setSelectedPosition(0);
        }

    }

    @Override
    public void onFailedLandingPage(String errorMessage) {
        UniqueUtils.showErrorMessage(errorMessage);
    }

    @OnClick(R.id.nav_logout)
    public void onViewClicked() {
        if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
            drawerLayout.closeDrawers();
        }
        ProgressBar progressBar = new ProgressBar(this);
        progressBar.show();
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                progressBar.dismiss();
                PreferenceManager.getInstance().clearUserData();
                MyApplication.getInstance().clear();
                Intent intent = new Intent(DashboardActivity.this, LoginActivity.class);
                startActivity(intent);
                finish();
            }
        }, 2000);
    }

    @Override
    public void onSuccessResponse(DoumentModel doumentModel) {
        if (doumentModel != null && doumentModel.status) {
            download(doumentModel);
        } else {
            assert doumentModel != null;
            UniqueUtils.showErrorMessage(doumentModel.message);
        }
    }


    ArrayList<MenuItem> menuItems = MenuProvider.getDashboardMenus();


    @Override
    public void onSuccessUnread(List<NotificationModel> list){
        ArrayList<MenuItem> finalList = new ArrayList<>();

        if (list.size()==0){
            cellAdapter.resetMenus();
            gridAdapter.resetMenus();
        }else {
            for(MenuItem item : menuItems){
                for (NotificationModel model : list){
                    if (item.menuCode.equalsIgnoreCase(model.subscriptionMasterCode)){
                        item.status="unread";
                        break;
                    }else {
                        item.status="read";
                    }
                }
                finalList.add(item);
            }

            cellAdapter.updateMenus(finalList);
            gridAdapter.updateMenus(finalList);
        }

    }

    @Override
    public void onFailure(String errorMessage) {
        UniqueUtils.showErrorMessage(errorMessage);
    }

    private long downloadID;
    private BroadcastReceiver onDownloadComplete = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            //Fetching the download id received with the broadcast
            long id = intent.getLongExtra(DownloadManager.EXTRA_DOWNLOAD_ID, -1);
            //Checking if the received broadcast is for our enqueued download by matching download id
            if (downloadID == id) {
                UniqueUtils.showSuccessMessage("Download Completed\n at "+fileName);
            }else {
                UniqueUtils.showWarningMessage("Something went wrong.");
            }
        }
    };

    String fileName="";
    private void download(DoumentModel model) {
        if (UniqueUtils.isNetworkAvailable(this)) {

            UniqueUtils.showSuccessMessage("Downloading....");
            String sFolder = Environment.getExternalStorageDirectory().getAbsolutePath() + "/UniqueRehab";
            fileName = sFolder + "/" + model.data.name;
            File file = new File(fileName);
            // Create directories, make sure exists
            new File(sFolder).mkdirs();
        /*
        Create a DownloadManager.Request with all the information necessary to start the download
         */
            DownloadManager.Request request=new DownloadManager.Request(Uri.parse(model.data.path))
                    .setTitle(model.data.name)// Title of the Download Notification
                    .setDescription("Downloading")// Description of the Download Notification
                    .setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE)// Visibility of the download Notification
                    .setDestinationUri(Uri.fromFile(file))// Uri of the destination file
                    // Set if charging is required to begin the download
                    .setAllowedOverMetered(true)// Set if download is allowed on Mobile network
                    .setAllowedOverRoaming(true);// Set if download is allowed on roaming network
            DownloadManager downloadManager= (DownloadManager) getSystemService(DOWNLOAD_SERVICE);
            downloadID = downloadManager.enqueue(request);// enqueue puts the download request in the queue.

        } else {
            UniqueUtils.showErrorMessage(Constant.NETWORK_ERROR_MESSAGE);
        }
    }


    class SpinnerAdapter extends ArrayAdapter<LandingPageModel>{

        private List<LandingPageModel> modelList;

        public SpinnerAdapter(@NonNull Context context,List<LandingPageModel> modelList) {
            super(context, 0,modelList);
            this.modelList = modelList;
        }

        @Override
        public int getCount() {
            return modelList.size();
        }

        private View initView(int position, View convertView, ViewGroup parent) {
            if (convertView == null) {
                convertView = LayoutInflater.from(getContext()).inflate(
                        R.layout.custom_spinner, parent, false
                );
            }
            LandingPageModel  model = getItem(position);


            View indicator = convertView.findViewById(R.id.indicator);
            TextView projectName = convertView.findViewById(R.id.txt_project_name);
            projectName.setText(model.name);
            projectName.setVisibility(View.VISIBLE);
            if (model.status.equalsIgnoreCase("Unread")){
                indicator.setVisibility(View.VISIBLE);
            }else {
                indicator.setVisibility(View.GONE);
            }

            return convertView;
        }


        @Override
        public View getDropDownView(int position, View convertView,
                                    ViewGroup parent) {
            return initView(position, convertView, parent);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            return initView(position, convertView, parent);
        }
    }
}
